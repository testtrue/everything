# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.1/maven-plugin/reference/html/#build-image)


### Build Docker image

```
mvn -Dvaadin.ci.build=true -Pprod -DskipTests spring-boot:build-image
docker tag backend:0.0.1-SNAPSHOT docker-home.boaslehrke.com/home/dashboard:XXX
docker push docker-home.boaslehrke.com/home/dashboard:XXX
```
