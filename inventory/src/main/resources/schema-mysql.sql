CREATE DATABASE IF NOT EXISTS `inventory`;

GRANT ALL PRIVILIGES `inventory`.* TO `root`;
flush privileges;

USE inventory;


CREATE TABLE IF NOT EXISTS `item` (
   `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
   `name` varchar(255) NOT NULL,
   `available_amount` float NOT NULL,
   `threshhold` float NOT NULL,
);


CREATE TABLE IF NOT EXISTS `item_productcode` (
   `product_code` varchar(255) PRIMARY KEY NOT NULL,
   `item` int REFERENCES `item` (`id`);
);

