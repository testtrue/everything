package de.lehrke.boas.everything.inventory.database;

import de.lehrke.boas.everything.inventory.workflows.ItemDTO;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity(name = "item")
@Data
public class JPAItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Integer availableAmount;
    private Integer threshhold;
    @OneToMany
    private List<JPAItemProductCode> productCodes;

    public JPAItem() {

    }

    public JPAItem(Long id, String name, Integer availableAmount, Integer threshhold) {
        this.id = id;
        this.name = name;
        this.availableAmount = availableAmount;
        this.threshhold = threshhold;
    }

    public JPAItem(ItemDTO dto) {
        this(dto.id(), dto.name(), dto.availableAmount(), dto.threshhold());
    }
}
