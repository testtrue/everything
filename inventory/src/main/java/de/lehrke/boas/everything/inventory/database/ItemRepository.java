package de.lehrke.boas.everything.inventory.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ItemRepository extends JpaRepository<JPAItem, String> {
    
}
