package de.lehrke.boas.everything.inventory.workflows;

import de.lehrke.boas.everything.inventory.domain.Item;

class ItemDomainMapper {

    private ItemDomainMapper() {
    }

    public static Item mapToDomain(ItemDTO itemDTO) {
        return Item.builder().id(itemDTO.id()).name(itemDTO.name()).availableAmount(itemDTO.availableAmount())
                .threshhold(itemDTO.threshhold()).barcodes(itemDTO.barcodes()).build();
    }

    public static ItemDTO mapToDto(Item item) {
        return new ItemDTO(item);
    }
}
