package de.lehrke.boas.everything.inventory.workflows;

import java.util.List;

public abstract class DomainDtos {

    public record RemoveItem(String id, Integer amount) {
    };

    public record InsertItem(String id, Integer amount) {
    };

    public record ModifyItem(String id, String name, Integer newThreshhold, List<String> barcodes) {
    };

    public record NewItem(String name, Integer threshhold, List<String> barcodes) {
    };
}
