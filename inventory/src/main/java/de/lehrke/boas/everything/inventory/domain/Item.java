package de.lehrke.boas.everything.inventory.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Item {

    private Long id;
    private String name;
    private Integer availableAmount;
    private Integer threshhold;
    private List<String> barcodes;

    public static Item createItem(String name, Integer threshhold, List<String> barcodes) {
        return Item.builder().name(name).threshhold(threshhold).availableAmount(0).barcodes(barcodes).build();
    }

    public void insertItems(Integer amount) {
        this.availableAmount += amount;
    }

    public void removeItems(Integer amount) {
        this.availableAmount -= amount;
    }

    public Boolean isAvailableAmountAboveThreshold() {
        return threshhold < availableAmount;
    }

}
