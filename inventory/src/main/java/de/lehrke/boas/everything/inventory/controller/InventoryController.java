package de.lehrke.boas.everything.inventory.controller;

import de.lehrke.boas.everything.inventory.workflows.DomainDtos;
import de.lehrke.boas.everything.inventory.workflows.InventoryWorkflows;
import de.lehrke.boas.everything.inventory.workflows.ItemDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("inventory")
public class InventoryController {

    private final InventoryWorkflows inventoryWorkflows;

    public InventoryController(InventoryWorkflows inventoryWorkflows) {
        this.inventoryWorkflows = inventoryWorkflows;
    }

    @GetMapping("/items")
    public List<ItemDTO> showAllItems() {
        return this.inventoryWorkflows.showAllItems();
    }

    @GetMapping("/items/search")
    public Optional<ItemDTO> searchItemByProductCode(@RequestParam String productCode) {
        return this.searchItemByProductCode(productCode);
    }

    @PostMapping("/items/new")
    public ItemDTO listNewItem(@RequestBody DomainDtos.NewItem event) {
        return this.inventoryWorkflows.listNewItem(event);
    }

    @PostMapping("/items/stock")
    public Optional<ItemDTO> stockItem(@RequestBody DomainDtos.InsertItem event) {
        return this.inventoryWorkflows.stockItem(event);
    }

    @PutMapping("/items/update")
    public Optional<ItemDTO> modifyItem(DomainDtos.ModifyItem event) {
        return this.modifyItem(event);
    }

    @PostMapping("/items/withdraw")
    public Optional<ItemDTO> withdrawItem(DomainDtos.RemoveItem event) {
        return this.withdrawItem(event);
    }

    @GetMapping("/items/export")
    public List<ItemDTO> exportShoppingList() {
        return this.exportShoppingList();
    }
}
