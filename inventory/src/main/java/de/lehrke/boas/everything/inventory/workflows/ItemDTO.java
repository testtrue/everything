package de.lehrke.boas.everything.inventory.workflows;

import de.lehrke.boas.everything.inventory.domain.Item;
import lombok.Builder;

import java.util.List;

@Builder
public record ItemDTO(Long id, String name, Integer availableAmount, Integer threshhold, List<String> barcodes) {

    public ItemDTO(Item item) {
        this(item.getId(), item.getName(), item.getAvailableAmount(), item.getThreshhold(), item.getBarcodes());
    }
}
