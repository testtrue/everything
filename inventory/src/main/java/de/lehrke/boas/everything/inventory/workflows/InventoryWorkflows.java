package de.lehrke.boas.everything.inventory.workflows;

import de.lehrke.boas.everything.inventory.domain.Item;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InventoryWorkflows {

    private ItemDataService itemDataService;

    public InventoryWorkflows(ItemDataService itemDataService) {
        this.itemDataService = itemDataService;
    }

    public List<ItemDTO> showAllItems() {
        return this.itemDataService.findAll();
    }

    public Optional<ItemDTO> searchItemByProductCode(String productCode) {
        return this.itemDataService.findByProductCode(productCode);
    }

    public ItemDTO listNewItem(DomainDtos.NewItem event) {
        Item itemToAdd = Item.createItem(event.name(), event.threshhold(), event.barcodes());
        return this.itemDataService.save(ItemDomainMapper.mapToDto(itemToAdd));
    }

    public Optional<ItemDTO> stockItem(DomainDtos.InsertItem event) {
        return this.itemDataService.findById(event.id()).or(() -> this.itemDataService.findByProductCode(event.id()))
                .map(ItemDomainMapper::mapToDomain).map(i -> {
                    i.insertItems(event.amount());
                    return i;
                }).map(ItemDomainMapper::mapToDto).map(this.itemDataService::save);
    }

    public Optional<ItemDTO> modifyItem(DomainDtos.ModifyItem event) {
        return this.itemDataService.findById(event.id()).map(ItemDomainMapper::mapToDomain)
                .map(i -> {
                    i.setThreshhold(event.newThreshhold());
                    i.setName(event.name());
                    i.setBarcodes(event.barcodes());
                    return i;
                }).map(ItemDomainMapper::mapToDto)
                .map(this.itemDataService::save);
    }

    public Optional<ItemDTO> withdrawItem(DomainDtos.RemoveItem event) {
        return this.itemDataService.findById(event.id()).map(ItemDomainMapper::mapToDomain).map(i -> {
            i.removeItems(event.amount());
            return i;
        }).map(ItemDomainMapper::mapToDto).map(this.itemDataService::save);
    }

    public List<ItemDTO> exportShoppingList() {
        return this.showAllItems().stream().map(ItemDomainMapper::mapToDomain)
                .filter(i -> !i.isAvailableAmountAboveThreshold()).map(ItemDomainMapper::mapToDto).toList();
    }

}
