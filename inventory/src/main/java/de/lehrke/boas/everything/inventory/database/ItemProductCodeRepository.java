package de.lehrke.boas.everything.inventory.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("inventoryItemProductCodeRepository")
public interface ItemProductCodeRepository extends JpaRepository<JPAItemProductCode, String> {

    Optional<JPAItemProductCode> findByProductCode(String code);

    List<JPAItemProductCode> findAllByItem(JPAItem item);

    void deleteAllByItem(JPAItem item);
}
