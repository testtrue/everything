package de.lehrke.boas.everything.inventory.database;

import de.lehrke.boas.everything.inventory.workflows.ItemDTO;
import de.lehrke.boas.everything.inventory.workflows.ItemDataService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class ItemDataServiceImpl implements ItemDataService {

    private final ItemRepository itemRepository;

    private final ItemProductCodeRepository itemProductCodeRepository;

    private final Function<JPAItem, ItemDTO> mapper;

    public ItemDataServiceImpl(ItemRepository inventoryItemRepository,
            ItemProductCodeRepository itemProductCodeRepository) {
        this.itemRepository = inventoryItemRepository;
        this.itemProductCodeRepository = itemProductCodeRepository;
        this.mapper = (i) -> ItemDTO.builder()
                .id(i.getId()).name(i.getName())
                .threshhold(i.getThreshhold()).availableAmount(i.getAvailableAmount())
                .barcodes(
                        this.itemProductCodeRepository.findAllByItem(i).stream().map(JPAItemProductCode::getProductCode)
                                .toList())
                .build();
    }

    @Override
    public List<ItemDTO> findAll() {
        return this.itemRepository.findAll().stream().map(this.mapper).toList();
    }

    @Override
    public ItemDTO save(ItemDTO item) {
        JPAItem dtoToReturn = this.itemRepository.save(new JPAItem(item));
        this.itemProductCodeRepository.deleteAllByItem(dtoToReturn);
        item.barcodes().stream().map(b -> new JPAItemProductCode(b, dtoToReturn))
                .forEach(this.itemProductCodeRepository::save);
        return mapper.apply(dtoToReturn);
    }

    @Override
    public Optional<ItemDTO> findById(String id) {
        return this.itemRepository.findById(id).map(this.mapper);
    }

    @Override
    public Optional<ItemDTO> findByProductCode(String productCode) {
        return this.itemProductCodeRepository.findByProductCode(productCode).map(pc -> pc.getItem()).map(this.mapper);
    }

}
