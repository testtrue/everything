package de.lehrke.boas.everything.inventory.database;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "item_productcode")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JPAItemProductCode {

    @Id
    private String productCode;

    @ManyToOne
    private JPAItem item;
}
