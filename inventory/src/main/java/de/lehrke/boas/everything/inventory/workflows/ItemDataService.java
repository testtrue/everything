package de.lehrke.boas.everything.inventory.workflows;

import java.util.List;
import java.util.Optional;

public interface ItemDataService {
    
    List<ItemDTO> findAll();
    ItemDTO save(ItemDTO item);
    Optional<ItemDTO> findById(String id);

    Optional<ItemDTO> findByProductCode(String productCode);
}
