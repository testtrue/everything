package de.lehrke.boas.everything.budgetmanagement.usecase;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BudgetStoryTests extends AbstractBudgetManagementTests {

    @Test
    public void testCreateNewBudget() {
        // Given -----------------------------------------------
        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        int amountBeforeCreation = this.budgetRepository.getAllBudgets().size();
        Month period = Month.create(LocalDate.now());

        // When ------------------------------------------------
        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");

        budgetService.addBudget(budget, period);

        // Then ------------------------------------------------
        assertEquals(0, amountBeforeCreation);
        assertEquals(1, this.budgetRepository.getAllBudgets().size());
        Budget newBudget = budgetService.getBudgetByName("Test", period);
        assertNotNull(newBudget);
        assertEquals(budget.getAmount(), newBudget.getAmount());
        assertEquals(budget.getName(), newBudget.getName());
        assertNull(newBudget.getBudgetCategory());
        assertNull(budget.getId());
        assertNotNull(newBudget.getId());
    }

    @Test
    public void testCreateNewBudgetWithAlreadyExisitingBudget() {
        // Given -----------------------------------------------
        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        int amountBeforeCreation = this.budgetRepository.getAllBudgets().size();
        Month period = Month.create(LocalDate.now());

        // When ------------------------------------------------
        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");

        budgetService.addBudget(budget, period);
        budgetService.addBudget(budget, period);

        // Then ------------------------------------------------
        assertEquals(0, amountBeforeCreation);
        assertEquals(1, this.budgetRepository.getAllBudgets().size());
        Budget newBudget = budgetService.getBudgetByName("Test", period);
        assertNotNull(newBudget);
        assertEquals(budget.getAmount(), newBudget.getAmount());
        assertEquals(budget.getName(), newBudget.getName());
        assertNull(newBudget.getBudgetCategory());
        assertNull(budget.getId());
        assertNotNull(newBudget.getId());
    }

    @Test
    public void testGetAllBudgetsOfACategoryWithRemainingAmount() {
        Month period = Month.create(LocalDate.now());

        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setName("Hello World");
        budgetCategory = budgetCategoryRepository.save(budgetCategory);

        Budget budget = new Budget();
        budget.setBudgetCategory(budgetCategory);
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");
        budget = budgetRepository.save(budget);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(50d));
        posten.setName("Test");
        posten.setId(0l);
        posten.setDate(LocalDateTime.now());

        this.itemRepository.save(posten);

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);

        List<Budget> budgets = budgetService.getAllBudgetsByBudgetCategory(budgetCategory, period);
        
        assertEquals(1, budgets.size());
        Budget dto = budgets.get(0);
        assertEquals("Test", dto.getName());
        assertEquals(100d, dto.getAmount().doubleValue());
        assertEquals(50d, dto.getRemainingAmount().doubleValue());
        assertEquals("Hello World", dto.getBudgetCategory().getName());
    }

    @Test
    public void testGetAllBudgetsOfANullCategoryWithRemainingAmount() {
        BudgetCategory budgetCategory = null;
        Month period = Month.create(LocalDate.now());

        Budget budget = new Budget();
        budget.setBudgetCategory(budgetCategory);
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");
        budget = budgetRepository.save(budget);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(50d));
        posten.setName("Test");
        posten.setId(0l);
        posten.setDate(LocalDateTime.now());

        this.itemRepository.save(posten);

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);

        List<Budget> budgets = budgetService.getAllBudgetsByBudgetCategory(budgetCategory, period);
        
        assertEquals(1, budgets.size());
        Budget dto = budgets.get(0);
        assertEquals("Test", dto.getName());
        assertEquals(100d, dto.getAmount().doubleValue());
        assertEquals(50d, dto.getRemainingAmount().doubleValue());
        assertNull(dto.getBudgetCategory());
    }

    @Test
    public void testGetAllBudgetsOfACategoryWithRemainingAmountWithMultipleCategories() {
        Month period = Month.create(LocalDate.now());
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setName("Hello World");
        budgetCategory = budgetCategoryRepository.save(budgetCategory);
        BudgetCategory budgetCategory2 = new BudgetCategory();
        budgetCategory2.setName("Test2");
        budgetCategory2 = budgetCategoryRepository.save(budgetCategory2);

        Budget budget = new Budget();
        budget.setBudgetCategory(budgetCategory);
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");
        budget = budgetRepository.save(budget);
        Budget budget2 = new Budget();
        budget2.setBudgetCategory(budgetCategory);
        budget2.setAmount(BigDecimal.valueOf(25d));
        budget2.setName("Test2");
        budget2 = budgetRepository.save(budget2);
        Budget budget3 = new Budget();
        budget3.setBudgetCategory(budgetCategory2);
        budget3.setAmount(BigDecimal.valueOf(50d));
        budget3.setName("Test3");
        budget3 = budgetRepository.save(budget3);
        Budget budget4 = new Budget();
        budget4.setBudgetCategory(budgetCategory2);
        budget4.setAmount(BigDecimal.valueOf(200d));
        budget4.setName("Test4");
        budget4 = budgetRepository.save(budget4);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(50d));
        posten.setName("Test");
        posten.setDate(LocalDateTime.now());
        Item posten5 = new Item();
        posten5.setBudget(budget);
        posten5.setAmount(BigDecimal.valueOf(25d));
        posten5.setName("Test");
        posten5.setDate(LocalDateTime.now());
        Item posten2 = new Item();
        posten2.setBudget(budget2);
        posten2.setAmount(BigDecimal.valueOf(24.99d));
        posten2.setName("Test2");
        posten2.setDate(LocalDateTime.now());
        Item posten3 = new Item();
        posten3.setBudget(budget3);
        posten3.setAmount(BigDecimal.valueOf(55d));
        posten3.setName("Test3");
        posten3.setDate(LocalDateTime.now());
        Item posten4 = new Item();
        posten4.setBudget(budget4);
        posten4.setAmount(BigDecimal.valueOf(150d));
        posten4.setName("Test4");
        posten4.setDate(LocalDateTime.now());

        posten = this.itemRepository.save(posten);
        posten2 = this.itemRepository.save(posten2);
        posten3 = this.itemRepository.save(posten3);
        posten4 = this.itemRepository.save(posten4);
        posten5 = this.itemRepository.save(posten5);

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);

        List<Budget> budgetsOfBudgetCategory = budgetService.getAllBudgetsByBudgetCategory(budgetCategory, period);
        List<Budget> budgetsOfBudgetCategory2 = budgetService.getAllBudgetsByBudgetCategory(budgetCategory2, period);
        
        assertEquals(2, budgetsOfBudgetCategory.size());
        Budget dto = budgetsOfBudgetCategory.get(0);
        assertEquals("Test", dto.getName());
        assertEquals(100d, dto.getAmount().doubleValue());
        assertEquals(25, dto.getRemainingAmount().doubleValue());
        assertEquals("Hello World", dto.getBudgetCategory().getName());

        Budget dto2 = budgetsOfBudgetCategory.get(1);
        assertEquals("Test2", dto2.getName());
        assertEquals(25d, dto2.getAmount().doubleValue());
        assertEquals(0.01d, dto2.getRemainingAmount().doubleValue());
        assertEquals("Hello World", dto2.getBudgetCategory().getName());

        assertEquals(2, budgetsOfBudgetCategory2.size());

        Budget dto4 = budgetsOfBudgetCategory2.get(1);
        assertEquals("Test4", dto4.getName());
        assertEquals(200d, dto4.getAmount().doubleValue());
        assertEquals(50d, dto4.getRemainingAmount().doubleValue());
        assertEquals("Test2", dto4.getBudgetCategory().getName());

        Budget dto3 = budgetsOfBudgetCategory2.get(0);
        assertEquals("Test3", dto3.getName());
        assertEquals(50d, dto3.getAmount().doubleValue());
        assertEquals(-5d, dto3.getRemainingAmount().doubleValue());
        assertEquals("Test2", dto3.getBudgetCategory().getName());

    }
    
}
