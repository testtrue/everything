package de.lehrke.boas.everything.budgetmanagement.usecase;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import de.lehrke.boas.everything.budgetmanagement.service.ItemService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ItemStoryTests extends AbstractBudgetManagementTests{


    @Test
    public void testCreationOfItemInBudget() {
        // Given -----------------------------------
        Long id = 1l;
        Month period = Month.create(LocalDate.now());

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(50.00d));
        budget.setName("Freizeit");
        budget.setBudgetCategory(null);
        budget = budgetRepository.save(budget);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(14.99d));
        posten.setName("Kino");
        posten.setDate(LocalDateTime.now());

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        ItemService itemService = new ItemService(itemRepository);

        // When ---------------------------------------
        id = itemService.addItem(posten).getId();

        // Then ---------------------------------------
        List<Budget> budgetDtos = budgetService.getAllBudgetsByBudgetCategory(null, period);
        assertEquals(1, budgetDtos.size());
        Budget b = budgetDtos.get(0);
        assertEquals(BigDecimal.valueOf(35.01d), b.getRemainingAmount());
        assertEquals("Freizeit", b.getName());
        assertEquals(budget.getAmount(), b.getAmount());


        List<Item> items = itemService.getAllItemsByBudget(budget, period);
        assertEquals(1, items.size());
        Item item = items.get(0);
        assertNotNull(item);
        assertEquals("Kino", item.getName());
        assertEquals(14.99d, item.getAmount().doubleValue());
        assertEquals(budget, item.getBudget());

        item = itemService.getItemById(id).get();
        assertNotNull(item);
        assertEquals("Kino", item.getName());
        assertEquals(14.99d, item.getAmount().doubleValue());
        assertEquals(budget, item.getBudget());

    }

    @Test
    public void testCreationOfMultipleItemsInBudget() {
        // Given -----------------------------------
        Long id = 1l;
        Month period = Month.create(LocalDate.now());

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(50d));
        budget.setName("Freizeit");
        budget = budgetRepository.save(budget);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(14.99d));
        posten.setName("Kino");
        posten.setId(id);
        posten.setDate(LocalDateTime.now());

        Item posten2 = new Item();
        posten2.setBudget(budget);
        posten2.setAmount(BigDecimal.valueOf(14.99d));
        posten2.setName("Kino");
        posten2.setId(id + 1);
        posten2.setDate(LocalDateTime.now());

        

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        ItemService itemService = new ItemService(itemRepository);

        // When ---------------------------------------
        itemService.addItem(posten);
        itemService.addItem(posten2);

        // Then ---------------------------------------

        List<Budget> budgets = budgetService.getAllBudgetsByBudgetCategory(null, period);
        assertEquals(1, budgets.size());
        Budget budgetDto = budgets.get(0);
        assertEquals(BigDecimal.valueOf(20.02d), budgetDto.getRemainingAmount());
        assertEquals("Freizeit", budgetDto.getName());

        List<Item> items = itemService.getAllItemsByBudget(budget, period);
        assertEquals(2, items.size());

    }

    @Test
    public void testCreationOfMultipleItemsInMultipleBudgets() {
        // Given -----------------------------------
        Month period = Month.create(LocalDate.now());
        Long id = 1l;

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(50d));
        budget.setName("Freizeit");

        Budget budget2 = new Budget();
        budget2.setAmount(BigDecimal.valueOf(35d));
        budget2.setName("Essen");

        budget = budgetRepository.save(budget);
        budget2 = budgetRepository.save(budget2);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(14.99d));
        posten.setName("Kino");
        posten.setId(id);
        posten.setDate(LocalDateTime.now());

        Item posten2 = new Item();
        posten2.setBudget(budget2);
        posten2.setAmount(BigDecimal.valueOf(14.99d));
        posten2.setName("Popcorn");
        posten2.setId(id + 1);
        posten2.setDate(LocalDateTime.now());




        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        ItemService itemService = new ItemService(itemRepository);

        // When ---------------------------------------
        itemService.addItem(posten);
        itemService.addItem(posten2);

        // Then ---------------------------------------
        List<Budget> budgetDtos = budgetService.getAllBudgetsByBudgetCategory(null, period);
        assertEquals(2, budgetDtos.size());
        Budget budgetDto = budgetDtos.get(0);
        assertEquals(BigDecimal.valueOf(35.01d), budgetDto.getRemainingAmount());
        assertEquals("Freizeit", budgetDto.getName());

        Budget budgetDto2 = budgetDtos.get(1);
        assertEquals(BigDecimal.valueOf(20.01d), budgetDto2.getRemainingAmount());
        assertEquals("Essen", budgetDto2.getName());

        List<Item> items = itemService.getAllItemsByBudget(budget, period);
        assertEquals(1, items.size());
        Item item = items.get(0);
        assertNotNull(item);
        assertEquals("Kino", item.getName());
        assertEquals(14.99d, item.getAmount().doubleValue());
        assertEquals(budget, item.getBudget());

        items = itemService.getAllItemsByBudget(budget2, period);
        assertEquals(1, items.size());
        item = items.get(0);
        assertNotNull(item);
        assertEquals("Popcorn", item.getName());
        assertEquals(14.99d, item.getAmount().doubleValue());
        assertEquals(budget2, item.getBudget());
    }

    @Test
    public void testUdpateOfItem() {
        // Given -----------------------------------
        Long id = 0l;
        Month period = Month.create(LocalDate.now());

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(50d));
        budget.setName("Freizeit");
        budget = budgetRepository.save(budget);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(14.99d));
        posten.setName("Kino");
        posten.setId(id);
        posten.setDate(LocalDateTime.now());

        super.itemRepository.save(posten);


        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);

        List<Budget> budgetDtosBeforeChange = budgetService.getAllBudgetsByBudgetCategory(null, period);


        // When ---------------------------------------
        Budget budgetToChange = budgetService.getBudgetByName("Freizeit", period);
        budgetToChange.setAmount(BigDecimal.valueOf(40d));
        budgetService.updateBudget(budgetToChange);

        // Then ---------------------------------------
        assertEquals(1, budgetDtosBeforeChange.size());
        Budget budgetDtoBeforeChange = budgetDtosBeforeChange.get(0);
        assertEquals(BigDecimal.valueOf(35.01d), budgetDtoBeforeChange.getRemainingAmount());
        List<Budget> budgetDtos = budgetService.getAllBudgetsByBudgetCategory(null, period);
        assertEquals(1, budgetDtos.size());
        Budget budgetDto = budgetDtos.get(0);
        assertEquals(BigDecimal.valueOf(25.01d), budgetDto.getRemainingAmount());
        assertEquals("Freizeit", budgetDto.getName());

    }

    @Test
    public void testItemsFromDifferentMonthsGetJustOne() {
        // Given -----------------------------------
        Long id = 1l;
        Month period = Month.create(LocalDate.now());

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(50d));
        budget.setName("Freizeit");

        Budget budget2 = new Budget();
        budget2.setAmount(BigDecimal.valueOf(35d));
        budget2.setName("Essen");

        budget = budgetRepository.save(budget);
        budget2 = budgetRepository.save(budget2);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(14.99d));
        posten.setName("Kino");
        posten.setId(id);
        posten.setDate(LocalDateTime.now());

        Item posten2 = new Item();
        posten2.setBudget(budget2);
        posten2.setAmount(BigDecimal.valueOf(14.99d));
        posten2.setName("Popcorn");
        posten2.setId(id + 1);
        posten2.setDate(LocalDateTime.now().minusMonths(2));

        BudgetService budgetService = new BudgetService(budgetRepository, itemRepository);
        ItemService itemService = new ItemService(itemRepository);

        // When ---------------------------------------
        itemService.addItem(posten);
        itemService.addItem(posten2);

        // Then ---------------------------------------
        List<Budget> budgetDtos = budgetService.getAllBudgetsByBudgetCategory(null, period);
        assertEquals(2, budgetDtos.size());
        Budget budgetDto = budgetDtos.get(0);
        assertEquals(BigDecimal.valueOf(35.01d), budgetDto.getRemainingAmount());
        assertEquals("Freizeit", budgetDto.getName());

        Budget budgetDto2 = budgetDtos.get(1);
        assertEquals(BigDecimal.valueOf(35d).setScale(2), budgetDto2.getRemainingAmount());
        assertEquals("Essen", budgetDto2.getName());

        List<Item> items = itemService.getAllItemsByBudget(budget, period);
        assertEquals(1, items.size());
        Item item = items.get(0);
        assertNotNull(item);
        assertEquals("Kino", item.getName());
        assertEquals(14.99d, item.getAmount().doubleValue());
        assertEquals(budget, item.getBudget());

        items = itemService.getAllItemsByBudget(budget2, period);
        assertEquals(0, items.size());

        items = itemService.getAllItemsByBudget(budget2, Month.create(LocalDate.now().minusMonths(2)));
        assertEquals(1, items.size());
    }
    
}
