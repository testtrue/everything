package de.lehrke.boas.everything.budgetmanagement.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BudgetTest {
    
    @Test
    public void testBudgetObject() {

        // Given
        double amount = 2.00;
        String name = "Test";
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setId(0l);
        budgetCategory.setName("Testcategory");

        // When
        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(amount));
        budget.setName(name);
        budget.setBudgetCategory(budgetCategory);

        // Then
        assertEquals(amount, budget.getAmount().doubleValue());
        assertEquals(name, budget.getName());
        assertEquals(budgetCategory, budget.getBudgetCategory());

    }

}
