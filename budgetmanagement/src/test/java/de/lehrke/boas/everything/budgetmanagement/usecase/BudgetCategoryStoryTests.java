package de.lehrke.boas.everything.budgetmanagement.usecase;

import de.lehrke.boas.everything.budgetmanagement.controller.BudgetCategoryController;
import de.lehrke.boas.everything.budgetmanagement.controller.BudgetCategoryDto;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryService;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


public class BudgetCategoryStoryTests extends AbstractBudgetManagementTests {
    
    @Test
    public void testGetAllBudgetCategoriesWithRemainingAmount() {
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setName("Hello World");
        budgetCategory = budgetCategoryRepository.save(budgetCategory);
        Budget budget = new Budget();
        budget.setBudgetCategory(budgetCategory);
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");
        budget = budgetRepository.save(budget);
        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(50d));
        posten.setDate(LocalDateTime.now());
        posten.setName("Test");

        super.itemRepository.save(posten);

        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> categories = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        assertEquals(1, categories.size());
        
        BudgetCategoryDto dto = categories.get(0);
        
        assertEquals(100d, dto.getAmount().doubleValue());
        assertEquals("Hello World", dto.getName());
        assertEquals(50d, dto.getRemainingAmount().doubleValue());

    }

    @Test
    public void testGetAllBudgetCategoriesWithRemainingAmountAndMultipleCategories() {
        // Given ----------------------------------
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setName("Hello World");
        budgetCategory = budgetCategoryRepository.save(budgetCategory);
        BudgetCategory budgetCategory2 = new BudgetCategory();
        budgetCategory2.setName("Test2");
        budgetCategory2 = budgetCategoryRepository.save(budgetCategory2);
        Budget budget = new Budget();
        budget.setBudgetCategory(budgetCategory);
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setName("Test");
        budget = budgetRepository.save(budget);
        Budget budget2 = new Budget();
        budget2.setBudgetCategory(budgetCategory);
        budget2.setAmount(BigDecimal.valueOf(25d));
        budget2.setName("Test2");
        budget2 = budgetRepository.save(budget2);
        Budget budget3 = new Budget();
        budget3.setBudgetCategory(budgetCategory2);
        budget3.setAmount(BigDecimal.valueOf(50d));
        budget3.setName("Test3");
        budget3 = budgetRepository.save(budget3);
        Budget budget4 = new Budget();
        budget4.setBudgetCategory(budgetCategory2);
        budget4.setAmount(BigDecimal.valueOf(200d));
        budget4.setName("Test4");
        budget4 = budgetRepository.save(budget4);

        Item posten = new Item();
        posten.setBudget(budget);
        posten.setAmount(BigDecimal.valueOf(50d));
        posten.setName("Test");
        posten.setId(0l);
        posten.setDate(LocalDateTime.now());
        Item posten5 = new Item();
        posten5.setBudget(budget);
        posten5.setAmount(BigDecimal.valueOf(25d));
        posten5.setName("Test");
        posten5.setId(5l);
        posten5.setDate(LocalDateTime.now());
        Item posten2 = new Item();
        posten2.setBudget(budget2);
        posten2.setAmount(BigDecimal.valueOf(24.99d));
        posten2.setName("Test2");
        posten2.setId(2l);
        posten2.setDate(LocalDateTime.now());
        Item posten3 = new Item();
        posten3.setBudget(budget3);
        posten3.setAmount(BigDecimal.valueOf(55d));
        posten3.setName("Test3");
        posten3.setId(3l);
        posten3.setDate(LocalDateTime.now());
        Item posten4 = new Item();
        posten4.setBudget(budget4);
        posten4.setAmount(BigDecimal.valueOf(150d));
        posten4.setName("Test4");
        posten4.setId(4l);
        posten4.setDate(LocalDateTime.now());

        this.itemRepository.save(posten);
        this.itemRepository.save(posten2);
        this.itemRepository.save(posten3);
        this.itemRepository.save(posten4);
        this.itemRepository.save(posten5);


        // When ------------------------------------------------
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> categories = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        // When -------------------------------------------------
        assertEquals(2, categories.size());
        
        BudgetCategoryDto dto = categories.get(0);
        
        assertEquals("Hello World", dto.getName());
        assertEquals(125d, dto.getAmount().doubleValue());
        assertEquals(25,01d, dto.getRemainingAmount().doubleValue());

        BudgetCategoryDto dto2 = categories.get(1);
        assertEquals("Test2", dto2.getName());
        assertEquals(250d, dto2.getAmount().doubleValue());
        assertEquals(45d, dto2.getRemainingAmount().doubleValue());

    }

    @Test
    public void testCreateNewBudgetCategory() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null,0d,"Test",0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> budgetCategoriesBefore = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        // When ------------------------------------------------------
        budgetCategoryController.createBudgetCategory(budgetCategory);

        // Then ------------------------------------------------------
        BudgetCategoryDto budgetCategory2 = budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).get(0);
        assertNotNull(budgetCategory2);
        assertEquals("Test", budgetCategory2.getName());
        assertEquals(0d, budgetCategory2.getAmount());
        assertEquals(0d, budgetCategory2.getRemainingAmount());
        List<BudgetCategoryDto> budgetCategoriesAfter = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());
        assertEquals(0, budgetCategoriesBefore.size());
        assertEquals(1, budgetCategoriesAfter.size());
        BudgetCategoryDto budgetCategoryDto = budgetCategoriesAfter.get(0);
        assertEquals("Test", budgetCategoryDto.getName());
        assertEquals(0d, budgetCategoryDto.getAmount());
        assertEquals(0d, budgetCategoryDto.getRemainingAmount());
    }

    @Test
    public void testCreateNewBudgetCategoryWithInitalValues() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null,10d,"Test",0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> budgetCategoriesBefore = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        // When ------------------------------------------------------
        budgetCategoryController.createBudgetCategory(budgetCategory);

        // Then ------------------------------------------------------
        BudgetCategoryDto budgetCategory2 = budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).get(0);
        assertNotNull(budgetCategory2);
        assertEquals("Test", budgetCategory2.getName());
        assertEquals(0d, budgetCategory2.getAmount());
        assertEquals(0d, budgetCategory2.getRemainingAmount());
        List<BudgetCategoryDto> budgetCategoriesAfter = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());
        assertEquals(0, budgetCategoriesBefore.size());
        assertEquals(1, budgetCategoriesAfter.size());
        BudgetCategoryDto budgetCategoryDto = budgetCategoriesAfter.get(0);
        assertEquals("Test", budgetCategoryDto.getName());
        assertEquals(0d, budgetCategoryDto.getAmount());
        assertEquals(0d, budgetCategoryDto.getRemainingAmount());
    }


    @Test
    public void testCreateNewBudgetCategoryWithSameExistingCategory() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null,0d,"Test",0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> budgetCategoriesBefore = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        // When ------------------------------------------------------
        budgetCategoryController.createBudgetCategory(budgetCategory);
        budgetCategoryController.createBudgetCategory(budgetCategory);

        // Then ------------------------------------------------------
        List<BudgetCategoryDto> budgetCategoriesAfter = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());
        assertEquals(0, budgetCategoriesBefore.size());
        assertEquals(1, budgetCategoriesAfter.size());
        BudgetCategoryDto budgetCategoryDto = budgetCategoriesAfter.get(0);
        assertEquals("Test", budgetCategoryDto.getName());
        assertEquals(0d, budgetCategoryDto.getAmount());
        assertEquals(0d, budgetCategoryDto.getRemainingAmount());
    }

    @Test
    public void testCreateNewBudgetCategoryWithExistingCategory() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null,0d,"Test",0d);

        BudgetCategoryDto budgetCategory2 = new BudgetCategoryDto(null,0d,"Test2",0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        List<BudgetCategoryDto> budgetCategoriesBefore = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());

        // When ------------------------------------------------------
        budgetCategoryController.createBudgetCategory(budgetCategory);
        List<BudgetCategoryDto> budgetCategoriesAfterFirstCreation = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());
        budgetCategoryController.createBudgetCategory(budgetCategory2);

        // Then ------------------------------------------------------
        List<BudgetCategoryDto> budgetCategoriesAfter = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.empty());
        assertEquals(0, budgetCategoriesBefore.size());
        assertEquals(1, budgetCategoriesAfterFirstCreation.size());
        assertEquals(2, budgetCategoriesAfter.size());
        BudgetCategoryDto budgetCategoryDto = budgetCategoriesAfter.get(0);
        assertEquals("Test", budgetCategoryDto.getName());
        assertEquals(0d, budgetCategoryDto.getAmount());
        assertEquals(0d, budgetCategoryDto.getRemainingAmount());
        BudgetCategoryDto budgetCategoryDto2 = budgetCategoriesAfter.get(1);
        assertEquals("Test2", budgetCategoryDto2.getName());
        assertEquals(0d, budgetCategoryDto2.getAmount());
        assertEquals(0d, budgetCategoryDto2.getRemainingAmount());
    }

    @Test
    public void testUpdateOfBudgetCategory() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null,0d,"Test",0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        budgetCategoryController.createBudgetCategory(budgetCategory);

        // When ------------------------------------------------------
        BudgetCategoryDto givenBudgetCategoryDto = budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).get(0);
        assertNotNull(givenBudgetCategoryDto);
        BudgetCategoryDto givenBudgetCategory = new BudgetCategoryDto(givenBudgetCategoryDto.getId(),0d, "Test2",0d);
        budgetCategoryController.updateBudgetCategory(givenBudgetCategory.getId(), givenBudgetCategory);

        // Then ------------------------------------------------------
        assertEquals(0, budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).size());
        assertNotNull(budgetCategoryController.getAllBudgetCategories(Optional.of("Test2"), Optional.empty()).get(0));
    }

    @Test
    public void testUpdateOfBudgetCategoryWithoutChanges() {
        // Given -----------------------------------------------------
        BudgetCategoryDto budgetCategory = new BudgetCategoryDto(null, 0d, "Test", 0d);
        
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        budgetCategoryController.createBudgetCategory(budgetCategory);

        // When ------------------------------------------------------
        BudgetCategoryDto givenBudgetCategoryDto = budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).get(0);
        assertNotNull(givenBudgetCategoryDto);
        BudgetCategoryDto givenBudgetCategory = new BudgetCategoryDto(givenBudgetCategoryDto.getId(), 0d, "Test", 0d);
        budgetCategoryController.updateBudgetCategory(givenBudgetCategory.getId(),givenBudgetCategory);

        // Then ------------------------------------------------------
        assertNotNull(budgetCategoryController.getAllBudgetCategories(Optional.of("Test"), Optional.empty()).get(0));
    }

    @Test
    public void testChangeBudgetCategoryOfBudget() {
        // Given ------------------------------------------------------
        BudgetService budgetService = new BudgetService(this.budgetRepository, this.itemRepository);

        BudgetCategoryController budgetCategoryController = new BudgetCategoryController(
                                                                new BudgetCategoryService(this.budgetCategoryRepository, budgetService),
                                                                budgetService);

        Month period = Month.create(LocalDate.now());

        BudgetCategory budgetCategory1 = new BudgetCategory();
        budgetCategory1.setName("Test");
        budgetCategory1 = this.budgetCategoryRepository.save(budgetCategory1);
        BudgetCategory budgetCategory2 = new BudgetCategory();
        budgetCategory2.setName("Test2");
        budgetCategory2 = this.budgetCategoryRepository.save(budgetCategory2);

        Budget budget = new Budget();
        budget.setAmount(BigDecimal.valueOf(100d));
        budget.setBudgetCategory(budgetCategory1);
        budget.setName("TestBudget");
        budgetService.addBudget(budget, period);

        List<Budget> budgetsInBudgetCategory1 = budgetService.getAllBudgetsByBudgetCategory(budgetCategory1, period);
        List<Budget> budgetsInBudgetCategory2 = budgetService.getAllBudgetsByBudgetCategory(budgetCategory2, period);
        List<BudgetCategoryDto> budgetCategoryDtosBeforeChange = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.of(period.initialDate.toLocalDate()));


        // When -------------------------------------------------------
        Budget storedBudget = budgetService.getBudgetByName("TestBudget", period);
        storedBudget.setBudgetCategory(budgetCategory2);
        budgetService.updateBudget(storedBudget);

        // Then -------------------------------------------------------
        assertEquals(1, budgetsInBudgetCategory1.size());
        assertEquals(0, budgetsInBudgetCategory2.size());
        assertEquals(2, budgetCategoryDtosBeforeChange.size());
        BudgetCategoryDto budgetCategoryDto1 = budgetCategoryDtosBeforeChange.get(0);
        BudgetCategoryDto budgetCategoryDto2 = budgetCategoryDtosBeforeChange.get(1);

        assertEquals("Test", budgetCategoryDto1.getName());
        assertEquals(100d, budgetCategoryDto1.getAmount());
        assertEquals(100d, budgetCategoryDto1.getRemainingAmount());

        assertEquals("Test2", budgetCategoryDto2.getName());
        assertEquals(0d, budgetCategoryDto2.getAmount());
        assertEquals(0d, budgetCategoryDto2.getRemainingAmount());

        budgetsInBudgetCategory1 = budgetService.getAllBudgetsByBudgetCategory(budgetCategory1, period);
        budgetsInBudgetCategory2 = budgetService.getAllBudgetsByBudgetCategory(budgetCategory2, period);
        budgetCategoryDtosBeforeChange = budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.of(period.initialDate.toLocalDate()));

        assertEquals(0, budgetsInBudgetCategory1.size());
        assertEquals(1, budgetsInBudgetCategory2.size());
        assertEquals(2, budgetCategoryDtosBeforeChange.size());
        budgetCategoryDto1 = budgetCategoryDtosBeforeChange.get(0);
        budgetCategoryDto2 = budgetCategoryDtosBeforeChange.get(1);

        assertEquals("Test", budgetCategoryDto1.getName());
        assertEquals(0d, budgetCategoryDto1.getAmount());
        assertEquals(0d, budgetCategoryDto1.getRemainingAmount());

        assertEquals("Test2", budgetCategoryDto2.getName());
        assertEquals(100d, budgetCategoryDto2.getAmount());
        assertEquals(100d, budgetCategoryDto2.getRemainingAmount());
    }

}
