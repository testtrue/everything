package de.lehrke.boas.everything.budgetmanagement.database;

import de.lehrke.boas.everything.budgetmanagement.database.utils.Mappers;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ModelMapperTests {
    
    @Test
    public void testBudgetIntoDbBudget() {
        // GIVEN
        de.lehrke.boas.everything.budgetmanagement.model.Budget modelBudget = new de.lehrke.boas.everything.budgetmanagement.model.Budget();
        modelBudget.setRemainingAmount(BigDecimal.valueOf(99L));
        modelBudget.setAmount(BigDecimal.valueOf(100L));
        modelBudget.setId(1L);
        modelBudget.setName("Test");

        // WHEN
        de.lehrke.boas.everything.budgetmanagement.database.Budget dbBudget = Mappers.mapBudgetToDBModel.apply(modelBudget);

        // THEN
        assertEquals(modelBudget.getAmount(), dbBudget.getAmount());
        assertEquals(modelBudget.getId(), dbBudget.getId());
        assertEquals(modelBudget.getName(), dbBudget.getName());

    }
}
