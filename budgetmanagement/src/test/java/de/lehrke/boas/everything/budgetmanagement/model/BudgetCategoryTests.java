package de.lehrke.boas.everything.budgetmanagement.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BudgetCategoryTests {

    @Test
    public void testBudgetCategoryObject() {
        String name = "Test";
        Long id = 0l;
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setId(id);
        budgetCategory.setName(name);

        assertEquals(id, budgetCategory.getId());
        assertEquals(name, budgetCategory.getName());
        
    }
    
}
