package de.lehrke.boas.everything.budgetmanagement.usecase;

import de.lehrke.boas.everything.budgetmanagement.controller.BudgetCategoryDto;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BudgetCategoryDtoTests {
    
    @Test
    public void testBudgetCategoryDto() {
        Double amount = 0d;
        String name = "TestCategory";
        Double remainingAmount = 0d;

        BudgetCategoryDto budgetCategoryDto = new BudgetCategoryDto(0l ,amount, name, remainingAmount);

        assertEquals(amount, budgetCategoryDto.getAmount());
        assertEquals(name, budgetCategoryDto.getName());
        assertEquals(remainingAmount, budgetCategoryDto.getRemainingAmount());

    }
}
