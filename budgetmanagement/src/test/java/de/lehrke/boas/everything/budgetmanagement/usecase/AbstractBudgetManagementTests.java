package de.lehrke.boas.everything.budgetmanagement.usecase;

import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryRepository;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetRepository;
import de.lehrke.boas.everything.budgetmanagement.service.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

import java.time.LocalDate;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractBudgetManagementTests {
    
    @Autowired
    protected BudgetCategoryRepository budgetCategoryRepository;

    @Autowired
    protected BudgetRepository budgetRepository;

    @Autowired
    protected ItemRepository itemRepository;

    protected Month period = Month.create(LocalDate.now());

}
