package de.lehrke.boas.everything.budgetmanagement.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BudgetCategoryRepository extends JpaRepository<BudgetCategory, Long>{

    BudgetCategory findByName(String name);
}
