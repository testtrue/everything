package de.lehrke.boas.everything.budgetmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class BudgetManagementApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BudgetManagementApplication.class, args);
	}

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BudgetManagementApplication.class);
    }

}
