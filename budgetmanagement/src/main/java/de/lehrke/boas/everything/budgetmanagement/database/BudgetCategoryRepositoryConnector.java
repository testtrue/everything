package de.lehrke.boas.everything.budgetmanagement.database;

import de.lehrke.boas.everything.budgetmanagement.database.utils.Mappers;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BudgetCategoryRepositoryConnector implements de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryRepository {

    @Autowired
    private BudgetCategoryRepository budgetCategoryRepository;

    @Override
    public List<de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory> getAllBudgetCategories() {
        return budgetCategoryRepository.findAll().stream().map(Mappers.mapBudgetCategoryToDomainModel).collect(Collectors.toList());
    }

    @Override
    public Optional<de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory> getBudgetCategoryByName(String name) {
        return Optional.ofNullable(budgetCategoryRepository.findByName(name)).map(Mappers.mapBudgetCategoryToDomainModel);
    }

    @Override
    public de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory save(de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory budgetCategory) {
        return Mappers.mapBudgetCategoryToDomainModel.apply(budgetCategoryRepository.save(Mappers.mapBudgetCategoryToDBModel.apply(budgetCategory)));
    }

    @Override
    public Optional<BudgetCategory> getBudgetCategoryById(Long id) {
        return budgetCategoryRepository.findById(id).map(Mappers.mapBudgetCategoryToDomainModel);
    }

    @Override
    public void deleteById(Long id) {
        budgetCategoryRepository.deleteById(id);
        
    }
    
}
