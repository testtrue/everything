package de.lehrke.boas.everything.budgetmanagement.model;

import lombok.Data;

/**
 * BudgetCategory
 */
@Data
public class BudgetCategory {

    private Long id;
    private String name;
}