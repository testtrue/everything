package de.lehrke.boas.everything.budgetmanagement.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long>{

    Optional<Budget> findByName(String name);

    List<Budget> findAllByBudgetCategory(BudgetCategory budgetCategory);
}
