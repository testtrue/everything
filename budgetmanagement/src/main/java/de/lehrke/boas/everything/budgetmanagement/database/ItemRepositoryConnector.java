package de.lehrke.boas.everything.budgetmanagement.database;

import de.lehrke.boas.everything.budgetmanagement.database.utils.Mappers;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ItemRepositoryConnector implements de.lehrke.boas.everything.budgetmanagement.service.ItemRepository {

    @Autowired
    private ItemRepository itemRepository;
    
    @Override
    public List<de.lehrke.boas.everything.budgetmanagement.model.Item> getAllItems() {
        return itemRepository.findAll().stream().map(Mappers.mapItemToDomainModel).collect(Collectors.toList());
    }

    @Override
    public List<de.lehrke.boas.everything.budgetmanagement.model.Item> getAllItemsByBudget(Budget budget, Month period) {
        return itemRepository.findAllByBudgetAndDateBetween(Mappers.mapBudgetToDBModel.apply(budget), period.beginningOfMonth, period.endOfMonth).stream().map(Mappers.mapItemToDomainModel).collect(Collectors.toList());
    }

    @Override
    public Optional<de.lehrke.boas.everything.budgetmanagement.model.Item> getItemById(Long id) {
        return itemRepository.findById(id.longValue()).map(Mappers.mapItemToDomainModel);
    }

    @Override
    public de.lehrke.boas.everything.budgetmanagement.model.Item save(Item item) {
        return Mappers.mapItemToDomainModel.apply(itemRepository.save(Mappers.mapItemToDBModel.apply(item)));
    }
    
}
