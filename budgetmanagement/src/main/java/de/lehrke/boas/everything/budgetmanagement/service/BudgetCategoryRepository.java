package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;

import java.util.List;
import java.util.Optional;

public interface BudgetCategoryRepository {

    Optional<BudgetCategory> getBudgetCategoryById(Long id);
    
    List<BudgetCategory> getAllBudgetCategories();

    Optional<BudgetCategory> getBudgetCategoryByName(String name);

    BudgetCategory save(BudgetCategory budgetCategory);

    void deleteById(Long id);

}
