package de.lehrke.boas.everything.budgetmanagement.database.utils;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.database.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.database.Item;
import org.modelmapper.ModelMapper;

import java.util.function.Function;

public class Mappers {

    private static class MyModelMaper extends ModelMapper {

        MyModelMaper() {
            
        }
    
        @Override
        public <D> D map(Object source, Class<D> destinationType) {
            if (source == null)
                return null;

            return super.map(source, destinationType);
        }
    }

    private static final ModelMapper modelMapper = new MyModelMaper();

    public static final Function<BudgetCategory, de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory>
                mapBudgetCategoryToDomainModel = (bc) -> { return modelMapper.map(bc, de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory.class);};

    public static final Function<de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory, BudgetCategory>
                mapBudgetCategoryToDBModel = (bc) -> {return modelMapper.map(bc, BudgetCategory.class);};

    public static final Function<Budget, de.lehrke.boas.everything.budgetmanagement.database.Budget>
                mapBudgetToDBModel = (b) -> {return modelMapper.map(b, de.lehrke.boas.everything.budgetmanagement.database.Budget.class);};

    public static final Function<de.lehrke.boas.everything.budgetmanagement.database.Budget, Budget>
                mapBudgetToDomainModel = (b) -> {return modelMapper.map(b, Budget.class);};

    public static final Function<Item, de.lehrke.boas.everything.budgetmanagement.model.Item>
                mapItemToDomainModel = (i) -> {return modelMapper.map(i, de.lehrke.boas.everything.budgetmanagement.model.Item.class);};

    public static final Function<de.lehrke.boas.everything.budgetmanagement.model.Item, Item >
                mapItemToDBModel = (i) -> {return modelMapper.map(i, Item.class);};

}
