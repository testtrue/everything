package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;

import java.util.List;
import java.util.Optional;

public class ItemService {

    private ItemRepository itemRepository;

    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public Item addItem(Item posten){
        return this.itemRepository.save(posten);
    }

    public List<Item> getAllItemsByBudget(Budget budget, Month month) {
        return itemRepository.getAllItemsByBudget(budget, month);
    }

    public Optional<Item> getItemById(Long id) {
        return itemRepository.getItemById(id);
    }
    
}
