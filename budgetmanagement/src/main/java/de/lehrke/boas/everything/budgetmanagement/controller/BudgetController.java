package de.lehrke.boas.everything.budgetmanagement.controller;

import de.lehrke.boas.everything.budgetmanagement.configuration.BudgetManagementConfiguration;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryService;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController()
@RequestMapping(path = BudgetManagementConfiguration.BASE_URL + "/budgets")
@PreAuthorize("budgetmanagement")
@Slf4j
public class BudgetController {

    @Autowired
    private BudgetService budgetService;

    @Autowired
    private BudgetCategoryService budgetCategoryService;

    private Function<Budget, BudgetDto> budget = (budget) -> {
        return new BudgetDto(budget.getId(), budget.getAmount(), 
                                budget.getName(), 
                                budget.getRemainingAmount(),
                                budget.getBudgetCategory() != null ? budget.getBudgetCategory().getName() : "");
    };

    @GetMapping("/")
    @CrossOrigin
    public List<BudgetDto> getAllBudgetsByBudgetCategory(@RequestParam("budgetCategoryName") Optional<String> name, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth) {
        Month month = Month.create(optMonth.orElse(LocalDate.now()));
        BudgetCategory budgetCategory;
        if (name.isPresent()) {
            budgetCategory = budgetCategoryService.getBudgetCategoryByName(name.get());
        } else {
            budgetCategory = null;
        }
        return this.budgetService.getAllBudgetsByBudgetCategory(budgetCategory, month).stream().map(budget).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    @CrossOrigin
    public BudgetDto getBudgetById(@PathVariable("id") Long id, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth) {
        Month month = Month.create(optMonth.orElse(LocalDate.now()));
        return budgetService.getBudgetById(id, month).map(this.budget).get();
    }

    @PutMapping("/{id}")
    @CrossOrigin
    public BudgetDto updateBudget(@PathVariable Long id, @RequestBody BudgetDto budgetDto) {
        log.debug("DTO: {}", budgetDto);
        Budget budget = new Budget();
        budget.setId(id);
        budget.setName(budgetDto.getName());
        budget.setRemainingAmount(budgetDto.getRemainingAmount());
        budget.setAmount(budgetDto.getAmount());
        budget.setBudgetCategory(this.budgetCategoryService.getBudgetCategoryByName(budgetDto.getCategoryName()));
        this.budgetService.updateBudget(budget);
        return budgetDto;
    }

    @DeleteMapping("/{id}")
    @CrossOrigin
    public void deleteBudget(@PathVariable Long id) {
        if (id == null) {
            throw new IllegalArgumentException("The given id must not be null");
        }
        this.budgetService.deleteBudget(id);
    }

    @PostMapping("/")
    @CrossOrigin
    public BudgetDto createBudgetCategory(@RequestBody BudgetDto budgetDto) {
        Month month = Month.create(LocalDate.now());
        Budget budget = new Budget();
        budget.setId(null);
        budget.setName(budgetDto.getName());
        budget.setRemainingAmount(budgetDto.getRemainingAmount());
        budget.setAmount(budgetDto.getAmount());
        budget.setBudgetCategory(this.budgetCategoryService.getBudgetCategoryByName(budgetDto.getCategoryName()));
        return this.budget.apply(budgetService.addBudget(budget, month));
    }
    
}
