package de.lehrke.boas.everything.budgetmanagement.configuration;

import de.lehrke.boas.everything.budgetmanagement.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BudgetManagementConfiguration {
    
    public static final String BASE_URL = "/budgetmanagement";

    @Bean
    public BudgetCategoryService budgetCategoryService(BudgetCategoryRepository budgetCategoryRepository, BudgetService budgetService) {
        return new BudgetCategoryService(budgetCategoryRepository, budgetService);
    }

    @Bean
    public BudgetService budgetService(BudgetRepository budgetRepository, ItemRepository itemRepository) {
        return new BudgetService(budgetRepository, itemRepository);
    }

    @Bean
    public ItemService itemService(ItemRepository itemRepository) {
        return new ItemService(itemRepository);
    }
}
