package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;

import java.util.List;
import java.util.Optional;

public interface ItemRepository {
    
    List<Item> getAllItems();

    List<Item> getAllItemsByBudget(Budget budget, Month period);

    Optional<Item> getItemById(Long id);

    Item save(Item item);
}
