package de.lehrke.boas.everything.budgetmanagement.database;

import de.lehrke.boas.everything.budgetmanagement.database.utils.Mappers;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BudgetRepositoryConnector implements de.lehrke.boas.everything.budgetmanagement.service.BudgetRepository {

    @Autowired
    private BudgetRepository budgetRepository;

    @Override
    public List<de.lehrke.boas.everything.budgetmanagement.model.Budget> getAllBudgets() {
        return this.budgetRepository.findAll().stream().map(Mappers.mapBudgetToDomainModel).collect(Collectors.toList());
    }

    @Override
    public Optional<de.lehrke.boas.everything.budgetmanagement.model.Budget> getBudgetByName(String name) {
        return budgetRepository.findByName(name).map(Mappers.mapBudgetToDomainModel);
    }

    @Override
    public List<de.lehrke.boas.everything.budgetmanagement.model.Budget> getAllBudgetsByBudgetCategory(BudgetCategory budgetCategory) {
        return this.budgetRepository.findAllByBudgetCategory(Mappers.mapBudgetCategoryToDBModel.apply(budgetCategory)).stream().map(Mappers.mapBudgetToDomainModel).collect(Collectors.toList());
    }

    @Override
    public de.lehrke.boas.everything.budgetmanagement.model.Budget save(de.lehrke.boas.everything.budgetmanagement.model.Budget budget) {
        return Mappers.mapBudgetToDomainModel.apply(budgetRepository.save(Mappers.mapBudgetToDBModel.apply(budget)));
    }

    @Override
    public Optional<Budget> getBudgetById(Long id) {
        return this.budgetRepository.findById(id).map(Mappers.mapBudgetToDomainModel);
    }

    @Override
    public void deleteBudget(Long id) {
        this.budgetRepository.deleteById(id);
    }
    
}
