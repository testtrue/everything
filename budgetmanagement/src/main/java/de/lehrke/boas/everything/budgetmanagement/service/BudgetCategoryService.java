package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;

import java.util.List;
import java.util.Optional;


public class BudgetCategoryService {

    private BudgetCategoryRepository budgetCategoryRepository;

    public BudgetCategoryService(BudgetCategoryRepository budgetCategoryRepository,BudgetService budgetService) {
        this.budgetCategoryRepository = budgetCategoryRepository;
    }

    public List<BudgetCategory> getAllBudgetCategories(){
        return this.budgetCategoryRepository.getAllBudgetCategories();
    }

    public BudgetCategory createBudgetCategory(BudgetCategory budgetCategory) {
        Optional<BudgetCategory> optBudgetCategory = this.budgetCategoryRepository.getBudgetCategoryByName(budgetCategory.getName());
        if (!optBudgetCategory.isPresent()) {
            BudgetCategory newBudgetCategory = new BudgetCategory();
            newBudgetCategory.setName(budgetCategory.getName());
            newBudgetCategory = this.budgetCategoryRepository.save(newBudgetCategory);
            return newBudgetCategory;
        }
        return optBudgetCategory.get();
    }

    public BudgetCategory getBudgetCategoryById(Long id) {
        return this.budgetCategoryRepository.getBudgetCategoryById(id).orElse(null);
    }

    public BudgetCategory getBudgetCategoryByName(String name) {
        return this.budgetCategoryRepository.getBudgetCategoryByName(name).orElse(null);
    }

    public BudgetCategory updateBudgetCategory(BudgetCategory budgetCategoryDto) {
        Optional<BudgetCategory> storedBudgetCategory = this.budgetCategoryRepository.getBudgetCategoryById(budgetCategoryDto.getId());
        if (storedBudgetCategory.isPresent()) {
            BudgetCategory budgetCategory = storedBudgetCategory.get();
            budgetCategory.setName(budgetCategoryDto.getName());
            this.budgetCategoryRepository.save(budgetCategory);
        }
        return budgetCategoryDto;
    }

    public void deleteBudgetCategory(Long id) {
        this.budgetCategoryRepository.deleteById(id);
    }
    
}
