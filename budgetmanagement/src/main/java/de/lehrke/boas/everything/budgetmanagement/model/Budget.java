package de.lehrke.boas.everything.budgetmanagement.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Budget {
	
	private Long id;
	private String name;
	private BigDecimal amount;
	private BudgetCategory budgetCategory;
	private BigDecimal remainingAmount;

	public void setAmount(BigDecimal amount) {
		this.amount = amount.setScale(2);
	}

	public void setRemainingAmount(BigDecimal remainingAmount) {
		this.remainingAmount = remainingAmount.setScale(2);
	}

}
