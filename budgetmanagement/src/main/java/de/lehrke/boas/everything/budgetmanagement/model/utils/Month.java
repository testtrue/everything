package de.lehrke.boas.everything.budgetmanagement.model.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;


public class Month {
    
    public final LocalDateTime beginningOfMonth;
    public final LocalDateTime endOfMonth;
    public final LocalDateTime initialDate;

    private Month(LocalDate initialDate) {
        this.initialDate = initialDate.atStartOfDay();
        this.beginningOfMonth = this.initialDate.withDayOfMonth(1);
        this.endOfMonth = this.initialDate.withDayOfMonth(initialDate.lengthOfMonth());
    }

    public static Month create(LocalDate date) {
        return new Month(date);
    }

}
