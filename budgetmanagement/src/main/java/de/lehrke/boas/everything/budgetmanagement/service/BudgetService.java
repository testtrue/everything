package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BudgetService {

    private BudgetRepository budgetRepository;
    
    private ItemRepository itemRepository;

    private BigDecimal calculateSumOfAllPosten(Budget budget, Month month){
        return itemRepository.getAllItemsByBudget(budget, month).stream().
                map(posten -> posten.getAmount()).
                reduce(BigDecimal.ZERO, BigDecimal::add);
    };

    private Budget setRemainingAmount(Budget budget, Month month) {
        budget.setRemainingAmount(budget.getAmount().subtract(this.calculateSumOfAllPosten(budget, month)));
        return budget;
    };

    public BudgetService(BudgetRepository budgetRepository, ItemRepository itemRepository) {
        this.budgetRepository = budgetRepository;
        this.itemRepository = itemRepository;
    }

    public Budget getBudgetByName(String name, Month month) {
        return this.budgetRepository.getBudgetByName(name).map(b -> this.setRemainingAmount(b, month)).orElse(null);
    }

    public Optional<Budget> getBudgetById(Long id, Month month) {
        return this.budgetRepository.getBudgetById(id).map(b -> this.setRemainingAmount(b, month));
    }

    public Budget addBudget(Budget budget, Month month) {
        Optional<Budget> optBudget = this.budgetRepository.getBudgetByName(budget.getName());
        if (!optBudget.isPresent()) {
            optBudget = Optional.ofNullable(this.budgetRepository.save(budget));
        }
        return optBudget.map(b -> this.setRemainingAmount(b, month)).get();
    }

    public void updateBudget(Budget budget) {
        this.budgetRepository.save(budget);
    }

    public List<Budget> getAllBudgetsByBudgetCategory(BudgetCategory budgetCategory, Month month) {
        return this.budgetRepository.
                    getAllBudgetsByBudgetCategory(budgetCategory).stream().
                    map(b -> this.setRemainingAmount(b, month)).
                    collect(Collectors.toList());
    }

    public void deleteBudget(Long id) {
        this.budgetRepository.deleteBudget(id);
    }

}
