package de.lehrke.boas.everything.budgetmanagement.controller;

import lombok.Data;

import java.math.BigDecimal;

/**
 * BudgetCategoryDto
 */
@Data
public class BudgetDto {
    private final Long id;
    private final BigDecimal amount;
    private final String name;
    private final BigDecimal remainingAmount;
    private final String categoryName;
    
}