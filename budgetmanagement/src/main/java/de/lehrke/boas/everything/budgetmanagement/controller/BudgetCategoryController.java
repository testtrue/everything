package de.lehrke.boas.everything.budgetmanagement.controller;

import de.lehrke.boas.everything.budgetmanagement.configuration.BudgetManagementConfiguration;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryService;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@RestController()
@RequestMapping(path = BudgetManagementConfiguration.BASE_URL +"/budgetcategories")
@PreAuthorize("budgetmanagement")
public class BudgetCategoryController {

    private BudgetCategoryService budgetCategoryService;
    
    private BudgetService budgetService;

    private BiFunction<BudgetCategory, Month, BudgetCategoryDto> mapBudgetCategory = (budgetCategory, month) -> {
        List<Budget> budgets = this.budgetService.getAllBudgetsByBudgetCategory(budgetCategory, month);
        BigDecimal amount = budgets.stream().map(budget -> budget.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal remainingAmount = budgets.stream().
                            map(b -> b.getAmount().subtract(b.getRemainingAmount()) ).
                            reduce(BigDecimal.ZERO, BigDecimal::add);
        
        return new BudgetCategoryDto(budgetCategory.getId(), amount.doubleValue(), budgetCategory.getName(), amount.subtract(remainingAmount).doubleValue());
    };

    public BudgetCategoryController(BudgetCategoryService budgetCategoryService, BudgetService budgetService){
        this.budgetCategoryService = budgetCategoryService;
        this.budgetService = budgetService;
    }

    @GetMapping({"","/"})
    public List<BudgetCategoryDto> getAllBudgetCategories(@RequestParam("categoryName") Optional<String> name, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth) {
        Month month = Month.create(optMonth.orElse(LocalDate.now()));
        if (name.isPresent()) {
            BudgetCategory budgetCategory = this.budgetCategoryService.getBudgetCategoryByName(name.get());
            if (budgetCategory != null) {
                return List.of(mapBudgetCategory.apply(budgetCategory, month));
            } else {
                return List.of();
            }
        }
        return this.budgetCategoryService.getAllBudgetCategories().stream().map( bc -> mapBudgetCategory.apply(bc, month)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public BudgetCategoryDto getBudgetCategoryById(@PathVariable Long id) {
        Month month = Month.create(LocalDate.now());
        return mapBudgetCategory.apply(this.budgetCategoryService.getBudgetCategoryById(id), month);
    }

    @PutMapping("/{id}")
    public BudgetCategoryDto updateBudgetCategory(@PathVariable Long id, @RequestBody BudgetCategoryDto budgetCategoryDto) {
        Month month = Month.create(LocalDate.now());
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setId(budgetCategoryDto.getId());
        budgetCategory.setName(budgetCategoryDto.getName());

        return this.mapBudgetCategory.apply(this.budgetCategoryService.updateBudgetCategory(budgetCategory), month);
    }

    @PostMapping("/")
    public BudgetCategoryDto createBudgetCategory(@RequestBody BudgetCategoryDto budgetCategoryDto) {
        Month month = Month.create(LocalDate.now());
        BudgetCategory budgetCategory = new BudgetCategory();
        budgetCategory.setId(budgetCategoryDto.getId());
        budgetCategory.setName(budgetCategoryDto.getName());
        return this.mapBudgetCategory.apply(budgetCategoryService.createBudgetCategory(budgetCategory), month);
    }

    @DeleteMapping("/{id}")
    public void deleteBudgetCategory(@PathVariable Long id) {
        this.budgetCategoryService.deleteBudgetCategory(id);
    }
}
