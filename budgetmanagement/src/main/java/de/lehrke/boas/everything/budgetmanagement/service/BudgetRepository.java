package de.lehrke.boas.everything.budgetmanagement.service;

import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.BudgetCategory;

import java.util.List;
import java.util.Optional;

public interface BudgetRepository {

    Optional<Budget> getBudgetById(Long id);
    
    List<Budget> getAllBudgets();

    Optional<Budget> getBudgetByName(String name);

    List<Budget> getAllBudgetsByBudgetCategory(BudgetCategory budgetCategory);

    Budget save(Budget budget);
    
    void deleteBudget(Long id);
}
