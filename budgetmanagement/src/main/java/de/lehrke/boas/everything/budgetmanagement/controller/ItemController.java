package de.lehrke.boas.everything.budgetmanagement.controller;

import de.lehrke.boas.everything.budgetmanagement.configuration.BudgetManagementConfiguration;
import de.lehrke.boas.everything.budgetmanagement.model.Budget;
import de.lehrke.boas.everything.budgetmanagement.model.Item;
import de.lehrke.boas.everything.budgetmanagement.model.utils.Month;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import de.lehrke.boas.everything.budgetmanagement.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@PreAuthorize("budgetmanagement")
@RequestMapping(BudgetManagementConfiguration.BASE_URL + "/items")
public class ItemController {
    
    @Autowired
    private ItemService itemService;

    @Autowired
    private BudgetService budgetService;

    private Function<Item, ItemDto> mapItemToDto = i -> new ItemDto(i.getId(), i.getName(), i.getAmount().doubleValue(), i.getBudget().getName(), i.getBudget().getBudgetCategory().getName(), i.getDate());

    @GetMapping("/{id}")
    @CrossOrigin
    public ResponseEntity<ItemDto> getItemById(@PathVariable Long id){
        Optional<Item> optItem = itemService.getItemById(id);
        return ResponseEntity.of(optItem.map(mapItemToDto));
    }

    @GetMapping({"","/"})
    @CrossOrigin
    public ResponseEntity<List<ItemDto>> getAllItemByBudgetCategory(@RequestParam("budget") String categoryName, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth) {
        Month month = Month.create(optMonth.orElse(LocalDate.now()));
        Budget budget = budgetService.getBudgetByName(categoryName, month);
        List<ItemDto> items = itemService.getAllItemsByBudget(budget, month).stream().map(mapItemToDto).collect(Collectors.toList());
        return ResponseEntity.ok(items);
    }

    @PostMapping({"","/"})
    @CrossOrigin
    public ItemDto createItem(@RequestBody ItemDto itemDto){
        Month month = Month.create(LocalDate.now());
        Budget budget = budgetService.getBudgetByName(itemDto.getBudgetname(), month);
        Item item = new Item();
        item.setAmount(BigDecimal.valueOf(itemDto.getAmount()));
        item.setName(itemDto.getName());
        item.setBudget(budget);
        item.setDate(itemDto.getDate());

        return mapItemToDto.apply(itemService.addItem(item));
    }


}
