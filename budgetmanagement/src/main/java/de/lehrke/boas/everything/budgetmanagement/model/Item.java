package de.lehrke.boas.everything.budgetmanagement.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Item {
    
    private Long id;
    private String name;
    private BigDecimal amount;
    private LocalDateTime date;
    private Budget budget;
}
