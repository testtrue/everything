package de.lehrke.boas.everything.budgetmanagement.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item,Long>{

    List<Item> findAllByBudgetAndDateBetween(Budget budget, LocalDateTime from, LocalDateTime to);
    
}
