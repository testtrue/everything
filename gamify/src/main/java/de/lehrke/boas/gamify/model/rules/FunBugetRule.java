package de.lehrke.boas.gamify.model.rules;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FunBugetRule {

    private Integer value;
}
