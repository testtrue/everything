package de.lehrke.boas.gamify.model.quest;

public enum QuestType {
    URGENT,
    NORMAL,
    SIDE,
    PERPETUAL
}
