package de.lehrke.boas.gamify.model.quest;

public enum RepititionType {
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY
}
