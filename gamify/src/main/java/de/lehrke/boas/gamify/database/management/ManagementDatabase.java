package de.lehrke.boas.gamify.database.management;


import de.lehrke.boas.gamify.model.quest.Quest;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

@Data
@Service
public class ManagementDatabase {

    private final QuestDBORepository questDBORepository;

    private static final Function<QuestDBO, Quest> mapDBOtoModel = d -> {
        Quest q = new Quest();
        q.setId(d.getId());
        q.setType(d.getType());
        q.setQuest(d.getQuest());
        q.setAdvancement(d.getAdvancement());
        q.setQuestType(d.getQuestType());
        return q;
    };

    public List<Quest> getAllListByUserId(UUID userId) {

        return questDBORepository.findByOwner(userId).stream().map(
                mapDBOtoModel
        ).toList();
    }

    public List<Quest> getAllQuestsByQuestName(UUID userId, String query) {
        query = "%" + query + "%";
        return questDBORepository.findByQuestLikeIgnoreCaseAndOwnerOrQuestLikeIgnoreCaseAndShareWIthAllTrue( query , userId, query ).stream().map(mapDBOtoModel).toList();
    }
}
