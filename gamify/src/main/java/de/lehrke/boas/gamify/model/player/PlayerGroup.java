package de.lehrke.boas.gamify.model.player;

import de.lehrke.boas.gamify.model.quest.CoopQuests;
import de.lehrke.boas.gamify.model.quest.Quest;
import de.lehrke.boas.gamify.model.rules.NoQuestFoundException;
import lombok.Data;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class PlayerGroup {
    private UUID id;
    private String name;
    private final Player owner;
    private List<Player> players;
    private List<Player> invitations;
    private CoopQuests coopQuests;

    public PlayerGroup(@NonNull Player owner) {
        this.owner = owner;
        this.players = new ArrayList<>();
        this.invitations = new ArrayList<>();
        this.coopQuests = new CoopQuests();
        this.players.add(this.owner);
    }

    public void invitePlayer(Player player) {
        invitations.add(player);
    }

    public void acceptInvitation(Player player) {
        players.add(player);
        invitations.remove(player);
    }

    public void acceptQuest(Quest quest) {
        this.coopQuests.add(quest);
    }

    public void finishQuest(Quest quest) {
        if (!this.coopQuests.contains(quest)) {
            throw new NoQuestFoundException(quest);
        }
        this.players.forEach(p -> p.finishQuest(quest));
        this.coopQuests.remove(quest);
    }

}
