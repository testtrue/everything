package de.lehrke.boas.gamify.model.rules;

import jakarta.annotation.Nonnull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Unlocked {

    private Integer treats = 0;

    private Integer miniGames = 0;

    private Integer bigTreats = 0;

    public void incrementTreats() {
        this.treats++;
    }

    public void incrementMiniGames() {
        this.miniGames++;
    }

    public void incrementBigTreats() {
        this.bigTreats++;
    }

    public void decrementTreats() {
        this.treats--;
    }

    public void decrementBigTreats() {
        this.bigTreats--;
    }

    public void decrementMiniGames() {
        this.miniGames--;
    }
}
