package de.lehrke.boas.gamify.database.player;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "invited_members")
public class InvitedMembers {
    @EmbeddedId
    private InvitedMembersId invitedMembersId;

    @ManyToOne()
    @MapsId("playerGroupId")
    private PlayerGroupDBO playerGroupDBO;

    @ManyToOne()
    @MapsId("playerId")
    private PlayerDBO playerDBO;

    @Column(name = "invited_on")
    private LocalDateTime invitedOn;


}