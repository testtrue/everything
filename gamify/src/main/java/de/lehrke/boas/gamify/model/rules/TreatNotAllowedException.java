package de.lehrke.boas.gamify.model.rules;

import java.io.Serial;

public class TreatNotAllowedException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 1L;

    public TreatNotAllowedException() {
        super("Treat darf noch nicht genommen werden!");
    }

}
