package de.lehrke.boas.gamify.model.rules;

import lombok.Data;

@Data
public class Rules {
    private final TreatRule treatRule;
    private final MiniGameRule minigameRule;
    private final BigTreatRule bigTreatRule;
    private final FunBugetRule funBudgetRule;
}
