package de.lehrke.boas.gamify.service.dto;

import java.util.UUID;

public record QuestDeletionDto(UUID questId) {
}
