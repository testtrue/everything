package de.lehrke.boas.gamify.model.quest;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PerpetualQuest extends Quest {
    private RepititionType repetition;
}
