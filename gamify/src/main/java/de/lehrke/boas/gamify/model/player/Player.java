package de.lehrke.boas.gamify.model.player;

import de.lehrke.boas.gamify.model.game.MiniGame;
import de.lehrke.boas.gamify.model.quest.QuestList;
import de.lehrke.boas.gamify.model.rules.MinigameNotAllowedException;
import de.lehrke.boas.gamify.model.rules.Rules;
import de.lehrke.boas.gamify.model.rules.TreatNotAllowedException;
import de.lehrke.boas.gamify.model.quest.Quest;
import de.lehrke.boas.gamify.model.rules.Unlocked;
import lombok.Data;

import java.util.UUID;

@Data
public class Player {
    private UUID id;
    private String name;
    private Integer experience = 0;
    private Integer lore = 0;
    private Integer funBudget;
    private final QuestList questList;
    private final Unlocked unlocked;
    private final Rules rules;

    public Player(QuestList questList, Unlocked unlocked, Rules rules) {
        this.questList = questList;
        this.questList.init();
        this.unlocked = unlocked;
        this.rules = rules;
        this.funBudget = rules.getFunBudgetRule().getValue();
    }

    public void acceptQuest(Quest quest) {
        this.questList.acceptQuest(quest);
    }

    public void acceptCoopQuest(PlayerGroup playerGroup, Quest quest) {
        playerGroup.acceptQuest(quest);
    }

    public void finishQuest(Quest quest) {
        questList.archiveQuest(quest);
        evaluateQuest(quest);

        final int amountOfBigTreats = this.experience / this.rules.getBigTreatRule().getValue();


        if (this.rules.getBigTreatRule().getValue() <= this.experience) {
            for (int i = 0; i < amountOfBigTreats; i++) {
                this.unlocked.incrementBigTreats();
            }
            this.experience = this.experience % this.rules.getBigTreatRule().getValue();
        }
        final int amountOfSmallTreas = this.experience / this.rules.getTreatRule().getValue();

        if (this.rules.getTreatRule().getValue() <= this.experience) {
            for (int i = 0; i < amountOfSmallTreas; i++) {
                this.unlocked.incrementTreats();
            }
        }

        final int amountOfMiniGames = this.lore / this.rules.getMinigameRule().getValue();

        if (this.rules.getMinigameRule().getValue() <= this.lore) {
            for (int i = 0; i < amountOfMiniGames; i++) {
                this.unlocked.incrementMiniGames();
            }
        }
    }

    private void evaluateQuest(Quest quest) {
        switch (quest.getType()) {
            case LORE -> this.lore+= quest.getAdvancement();
            case EXPERIENCE -> this.experience+= quest.getAdvancement();
        }
    }

    public void playMiniGame(MiniGame minigame) {
        if(this.unlocked.getMiniGames() <= 0) {
            throw new MinigameNotAllowedException();
        }
        this.unlocked.decrementMiniGames();
        this.evaluateQuest(minigame.getGame());
        this.funBudget -= minigame.getCost();
    }

    public void allowTreat() {
        if (this.unlocked.getTreats().equals(0)) {
            throw new TreatNotAllowedException();
        }
        this.unlocked.decrementTreats();
    }

    public void allowBigTreat() {
        if (this.unlocked.getBigTreats().equals(0)) {
            throw new TreatNotAllowedException();
        }
        this.unlocked.decrementBigTreats();
    }
}
