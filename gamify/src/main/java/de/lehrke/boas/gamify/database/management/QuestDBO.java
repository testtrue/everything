package de.lehrke.boas.gamify.database.management;

import de.lehrke.boas.gamify.model.quest.QuestType;
import de.lehrke.boas.gamify.model.rules.AdvancementType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "quest")
public class QuestDBO {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(nullable = false)
    private String quest;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private QuestType questType;
    @Column(nullable = false)
    private Integer advancement;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AdvancementType type;

    @Column(nullable = false)
    private UUID owner;
    private boolean shareWIthAll;

    @Column(nullable = false)
    private String locale;
}