package de.lehrke.boas.gamify.service.dto;

public record RulesDto(Integer treatRule, Integer bigTreatRule ,Integer miniGameRule, Integer funBudgetRule) {
}
