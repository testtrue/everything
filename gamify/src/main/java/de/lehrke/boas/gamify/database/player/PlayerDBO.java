package de.lehrke.boas.gamify.database.player;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "player")
public class PlayerDBO {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer experience;

    @Column(nullable = false)
    private Integer lore;

    @Column(nullable = false)
    private Integer funBudget;

    @Column(nullable = false)
    private Integer treats = 0;

    @Column(nullable = false)
    private Integer miniGames = 0;

    @Column(nullable = false)
    private Integer bigTreats = 0;

    @Column(nullable = false)
    private Integer treatRule;

    @Column(nullable = false)
    private Integer minigameRule;

    @Column(nullable = false)
    private Integer bigTreatRule;

    @Column(nullable = false)
    private Integer funBudgetRule;

    @Type(JsonType.class)
    @Column(nullable = false, columnDefinition = "json")
    private String questList;

    @OneToMany(mappedBy = "playerDBO", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InvitedMembers> invitedMembers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerDBO playerDBO = (PlayerDBO) o;
        return Objects.equals(getId(), playerDBO.getId()) && Objects.equals(getName(), playerDBO.getName()) && Objects.equals(getExperience(), playerDBO.getExperience()) && Objects.equals(getLore(), playerDBO.getLore()) && Objects.equals(getFunBudget(), playerDBO.getFunBudget()) && Objects.equals(getTreats(), playerDBO.getTreats()) && Objects.equals(getMiniGames(), playerDBO.getMiniGames()) && Objects.equals(getBigTreats(), playerDBO.getBigTreats()) && Objects.equals(getTreatRule(), playerDBO.getTreatRule()) && Objects.equals(getMinigameRule(), playerDBO.getMinigameRule()) && Objects.equals(getBigTreatRule(), playerDBO.getBigTreatRule()) && Objects.equals(getFunBudgetRule(), playerDBO.getFunBudgetRule()) && Objects.equals(getQuestList(), playerDBO.getQuestList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getExperience(), getLore(), getFunBudget(), getTreats(), getMiniGames(), getBigTreats(), getTreatRule(), getMinigameRule(), getBigTreatRule(), getFunBudgetRule(), getQuestList());
    }
}