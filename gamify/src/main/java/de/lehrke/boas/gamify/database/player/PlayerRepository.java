package de.lehrke.boas.gamify.database.player;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PlayerRepository extends JpaRepository<PlayerDBO, UUID> {
}