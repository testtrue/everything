package de.lehrke.boas.gamify.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.lehrke.boas.gamify.database.management.ManagementDatabase;
import de.lehrke.boas.gamify.database.management.QuestDBO;
import de.lehrke.boas.gamify.database.management.QuestDBORepository;
import de.lehrke.boas.gamify.database.player.PlayerDatabase;
import de.lehrke.boas.gamify.model.player.Player;
import de.lehrke.boas.gamify.model.player.PlayerGroup;
import de.lehrke.boas.gamify.model.quest.Quest;
import de.lehrke.boas.gamify.model.quest.QuestList;
import de.lehrke.boas.gamify.model.rules.*;
import de.lehrke.boas.gamify.service.dto.*;
import de.lehrke.boas.gamify.service.events.ActionSuccessful;
import de.lehrke.boas.gamify.service.events.CreatedEvent;
import de.lehrke.boas.gamify.service.failures.GroupDoesNotExistException;
import de.lehrke.boas.gamify.service.failures.MissingAuthorizationException;
import de.lehrke.boas.gamify.service.failures.PlayerAlreadyExistsException;
import de.lehrke.boas.gamify.service.failures.PlayerDoesNotExistException;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Data
public class PlayerService {

    private final PlayerDatabase playerDatabase;
    private final ManagementDatabase managementDatabase;


    public CreatedEvent createPlayer(UUID uuid, String name) throws JsonProcessingException {

        if (playerDatabase.existsPlayerById(uuid)) {
            throw new PlayerAlreadyExistsException();
        }

        Player player = new Player(new QuestList(), new Unlocked(), new Rules(new TreatRule(100),
                new MiniGameRule(100),
                new BigTreatRule(1000),
                new FunBugetRule(400)));
        player.setId(uuid);
        player.setName(name);
        player.setLore(0);
        player.setExperience(0);
        player.setFunBudget(0);

        playerDatabase.save(player);

        return new CreatedEvent(uuid, name);
    }

    public ActionSuccessful updateRules(UUID userId, UUID id, RulesDto rulesDto) throws JsonProcessingException {

        if (!userId.equals(id)) {
            throw new MissingAuthorizationException();
        }

        Player player = playerDatabase.findById(id);

        if (player == null) {
            throw new PlayerDoesNotExistException();
        }

        Rules rules = player.getRules();

        rules.getTreatRule().setValue(rulesDto.treatRule());
        rules.getMinigameRule().setValue(rulesDto.miniGameRule());
        rules.getBigTreatRule().setValue(rulesDto.bigTreatRule());
        rules.getFunBudgetRule().setValue(rulesDto.funBudgetRule());
        playerDatabase.save(player);

        Map<String, Object> param = new HashMap<>();
        param.put("id", id);
        param.put("rules", rules);
        return new ActionSuccessful("updateRules", param);
    }

    public CreatedEvent createGroup(UUID playerUuid, String name) throws JsonProcessingException {
        Player player = playerDatabase.findById(playerUuid);
        if (player == null) {
            throw new PlayerDoesNotExistException();
        }
        PlayerGroup playerGroup = new PlayerGroup(player);
        playerGroup.setName(name);
        do  {
            playerGroup.setId(UUID.randomUUID());
        } while (playerDatabase.existsGroupById(playerGroup.getId()));

        PlayerGroup createdPlayerGroup = playerDatabase.save(playerGroup);
        return new CreatedEvent(createdPlayerGroup.getId(), name);
    }

    public Player getPlayer(UUID uuid) throws JsonProcessingException {
        Player player = playerDatabase.findById(uuid);
        if (player == null) {
            throw new PlayerDoesNotExistException();
        }
        return player;
    }

    public List<PlayerGroup> getPlayerGroups(UUID uuid) throws JsonProcessingException {
        return playerDatabase.getPlayerGroupsByPlayerId(uuid);
    }

    public ActionSuccessful invitePlayer(UUID userId, InvitationDto invitationDto) throws JsonProcessingException {
        PlayerGroup group = this.playerDatabase.findGroupById(invitationDto.groupUuid());

        if (group == null) {
            throw new GroupDoesNotExistException();
        }

        if (!group.getOwner().getId().equals(userId)) {
            throw new MissingAuthorizationException();
        }

        Player player = this.playerDatabase.findById(invitationDto.playerUuid());
        if (player == null) {
            throw new PlayerDoesNotExistException();
        }

        group.invitePlayer(player);
        this.playerDatabase.save(group);
        Map<String, Object> param = new HashMap<>();
        param.put("playerId", invitationDto.playerUuid());
        param.put("groupId", group.getId());
        return new ActionSuccessful("invitePlayer", param);
    }

    public ActionSuccessful createQuest(UUID userId, QuestCreationDto creationDto) throws JsonProcessingException {
        Player player = playerDatabase.findById(userId);
        if (player == null) {
            throw new PlayerDoesNotExistException();
        }

        QuestDBO questDBO = new QuestDBO();
        questDBO.setQuest(creationDto.quest());
        questDBO.setQuestType(creationDto.questType());
        questDBO.setAdvancement(creationDto.advancement());
        questDBO.setType(creationDto.type());
        questDBO.setOwner(userId);
        questDBO.setShareWIthAll(creationDto.shareWIthAll());
        questDBO.setLocale("de_de");

        questDBO = managementDatabase.getQuestDBORepository().save(questDBO);

        Map<String, Object> param = new HashMap<>();
        param.put("questId", questDBO.getId());
        param.put("owner", questDBO.getOwner());
        return new ActionSuccessful("createQuest", param);
    }

    public ActionSuccessful deleteQuest(UUID userId, QuestDeletionDto questDeletionDto) throws JsonProcessingException {
        Player player = playerDatabase.findById(userId);
        if (player == null) {
            throw new PlayerDoesNotExistException();
        }

        Optional<QuestDBO> questDBO = managementDatabase.getQuestDBORepository().findByOwnerAndId(userId, questDeletionDto.questId());

        if (questDBO.isEmpty()) {
            throw new MissingAuthorizationException();
        }

        managementDatabase.getQuestDBORepository().delete(questDBO.get());

        Map<String, Object> param = new HashMap<>();
        return new ActionSuccessful("deleteQuest", param);
    }

    public List<Quest> getAllMyQuests(UUID userId) {
        return this.managementDatabase.getAllListByUserId(userId);
    }

    public List<Quest> searchQuest(UUID userId, QuestSearchDto questSearchDto) throws JsonProcessingException {
        Player player = playerDatabase.findById(userId);
        if (player == null) {
            return List.of();
        }

        return this.managementDatabase.getAllQuestsByQuestName(userId, questSearchDto.query());

    }

}
