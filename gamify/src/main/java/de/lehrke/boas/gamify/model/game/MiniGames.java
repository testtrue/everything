package de.lehrke.boas.gamify.model.game;

import lombok.Data;

import java.util.List;

@Data
public class MiniGames {
    private List<MiniGame> miniGames;
}
