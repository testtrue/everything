package de.lehrke.boas.gamify.database.management;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface QuestDBORepository extends JpaRepository<QuestDBO, UUID> {
    Optional<QuestDBO> findByOwnerAndId(UUID owner, UUID id);

    List<QuestDBO> findByOwner(UUID owner);

    @Query("select q from QuestDBO q where upper(q.quest) like upper(?1)")
    List<QuestDBO> findByQuestLikeIgnoreCase(String quest);

    @Query("""
            select distinct q from QuestDBO q
            where upper(q.quest) like upper(?1) and q.owner = ?2 or upper(q.quest) like upper(?3) and q.shareWIthAll = true""")
    List<QuestDBO> findByQuestLikeIgnoreCaseAndOwnerOrQuestLikeIgnoreCaseAndShareWIthAllTrue(String quest, UUID owner, String quest1);
}