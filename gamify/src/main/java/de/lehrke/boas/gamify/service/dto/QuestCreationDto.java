package de.lehrke.boas.gamify.service.dto;

import de.lehrke.boas.gamify.model.quest.QuestType;
import de.lehrke.boas.gamify.model.rules.AdvancementType;

public record QuestCreationDto(String quest, QuestType questType, Integer advancement, AdvancementType type, boolean shareWIthAll) {
}
