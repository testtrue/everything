package de.lehrke.boas.gamify.model.quest;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CoopQuests {
    private List<Quest> quests = new ArrayList<>();

    public void add(Quest quest) {
        this.quests.add(quest);
    }

    public boolean contains(Quest quest) {
        return this.quests.contains(quest);
    }

    public void remove(Quest quest) {
        this.quests.remove(quest);
    }

    public int size() {
        return this.quests.size();
    }
}
