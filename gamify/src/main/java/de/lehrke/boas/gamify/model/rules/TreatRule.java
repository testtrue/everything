package de.lehrke.boas.gamify.model.rules;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TreatRule {
    private Integer value;
    private final AdvancementType advancementType = AdvancementType.LORE;
}
