package de.lehrke.boas.gamify.service.dto;

public record QuestSearchDto(String query) {
}
