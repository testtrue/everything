package de.lehrke.boas.gamify.database.player;

import io.hypersistence.utils.hibernate.type.json.JsonType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "player_group")
public class PlayerGroupDBO {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Type(JsonType.class)
    @Column(nullable = false, columnDefinition = "json")
    private String questList;

    @ManyToOne(optional = false)
    private PlayerDBO owner;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<PlayerDBO> members;

    @OneToMany(mappedBy = "playerGroupDBO", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InvitedMembers> invitedMembers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerGroupDBO that = (PlayerGroupDBO) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getQuestList(), that.getQuestList()) && Objects.equals(getOwner(), that.getOwner()) && Objects.equals(getMembers(), that.getMembers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getQuestList(), getOwner(), getMembers());
    }
}