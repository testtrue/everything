package de.lehrke.boas.gamify.service.events;

import java.util.UUID;

public record CreatedEvent(UUID uuid, String name) {
}
