package de.lehrke.boas.gamify.model.rules;

public enum AdvancementType {
    EXPERIENCE,
    LORE
}
