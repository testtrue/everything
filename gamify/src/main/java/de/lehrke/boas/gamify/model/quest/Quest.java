package de.lehrke.boas.gamify.model.quest;

import de.lehrke.boas.gamify.model.rules.AdvancementType;
import lombok.Data;

import java.util.UUID;

@Data
public class Quest {
    private UUID id;
    private String quest;
    private QuestType questType;
    private Integer advancement;
    private AdvancementType type;
}
