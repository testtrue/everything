package de.lehrke.boas.gamify.service.events;

import java.util.Map;

public record ActionSuccessful(String name, Map<String, Object> values) {
}
