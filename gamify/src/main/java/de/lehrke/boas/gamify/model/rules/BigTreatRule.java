package de.lehrke.boas.gamify.model.rules;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BigTreatRule {
    private Integer value;
    private final AdvancementType advancementType = AdvancementType.EXPERIENCE;
}
