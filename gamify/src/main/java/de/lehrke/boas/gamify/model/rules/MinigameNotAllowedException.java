package de.lehrke.boas.gamify.model.rules;

import java.io.Serial;

public class MinigameNotAllowedException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = 1L;
    public MinigameNotAllowedException() {
        super("MiniGame darf nicht gespielt werden!");
    }
}
