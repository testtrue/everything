package de.lehrke.boas.gamify.model.rules;

import de.lehrke.boas.gamify.model.quest.Quest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NoQuestFoundException extends RuntimeException {

    private final Quest quest;

    public NoQuestFoundException(Quest quest) {
        super("Quest wurde nicht gefunden!");
        this.quest = quest;
    }
}
