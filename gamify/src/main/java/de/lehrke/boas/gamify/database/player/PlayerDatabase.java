package de.lehrke.boas.gamify.database.player;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lehrke.boas.gamify.model.player.Player;
import de.lehrke.boas.gamify.model.player.PlayerGroup;
import de.lehrke.boas.gamify.model.quest.CoopQuests;
import de.lehrke.boas.gamify.model.quest.QuestList;
import de.lehrke.boas.gamify.model.rules.*;
import de.lehrke.boas.gamify.service.failures.PlayerDoesNotExistException;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Data
@Service
public class PlayerDatabase {

    private final PlayerRepository playerRepository;

    private final PlayerGroupRepository playerGroupRepository;

    private final ObjectMapper objectMapper;

    public void save(Player player) throws JsonProcessingException {
        PlayerDBO playerDBO = new PlayerDBO();
        playerDBO.setId(player.getId());
        playerDBO.setName(player.getName());
        playerDBO.setLore(player.getLore());
        playerDBO.setExperience(player.getExperience());
        playerDBO.setFunBudget(player.getFunBudget());
        playerDBO.setBigTreats(player.getUnlocked().getBigTreats());
        playerDBO.setTreats(player.getUnlocked().getTreats());
        playerDBO.setMiniGames(player.getUnlocked().getMiniGames());
        playerDBO.setBigTreatRule(player.getRules().getBigTreatRule().getValue());
        playerDBO.setTreatRule(player.getRules().getTreatRule().getValue());
        playerDBO.setMinigameRule(player.getRules().getMinigameRule().getValue());
        playerDBO.setFunBudgetRule(player.getRules().getFunBudgetRule().getValue());
        playerDBO.setQuestList(objectMapper.writeValueAsString(player.getQuestList()));
        playerRepository.save(playerDBO);
    }

    public Boolean existsPlayerById(UUID uuid) {
        return this.playerRepository.existsById(uuid);
    }

    public Player findById(UUID uuid) throws JsonProcessingException {
        Optional<PlayerDBO> d = this.playerRepository.findById(uuid);
        if (d.isEmpty()) {
            return null;
        }

        PlayerDBO playerDBO = d.get();

        return mapDBOtoDTO(playerDBO);
    }

    private Player mapDBOtoDTO(PlayerDBO playerDBO) throws JsonProcessingException {
        Player player = new Player(objectMapper.readValue(playerDBO.getQuestList(), QuestList.class),
                Unlocked.builder()
                                .treats(playerDBO.getTreats())
                                .bigTreats(playerDBO.getBigTreats())
                                .miniGames(playerDBO.getMiniGames()).build(),
                        new Rules(
                                new TreatRule(playerDBO.getTreatRule()),
                                new MiniGameRule(playerDBO.getMinigameRule()),
                                new BigTreatRule(playerDBO.getBigTreatRule()),
                                new FunBugetRule(playerDBO.getFunBudgetRule())));
        player.setName(playerDBO.getName());
        player.setLore(playerDBO.getLore());
        player.setExperience(playerDBO.getExperience());
        player.setFunBudget(playerDBO.getFunBudget());
        return player;
    }

    @Transactional
    public PlayerGroup save(PlayerGroup playerGroup) throws JsonProcessingException {
        if (playerGroup.getPlayers().stream().map(Player::getId).anyMatch(uuid -> !this.playerRepository.existsById(uuid))) {
            throw new PlayerDoesNotExistException();
        }
        PlayerGroupDBO playerGroupDBO = new PlayerGroupDBO();
        playerGroupDBO.setId(playerGroup.getId());
        playerGroupDBO.setName(playerGroup.getName());
        playerGroupDBO.setOwner(this.playerRepository.getReferenceById(playerGroupDBO.getOwner().getId()));
        playerGroupDBO.setMembers(playerGroup.getPlayers().stream().map(p -> this.playerRepository.getReferenceById(p.getId())).toList());
        playerGroupDBO.setQuestList(objectMapper.writeValueAsString(playerGroup.getCoopQuests()));
        this.playerGroupRepository.save(playerGroupDBO);
        return playerGroup;
    }

    @Transactional
    public void saveInvitation(PlayerGroup playerGroup, Player player) {
        PlayerDBO playerDBO = this.playerRepository.getReferenceById(player.getId());
        PlayerGroupDBO playerGroupDBO = this.playerGroupRepository.getReferenceById(playerGroup.getId());

        InvitedMembers invitedMembers = new InvitedMembers();
        InvitedMembersId invitedMembersId = new InvitedMembersId();
        invitedMembersId.setPlayerId(player.getId());
        invitedMembersId.setPlayerGroupId(playerGroup.getId());
        invitedMembers.setInvitedMembersId(invitedMembersId);
        invitedMembers.setPlayerDBO(playerDBO);
        invitedMembers.setPlayerGroupDBO(playerGroupDBO);
        invitedMembers.setInvitedOn(LocalDateTime.now());

        playerGroupDBO.getInvitedMembers().add(invitedMembers);
        this.playerGroupRepository.save(playerGroupDBO);
    }

    public boolean existsGroupById(UUID id) {
        return this.playerGroupRepository.existsById(id);
    }

    public PlayerGroup findGroupById(UUID id) throws JsonProcessingException {
        Optional<PlayerGroupDBO> groupDBO = this.playerGroupRepository.findById(id);

        if (groupDBO.isEmpty()) {
            return null;
        }
        PlayerGroupDBO playerGroupDBO = groupDBO.get();
        return mapDBOtoDTO(playerGroupDBO);
    }

    public List<PlayerGroup> getPlayerGroupsByPlayerId(UUID uuid) throws JsonProcessingException {
        List<PlayerGroupDBO> group = this.playerGroupRepository.findAllByMembersId(uuid);

        List<PlayerGroup> list = new ArrayList<>();
        for (PlayerGroupDBO playerGroupDBO : group) {
            PlayerGroup mapDBOtoDTO = mapDBOtoDTO(playerGroupDBO);
            list.add(mapDBOtoDTO);
        }
        return list;
    }


    private PlayerGroup mapDBOtoDTO(PlayerGroupDBO playerGroupDBO) throws JsonProcessingException {
        PlayerGroup playerGroup = new PlayerGroup(this.mapDBOtoDTO(playerGroupDBO.getOwner()));
        playerGroup.setName(playerGroupDBO.getName());
        playerGroup.setId(playerGroupDBO.getId());
        List<Player> list = new ArrayList<>();
        for (PlayerDBO playerDBO : playerGroupDBO.getMembers()) {
            Player mapDBOtoDTO = mapDBOtoDTO(playerDBO);
            list.add(mapDBOtoDTO);
        }
        playerGroup.setPlayers(list);
        List<Player> result = new ArrayList<>();
        for (InvitedMembers invitedMembers : playerGroupDBO.getInvitedMembers()) {
            PlayerDBO playerDBO = invitedMembers.getPlayerDBO();
            Player mapDBOtoDTO = mapDBOtoDTO(playerDBO);
            result.add(mapDBOtoDTO);
        }
        playerGroup.setInvitations(result);
        playerGroup.setCoopQuests(this.objectMapper.readValue(playerGroupDBO.getQuestList(), CoopQuests.class));
        return playerGroup;
    }

}
