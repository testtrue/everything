package de.lehrke.boas.gamify.service.dto;

import java.util.UUID;

public record InvitationDto(UUID playerUuid, UUID groupUuid) {
}
