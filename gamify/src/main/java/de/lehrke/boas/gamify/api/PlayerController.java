package de.lehrke.boas.gamify.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.lehrke.boas.gamify.model.player.Player;
import de.lehrke.boas.gamify.service.PlayerService;
import de.lehrke.boas.gamify.service.dto.RulesDto;
import de.lehrke.boas.gamify.service.events.ActionSuccessful;
import de.lehrke.boas.gamify.service.events.CreatedEvent;
import de.lehrke.boas.gamify.service.failures.MissingAuthorizationException;
import de.lehrke.boas.gamify.service.failures.PlayerAlreadyExistsException;
import de.lehrke.boas.gamify.service.failures.PlayerDoesNotExistException;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

@Data
@RestController
@RequestMapping("/player")
public class PlayerController {

    private final PlayerService playerService;

    @GetMapping({"", "/"})
    public Player getPlayer(@AuthenticationPrincipal Jwt jwt) throws JsonProcessingException {
        try {
            return this.playerService.getPlayer(UUID.fromString(jwt.getClaimAsString("sub")));
        } catch (PlayerDoesNotExistException playerDoesNotExistException) {
            throw new AuthorizationServiceException("Player with id " + jwt.getId() + " does not exist");
        }
    }

    @PostMapping("/createPLayer")
    public ResponseEntity<CreatedEvent> createPlayer(@AuthenticationPrincipal Jwt jwt, @RequestBody String name) throws JsonProcessingException, URISyntaxException {
        try {
            return ResponseEntity.created(new URI("/player")).body(this.playerService.createPlayer(UUID.fromString(jwt.getClaimAsString("sub")), name));
        } catch (PlayerAlreadyExistsException playerAlreadyExistsException) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/updateRules")
    public ResponseEntity<ActionSuccessful> updateRules(@AuthenticationPrincipal Jwt jwt, RulesDto rulesDto) throws JsonProcessingException {
        UUID playerId = UUID.fromString(jwt.getClaimAsString("sub"));
        try {
            return ResponseEntity.ok(this.playerService.updateRules(playerId, playerId, rulesDto));
        } catch (MissingAuthorizationException missingAuthorizationException) {
            throw new AuthorizationServiceException("Missing authorization");
        } catch (PlayerDoesNotExistException playerDoesNotExistException) {
            throw new AuthorizationServiceException("Player with id " + jwt.getId() + " does not exist");
        }
    }

}
