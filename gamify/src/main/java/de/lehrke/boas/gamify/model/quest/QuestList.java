package de.lehrke.boas.gamify.model.quest;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;

import java.util.*;

@Getter
public class QuestList {
    private Map<QuestType, List<Quest>> quests;
    private Map<QuestType, List<Quest>> archive;

    public void init() {
        this.setQuests(new HashMap<>());
        this.setArchive(new HashMap<>());
        Arrays.stream(QuestType.values()).forEach(t -> {
            this.quests.put(t, new ArrayList<>());
            this.archive.put(t, new ArrayList<>());
        });
    }

    public void setQuests(Map<QuestType, List<Quest>> quests) {
        if (this.quests != null) {
            return;
        }
        this.quests = quests;
    }

    public void setArchive(Map<QuestType, List<Quest>> archive) {
        if (this.archive != null) {
            return;
        }
        this.archive = archive;
    }

    public void acceptQuest(Quest quest) {
        this.quests.get(quest.getQuestType()).add(quest);
    }

    public void archiveQuest(Quest quest) {
        List<Quest> quests = this.quests.get(quest.getQuestType());
        if (quests.remove(quest)) {
            this.archive.get(quest.getQuestType()).add(quest);
        }
    }

    public List<Quest> getQuestOf(QuestType questType) {
        return this.quests.get(questType);
    }
}
