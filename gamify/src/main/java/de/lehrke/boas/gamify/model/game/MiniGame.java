package de.lehrke.boas.gamify.model.game;

import de.lehrke.boas.gamify.model.quest.Quest;
import lombok.Data;

import java.util.UUID;

@Data
public class MiniGame {
    private Quest game;
    private Integer cost;
    private UUID playerId;
    private boolean shareWIthAll;
    private boolean isAllowedForAll;
}
