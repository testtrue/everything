package de.lehrke.boas.gamify.database.player;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Embeddable
public class InvitedMembersId {

  @Column(name = "player_group_id")
  private UUID playerGroupId;

  @Column(name = "player_id")
  private UUID playerId;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    InvitedMembersId entity = (InvitedMembersId) o;
    return Objects.equals(this.playerGroupId, entity.playerGroupId) &&
            Objects.equals(this.playerId, entity.playerId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(playerGroupId, playerId);
  }

}