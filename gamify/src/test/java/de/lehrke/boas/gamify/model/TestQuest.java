package de.lehrke.boas.gamify.model;

import de.lehrke.boas.gamify.model.game.MiniGame;
import de.lehrke.boas.gamify.model.player.Player;
import de.lehrke.boas.gamify.model.player.PlayerGroup;
import de.lehrke.boas.gamify.model.quest.PerpetualQuest;
import de.lehrke.boas.gamify.model.quest.Quest;
import de.lehrke.boas.gamify.model.quest.QuestList;
import de.lehrke.boas.gamify.model.quest.QuestType;
import de.lehrke.boas.gamify.model.rules.*;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestQuest {

    @Test
    public void testAcceptAndFinishQuest() {
        // Given
        Quest quest = getQuest();
        Player player = getDefaultPlayer();

        // When
        player.acceptQuest(quest);

        // Then
        assertEquals(1, player.getQuestList().getQuestOf(QuestType.NORMAL).size());
        assertEquals("Do something",player.getQuestList().getQuestOf(QuestType.NORMAL).getFirst().getQuest());

        // When
        player.finishQuest(quest);

        // Then
        assertEquals(0, player.getQuestList().getQuestOf(QuestType.NORMAL).size());
        assertEquals(0, player.getExperience());
        assertEquals(quest.getAdvancement(), player.getLore());
        assertEquals(0, player.getUnlocked().getBigTreats());
        assertEquals(0, player.getUnlocked().getMiniGames());
        assertEquals(0, player.getUnlocked().getTreats());

    }

    @Test
    public void testFinishQuestWIthLowRuleValues() {
        // Given
        Rules rules =new Rules(new TreatRule(5), new MiniGameRule(5), new BigTreatRule(15), new FunBugetRule(5));
        Quest quest = getQuest();
        Player player = new Player(new QuestList(), new Unlocked(), rules);
        player.acceptQuest(quest);

        // When
        player.finishQuest(quest);

        // Then
        assertEquals(0, player.getQuestList().getQuestOf(QuestType.NORMAL).size());
        assertEquals(0, player.getExperience());
        assertEquals(quest.getAdvancement(), player.getLore());
        assertEquals(0, player.getUnlocked().getBigTreats());
        assertEquals(2, player.getUnlocked().getMiniGames());
        assertEquals(0, player.getUnlocked().getTreats());
    }

    @Test
    public void testFinishSideQuestWIthLowRuleValues() {
        // Given
        Rules rules = new Rules(new TreatRule(5), new MiniGameRule(5), new BigTreatRule(15), new FunBugetRule(5));
        Quest quest = getSideQuest();
        Player player = new Player(new QuestList(), new Unlocked(), rules);
        player.acceptQuest(quest);

        // When
        player.finishQuest(quest);

        // Then
        assertEquals(0, player.getQuestList().getQuestOf(QuestType.NORMAL).size());
        assertEquals(quest.getAdvancement(), player.getExperience());
        assertEquals(0, player.getLore());
        assertEquals(0, player.getUnlocked().getBigTreats());
        assertEquals(0, player.getUnlocked().getMiniGames());
        assertEquals(2, player.getUnlocked().getTreats());
    }

    @Test
    public void testFinishOneMiniGameWithLowRuleValuesAndOneMiniGame() {
        // Given
        Rules rules = new Rules(new TreatRule(5), new MiniGameRule(10), new BigTreatRule(15), new FunBugetRule(5));
        Quest quest = getQuest();
        Player player = new Player(new QuestList(), new Unlocked(), rules);
        player.acceptQuest(quest);
        MiniGame miniGame = getMinigame();
        // When Then
        assertEquals(0,player.getUnlocked().getMiniGames());
        assertThrows(MinigameNotAllowedException.class, () -> player.playMiniGame(miniGame));

        // Given
        player.finishQuest(quest);
        assertEquals(1, player.getUnlocked().getMiniGames());
        assertEquals(5, player.getFunBudget());

        // When
        player.playMiniGame(miniGame);
        // Then
        assertEquals(0, player.getUnlocked().getMiniGames());
        assertEquals(miniGame.getGame().getAdvancement(),player.getExperience());
        assertEquals(0, player.getFunBudget());
    }

    @Test
    public void testAllowTreat() {
        // Given
        Rules rules = new Rules(new TreatRule(5), new MiniGameRule(10), new BigTreatRule(15), new FunBugetRule(5));
        Quest quest = getSideQuest();
        Player player = new Player(new QuestList(), new Unlocked(), rules);
        player.acceptQuest(quest);

        // When
        assertThrows(TreatNotAllowedException.class, player::allowTreat);

        // Given
        player.finishQuest(quest);
        assertEquals(2, player.getUnlocked().getTreats());
        assertEquals(quest.getAdvancement(), player.getExperience());

        // When
        player.allowTreat();
        // Then
        assertEquals(1, player.getUnlocked().getTreats());
        assertEquals(quest.getAdvancement(), player.getExperience());
    }

    @Test
    public void testAllowBigTreat() {
        // Given
        Rules rules = new Rules(new TreatRule(5), new MiniGameRule(10), new BigTreatRule(15), new FunBugetRule(5));
        Quest quest1 = getSideQuest();
        Quest quest2 = getSideQuest();
        Player player = new Player(new QuestList(), new Unlocked(), rules);
        player.acceptQuest(quest1);
        player.acceptQuest(quest2);

        // When
        assertThrows(TreatNotAllowedException.class, player::allowBigTreat);

        // Given
        player.finishQuest(quest1);
        player.finishQuest(quest2);
        assertEquals(3, player.getUnlocked().getTreats());
        assertEquals(5, player.getExperience());
        assertEquals(1, player.getUnlocked().getBigTreats());

        // When
        player.allowBigTreat();
        // Then
        assertEquals(3, player.getUnlocked().getTreats());
        assertEquals(5, player.getExperience());
        assertEquals( 0, player.getUnlocked().getBigTreats());

    }

    @Test
    public void testPlayerGroup() {
        Player player1 = getDefaultPlayer();
        player1.setName("player1");
        Player player2 = getDefaultPlayer();
        player2.setName("player2");
        Quest quest1 = getSideQuest();
        Quest quest2 = getSideQuest();

        PlayerGroup playerGroup = new PlayerGroup(player1);
        assertEquals(1, playerGroup.getPlayers().size());
        assertEquals(0, playerGroup.getCoopQuests().size());
        assertEquals(0, playerGroup.getInvitations().size());
        playerGroup.invitePlayer(player2);
        assertEquals(1, playerGroup.getPlayers().size());
        assertEquals(0, playerGroup.getCoopQuests().size());
        assertEquals(1, playerGroup.getInvitations().size());
        playerGroup.acceptInvitation(player2);
        assertEquals(2, playerGroup.getPlayers().size());
        assertEquals(0, playerGroup.getCoopQuests().size());
        assertEquals(0, playerGroup.getInvitations().size());
        assertThrows(NoQuestFoundException.class, () -> playerGroup.finishQuest(quest1));
        assertThrows(NoQuestFoundException.class, () -> playerGroup.finishQuest(quest2));
        player1.acceptCoopQuest(playerGroup, quest1);
        assertEquals(1, playerGroup.getCoopQuests().size());
        player2.acceptCoopQuest(playerGroup, quest2);
        assertEquals(2, playerGroup.getCoopQuests().size());
        playerGroup.finishQuest(quest1);
        assertEquals(1, playerGroup.getCoopQuests().size());
        assertEquals(quest1.getAdvancement(), player1.getExperience());
        assertEquals(quest1.getAdvancement(), player2.getExperience());
    }

    private static Quest getQuest() {
        Quest quest = new Quest();
        quest.setId(UUID.randomUUID());
        quest.setQuest("Do something");
        quest.setQuestType(QuestType.NORMAL);
        quest.setType(AdvancementType.LORE);
        quest.setAdvancement(10);
        return quest;
    }

    private static Quest getSideQuest() {
        Quest quest = new Quest();
        quest.setId(UUID.randomUUID());
        quest.setQuest("Do something");
        quest.setQuestType(QuestType.SIDE);
        quest.setType(AdvancementType.EXPERIENCE);
        quest.setAdvancement(10);
        return quest;
    }

    private static MiniGame getMinigame() {
        MiniGame miniGame = new MiniGame();
        miniGame.setCost(5);
        Quest quest = new Quest();
        miniGame.setGame(quest);

        quest.setId(UUID.randomUUID());
        quest.setQuest("Do something");
        quest.setQuestType(QuestType.SIDE);
        quest.setType(AdvancementType.EXPERIENCE);
        quest.setAdvancement(1);
        return miniGame;
    }

    private static Player getDefaultPlayer() {
        Player player = new Player(new QuestList(), new Unlocked(), new Rules(new TreatRule(100),
                                                                                new MiniGameRule(100),
                                                                                new BigTreatRule(1000),
                                                                                new FunBugetRule(400)));
        player.setName("testtrue");
        return player;
    }
}
