CREATE DATABASE IF NOT EXISTS `weather`;

GRANT ALL PRIVILIGES `weather`.* TO `root`;
flush privileges;

USE weather;


CREATE TABLE IF NOT EXISTS `messung` (
   `zeitpunkt` date NOT NULL,
   `ort` varchar(255) NOT NULL,
   `temperatur` float NOT NULL,
   `feuchtigkeit` float NOT NULL,
   CONSTRAINT pk_messung
   PRIMARY KEY (`zeitpunkt`, `ort`);
);


CREATE TABLE IF NOT EXISTS `messstation` (
   `ort` varchar(255) PRIMARY KEY NOT NULL,
   `object` varchar NOT NULL;
);

