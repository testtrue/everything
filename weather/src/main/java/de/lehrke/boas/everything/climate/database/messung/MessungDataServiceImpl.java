package de.lehrke.boas.everything.climate.database.messung;

import de.lehrke.boas.everything.climate.workflows.MessungDataService;
import de.lehrke.boas.everything.climate.workflows.MessungDto;
import de.lehrke.boas.everything.climate.workflows.Werte;
import lombok.Data;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Function;

@Data
@Repository
public class MessungDataServiceImpl implements MessungDataService {

    private final JPAMessungRepository jpaMessungRepository;

    private final Function<MessungDto, JPAMessung> mapToJpa = (m) -> {
        JPAMessung entity = new JPAMessung();
        entity.setZeitpunkt(m.date());
        entity.setOrt(m.ort());
        entity.setTemperatur(m.werte().temperatur());
        entity.setFeuchtigkeit(m.werte().humidity());
        return entity;
    };

    @Override
    public List<MessungDto> findAll() {
        return this.jpaMessungRepository.findAll().stream().map(
                j -> new MessungDto(j.getOrt(), j.getZeitpunkt(),
                        new Werte(j.getTemperatur(), j.getFeuchtigkeit())))
                .toList();
    }

    @Override
    public void save(MessungDto dto) {
        this.jpaMessungRepository.save(mapToJpa.apply(dto));
    }

    @Override
    public void saveAll(List<MessungDto> dto) {
        this.jpaMessungRepository.saveAll(dto.stream().map(mapToJpa).toList());
    }

}
