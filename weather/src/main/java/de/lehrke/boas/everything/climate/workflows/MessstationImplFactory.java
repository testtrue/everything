package de.lehrke.boas.everything.climate.workflows;

import java.util.List;

public interface MessstationImplFactory {

    List<MessstationImpl> ladeMessstationen();

    void fuegeMessstationHinzu(MessstationImpl messstationImpl);
}
