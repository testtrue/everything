package de.lehrke.boas.everything.climate.database.messung;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@IdClass(JPAMessungId.class)
@Table(name = "messung")
@Data
public class JPAMessung {

    @Id
    private LocalDateTime zeitpunkt;
    @Id
    private String ort;
    
    private Double temperatur;
    
    private Double feuchtigkeit;
}
