package de.lehrke.boas.everything.climate.model;

import lombok.Data;

import java.util.List;
import java.util.Optional;

@Data
public class Klima {

    private final List<Messstation> messstationen;

    private final List<Messung> messungen;

    public Optional<Messung> getLetzteMessungFuer(String ort) {
        return this.messungen.stream().filter(m -> ort.equals(m.getOrt()))
                .sorted((m1, m2) -> m2.getZeitpunkt().compareTo(m1.getZeitpunkt())).findFirst();
    }

    public Double getDurchschnittsTemperaturFuer(String ort) {
        return this.getMessungenFuer(ort).stream().mapToDouble(m -> m.getTemperatur())
                .average().orElse(Double.NaN);
    }

    public Double getDurchschnittsLuftFeuchtigkeitFuer(String ort) {
        return this.getMessungenFuer(ort).stream().mapToDouble(m -> m.getFeuchtigkeit())
                .average().orElse(Double.NaN);
    }

    public List<Messung> getMessungenFuer(String ort) {
        return this.messungen.stream().filter(m -> ort.equals(m.getOrt())).toList();
    }

    public void neueMessung() {
        this.messungen.addAll(this.messstationen.stream().map(Messstation::messen).toList());
    }
}
