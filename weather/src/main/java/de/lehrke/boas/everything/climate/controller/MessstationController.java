package de.lehrke.boas.everything.climate.controller;

import de.lehrke.boas.everything.climate.workflows.MessstationImpl;
import de.lehrke.boas.everything.climate.workflows.MessstationWorkflows;
import de.lehrke.boas.everything.climate.workflows.Sensor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/messstation/")
public class MessstationController {
    
    private final MessstationWorkflows messstationWorkflows;

    public MessstationController(MessstationWorkflows messstation) {
        this.messstationWorkflows = messstation;
    }

    @GetMapping("sensortypen")
    public String getSupportedSensortypen() {
        return "{\"DHT11Web\":[\"url\"]}";
    }

    @PostMapping("{ort}")
    public Boolean createMessstation(@PathVariable String ort, @RequestBody Sensor sensor) {
        this.messstationWorkflows.fuegeNeueMessstationHinzu(new MessstationImpl(ort, sensor));
        return true;
    } 

    @GetMapping("")
    public List<MessstationImpl> getMessstationen() {
        return this.messstationWorkflows.ladeMessstationen();
    }
}
