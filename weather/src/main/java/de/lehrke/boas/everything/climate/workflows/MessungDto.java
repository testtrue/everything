package de.lehrke.boas.everything.climate.workflows;

import de.lehrke.boas.everything.climate.model.Messung;

import java.time.LocalDateTime;

public record MessungDto(String ort, LocalDateTime date, Werte werte) {

    public MessungDto(Messung messung) {
        this(messung.getOrt(), messung.getZeitpunkt(), new Werte(messung.getTemperatur(), messung.getFeuchtigkeit()));
    }
}
