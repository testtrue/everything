package de.lehrke.boas.everything.climate.workflows;

import de.lehrke.boas.everything.climate.model.Messstation;
import de.lehrke.boas.everything.climate.model.Messung;
import lombok.Data;

@Data
public class MessstationImpl extends Messstation {
    private final Sensor sensor;

    public MessstationImpl(final String ort, final Sensor sensor) {
        super(ort);
        this.sensor = sensor;
    }

    public Messung messen() {
        Werte werte = this.sensor.getWerte();
        return Messung.neueMessung(ort, werte.temperatur(), werte.humidity());
    }
}
