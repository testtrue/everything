package de.lehrke.boas.everything.climate.model;

import lombok.Data;

@Data
public abstract class Messstation {

    protected final String ort;

    public abstract Messung messen();
}
