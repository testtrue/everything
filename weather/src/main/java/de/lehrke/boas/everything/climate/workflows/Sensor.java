package de.lehrke.boas.everything.climate.workflows;

public interface Sensor {

    Werte getWerte();
}
