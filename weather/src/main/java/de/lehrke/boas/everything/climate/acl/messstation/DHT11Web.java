package de.lehrke.boas.everything.climate.acl.messstation;

import de.lehrke.boas.everything.climate.workflows.Sensor;
import de.lehrke.boas.everything.climate.workflows.Werte;
import lombok.Data;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;

@Data
public class DHT11Web implements Sensor, Serializable {

    private final String url;
    RestTemplate restCall = new RestTemplate();

    @Override
    public Werte getWerte() {
        return restCall.getForObject(url, Werte.class);
    }

}
