package de.lehrke.boas.everything.climate.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Messung {

    private final LocalDateTime zeitpunkt;
    private final String ort;
    private final Double temperatur;
    private final Double feuchtigkeit;

    public static Messung neueMessung(String ort, Double temperatur, Double feuchtigkeit) {
        return new Messung(LocalDateTime.now(), ort, temperatur, feuchtigkeit);
    }
}
