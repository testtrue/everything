package de.lehrke.boas.everything.climate.workflows;

import java.time.format.DateTimeFormatter;

public enum Granularitaet {
    JAHR("yyyy", 1000), 
    MONAT("yyyyMM", 1000), 
    WOCHE("yyyyw", 1000), 
    TAG("yyyyMMdd", 1000), 
    STUNDE("yyyyMMddHH", 1000), 
    MINUTE("yyyyMMddHHmm", 2000);

    public final DateTimeFormatter formatter;
    public final Integer maxData;

    Granularitaet(String format, Integer maxData) {
        this.formatter = DateTimeFormatter.ofPattern(format);
        this.maxData = maxData;
    }
}
