package de.lehrke.boas.everything.climate.database.messstation;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JPAMessstationRepository extends JpaRepository<JPAMessstation, String> {

}
