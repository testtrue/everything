package de.lehrke.boas.everything.climate.database.messung;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

public interface JPAMessungRepository extends JpaRepository<JPAMessung, JPAMessungId> {

    public void deleteByZeitpunktBefore(LocalDateTime date);
}
