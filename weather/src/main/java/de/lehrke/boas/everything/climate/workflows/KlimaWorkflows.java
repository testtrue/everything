package de.lehrke.boas.everything.climate.workflows;

import de.lehrke.boas.everything.climate.model.Klima;
import de.lehrke.boas.everything.climate.model.Messstation;
import de.lehrke.boas.everything.climate.model.Messung;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
public class KlimaWorkflows {

        private final MessungDataService messungDataService;
        private final MessstationWorkflows messstationImplFactory;

        public void neueMessung() {
                List<Messstation> messstationen = new ArrayList<>(messstationImplFactory.ladeMessstationen());
                Klima klima = new Klima(messstationen, new ArrayList<>());
                klima.neueMessung();
                this.messungDataService.saveAll(klima.getMessungen().stream().map(
                                m -> new MessungDto(m.getOrt(), m.getZeitpunkt(),
                                                new Werte(m.getTemperatur(), m.getFeuchtigkeit())))
                                .toList());
        }

        public List<MessungDto> getDurchschnitt() {
                List<Messstation> messstationen = new ArrayList<>(messstationImplFactory.ladeMessstationen());
                List<Messung> messungen = this.messungDataService.findAll().stream()
                                .map(m -> new Messung(m.date(), m.ort(), m.werte().temperatur(), m.werte().humidity()))
                                .toList();

                Klima klima = new Klima(messstationen, messungen);
                return messstationen.stream().map(Messstation::getOrt).map(s -> new MessungDto(s, LocalDateTime.now(),
                                new Werte(klima.getDurchschnittsTemperaturFuer(s),
                                                klima.getDurchschnittsLuftFeuchtigkeitFuer(s))))
                                .toList();
        }

        public List<KlimaModel> getKlimaDaten() {
                return this.getKlimaDaten(Granularitaet.MINUTE);
        }

        public List<KlimaModel> getKlimaDaten(Granularitaet granularitaet) {
                List<Messstation> messstationen = new ArrayList<>(messstationImplFactory.ladeMessstationen());
                List<Messung> messungen = this.messungDataService.findAll().parallelStream()
                                .map(m -> new Messung(m.date(), m.ort(), m.werte().temperatur(), m.werte().humidity()))
                                .collect(Collectors.groupingBy(t -> granularitaet.formatter.format(t.getZeitpunkt()))).
                                entrySet().
                                parallelStream().map(e -> e.getValue().get(0)).toList();
                messungen = new ArrayList<>(messungen);
                messungen.sort((e1, e2) -> e1.getZeitpunkt().compareTo(e2.getZeitpunkt()));
                messungen = messungen.subList( messungen.size() > granularitaet.maxData ? messungen.size() - granularitaet.maxData : 0, messungen.size());

                Klima klima = new Klima(messstationen, messungen);
                return messstationen.stream().map(Messstation::getOrt).map(o -> {
                        Optional<Messung> letzteMessung = klima.getLetzteMessungFuer(o);
                        return new KlimaModel(
                                        o,
                                        new MessungDto(o,
                                                        letzteMessung.map(Messung::getZeitpunkt)
                                                                        .orElse(LocalDateTime.now()),
                                                        new Werte(letzteMessung.map(Messung::getTemperatur)
                                                                        .orElse(Double.NaN),
                                                                        letzteMessung.map(Messung::getFeuchtigkeit)
                                                                                        .orElse(Double.NaN))),
                                        new MessungDto(o, LocalDateTime.now(), new Werte(
                                                        klima.getDurchschnittsTemperaturFuer(o),
                                                        klima.getDurchschnittsLuftFeuchtigkeitFuer(o))),
                                        klima.getMessungenFuer(o).stream().map(MessungDto::new).toList());
                }).toList();
        }

}
