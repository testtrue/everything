package de.lehrke.boas.everything.climate.acl.cron;

import de.lehrke.boas.everything.climate.database.messung.JPAMessungRepository;
import de.lehrke.boas.everything.climate.workflows.KlimaWorkflows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class MessJob {

    @Autowired
    private KlimaWorkflows klimaWorkflows;

    @Autowired
    private JPAMessungRepository jpaMessungRepository;

    @Scheduled(cron = "${de.lehrke.boas.everything.climate.acl.cron.messen:0 */5 * * * ?}")
    public void messen() {
        log.info("----- JOB 'messen' START -----");
        klimaWorkflows.neueMessung();
        log.info("----- JOB END -----");
    }

    @Scheduled(cron = "${de.lehrke.boas.everything.climate.acl.cron.aufraeumen:0 0 0 * * ?}")
    public void dbAufraeumen() {
        log.info("----- JOB 'dbAufraeumen' START -----");
        jpaMessungRepository.deleteByZeitpunktBefore(LocalDateTime.now().minusWeeks(10));
        log.info("----- JOB END -----");
    }
}
