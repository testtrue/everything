package de.lehrke.boas.everything.climate.database.messstation;

import de.lehrke.boas.everything.climate.acl.messstation.DHT11Web;
import de.lehrke.boas.everything.climate.workflows.MessstationImpl;
import de.lehrke.boas.everything.climate.workflows.MessstationImplFactory;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MessstationFactory implements MessstationImplFactory {

    @Autowired
    private JPAMessstationRepository jpaMessstationRepository;

    @PostConstruct
    public void initMessstationen() {
        JPAMessstation messstation = new JPAMessstation();
        messstation.setOrt("Flur");
        messstation.setUrl("http://192.168.178.7:1880/climate");
        jpaMessstationRepository.save(messstation);
    }

    @Override
    public List<MessstationImpl> ladeMessstationen() {
        return jpaMessstationRepository.findAll().stream()
                .map(m -> new MessstationImpl(m.getOrt(), new DHT11Web(m.getUrl())))
                .toList();
    }

    @Override
    public void fuegeMessstationHinzu(MessstationImpl messstationImpl) {
        if(!(messstationImpl.getSensor() instanceof DHT11Web)) {
            return;
        }
        JPAMessstation messstation = new JPAMessstation();
        messstation.setOrt(messstationImpl.getOrt());
        messstation.setUrl(((DHT11Web)messstationImpl.getSensor()).getUrl());
        this.jpaMessstationRepository.save(messstation);
    }

}
