package de.lehrke.boas.everything.climate.workflows;

import java.util.List;

public interface MessungDataService {

    public List<MessungDto> findAll();

    public void save(MessungDto dto);

    public void saveAll(List<MessungDto> dto);
}
