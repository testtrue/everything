package de.lehrke.boas.everything.climate.workflows;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class MessstationWorkflows {

    @Autowired
    private MessstationImplFactory messstationImplFactory;

    public void fuegeNeueMessstationHinzu(MessstationImpl neueMessstation) {
        this.messstationImplFactory.fuegeMessstationHinzu(neueMessstation);
    }

    public List<MessstationImpl> ladeMessstationen() {
        return this.messstationImplFactory.ladeMessstationen();
    }
}
