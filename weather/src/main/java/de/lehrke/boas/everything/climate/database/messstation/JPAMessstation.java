package de.lehrke.boas.everything.climate.database.messstation;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="messstation")
@Data
public class JPAMessstation {

    @Id
    private String ort;
    private String url;
}
