package de.lehrke.boas.everything.climate.database.messung;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class JPAMessungId {
    private LocalDateTime zeitpunkt;

    private String ort;
}
