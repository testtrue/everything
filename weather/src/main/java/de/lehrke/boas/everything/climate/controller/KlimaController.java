package de.lehrke.boas.everything.climate.controller;

import de.lehrke.boas.everything.climate.workflows.Granularitaet;
import de.lehrke.boas.everything.climate.workflows.KlimaModel;
import de.lehrke.boas.everything.climate.workflows.KlimaWorkflows;
import de.lehrke.boas.everything.climate.workflows.MessungDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/climate")
public class KlimaController {
    
    private final KlimaWorkflows klimaWorkflows;

    public KlimaController(KlimaWorkflows klimaWorkflows) {
        this.klimaWorkflows = klimaWorkflows;
    }

    @GetMapping("/")
    public List<KlimaModel> getKlimaDaten(@RequestParam() Optional<Granularitaet> granularitaet) {
        return granularitaet.map(this.klimaWorkflows::getKlimaDaten).orElse(this.klimaWorkflows.getKlimaDaten());
    }

    @GetMapping("/durchschnitt")
    public List<MessungDto> getDurchschnitt() {
        return this.klimaWorkflows.getDurchschnitt();
    }

}
