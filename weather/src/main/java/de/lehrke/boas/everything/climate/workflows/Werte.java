package de.lehrke.boas.everything.climate.workflows;

public record Werte(
        Double temperatur,
        Double humidity) {
}
