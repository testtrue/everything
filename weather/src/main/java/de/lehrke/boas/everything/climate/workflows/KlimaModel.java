package de.lehrke.boas.everything.climate.workflows;

import java.util.List;

public record KlimaModel(String ort, MessungDto aktuell, MessungDto durchschnitt, List<MessungDto> historie) {

}
