CREATE DATABASE IF NOT EXISTS `everything`;

GRANT ALL PRIVILIGES `everything`.* TO `root`;
flush privileges;

USE everything;

CREATE TABLE IF NOT EXISTS `budget_category` (
    `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `name` varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `budget` (
    `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `name` varchar(255) NOT NULL,
    `amount` float NOT NULL,
    `budget_category_id` int REFERENCES `budget_category` (`id`)
);

CREATE TABLE IF NOT EXISTS `item` (
    `id` int PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `name` varchar(255) NOT NULL,
    `amount` float NOT NULL,
    `date` datetime NOT NULL,
    `budget_id` int REFERENCES `budget` (`id`)
);
