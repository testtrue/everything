package de.lehrke.boas.everything.budgetmanagement.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemDto {
    
    private Long id;
    private String name;
    private double amount;
    private String budgetname;
    private String budgetCategoryName;
    private LocalDateTime date;

    
}
