package de.lehrke.boas.everything.einkauf.view;

import de.lehrke.boas.everything.einkauf.service.EinkaufItem;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.math.RoundingMode;



public class EinkaufItemViewModel {

    @Getter @Setter
    private String name;
    private Integer menge;
    private BigDecimal preis;

    public EinkaufItemViewModel() {
    }

    public EinkaufItemViewModel(EinkaufItem item) {
        this.name = item.name();
        this.menge = item.menge();
        this.preis = item.preis().setScale(2, RoundingMode.HALF_UP);
    }

    public EinkaufItem toEinkaufItem() {
        return new EinkaufItem(this.name, this.menge, this.preis.setScale(2, RoundingMode.HALF_UP));
    }

    public Double getMenge() {
        return this.menge == null ? null : this.menge.doubleValue();
    }

    public void setMenge(Double menge) {
        this.menge = menge.intValue();
    }

    public Double getPreis() {
        return this.preis == null ? null : this.preis.doubleValue();
    }

    public void setPreis(Double preis) {
        this.preis = BigDecimal.valueOf(preis).setScale(2, RoundingMode.HALF_UP);
    }

    public String getDisplayName() {
        return this.name == null ? "" : this.name;
    }

    public String getDisplayMenge() {
        return this.menge == null ? "" : this.menge.toString();
    }

    public String getDisplayPreis() {
        return this.preis == null ? "" : this.preis.toString();
    }

}
