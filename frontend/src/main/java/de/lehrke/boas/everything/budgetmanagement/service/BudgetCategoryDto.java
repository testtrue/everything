package de.lehrke.boas.everything.budgetmanagement.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BudgetCategoryDto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BudgetCategoryDto {
    private Long id;
    private Double amount;
    private String name;
    private Double remainingAmount;
    
}