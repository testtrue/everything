package de.lehrke.boas.everything.budgetmanagement.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.*;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetDto;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import de.lehrke.boas.everything.budgetmanagement.service.ItemDto;
import de.lehrke.boas.everything.budgetmanagement.service.ItemService;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Route(value = "/budgetmanagement/budget", layout = MyLayout.class, registerAtStartup = true)
@PageTitle("Budgetmanagement")
@RolesAllowed({ "budgetmanagement" })
public class BudgetView extends BasicLayoutForView implements HasUrlParameter<String> {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private Long budgetId;
    private LocalDate month;

    private final BudgetService budgetController;
    private final ItemService itemController;

    private BudgetDto currentBudget;
    private List<ViewItem> data;
    private Grid<ViewItem> grid = new Grid<>(ViewItem.class);
    private Editor<ViewItem> editor = grid.getEditor();
    private Binder<ViewItem> binder = new Binder<>(ViewItem.class);
    private Binder<ViewItem> respBinder = new Binder<>(ViewItem.class);

    private DatePicker datum = new DatePicker(this.month);

    private Column<ViewItem> nameCol;
    private Column<ViewItem> kostenCol;
    private Column<ViewItem> ausgabeCol;
    private Column<ViewItem> respCol;

    public BudgetView(BudgetService budgetController, ItemService itemController) {
        this.budgetController = budgetController;
        this.itemController = itemController;
        this.createMenu();
        this.grid.removeAllColumns();
        this.grid.setAllRowsVisible(true);
        this.grid.setWidthFull();
        this.editor.setBuffered(true);


        respCol = this.grid.addComponentColumn(this::createResponsiveView).setHeader("Budget");
        nameCol = this.grid.addColumn(ViewItem::getName).setHeader("Name");
        kostenCol = this.grid.addColumn(ViewItem::getAmount).setHeader("Kosten");
        ausgabeCol = this.grid.addColumn(ViewItem::getViewDate).setHeader("Ausgegeben Am");

        this.createEditColumn();

        TextField namTextField = new TextField();
        binder.forField(namTextField).asRequired("Name muss angegeben sein").bind("name");
        nameCol.setEditorComponent(namTextField);

        NumberField kostNumberField = new NumberField();
        binder.forField(kostNumberField).asRequired("Kosten müssen angegeben werden").bind("amount");
        kostenCol.setEditorComponent(kostNumberField);

        DateTimePicker amDateTimePicker = new DateTimePicker(LocalDateTime.now());
        binder.forField(amDateTimePicker).asRequired("Datum muss angegeben werden").bind("date");
        ausgabeCol.setEditorComponent(amDateTimePicker);

        this.editor.setBinder(binder);

        this.editor.addSaveListener(e -> {

            this.itemController.createItem(e.getItem().mapToDto());
        });

        this.add(this.grid);
    }

    private void createMenu() {
        MenuBar menubar = new MenuBar();
        menubar.setWidthFull();

        MenuItem back = menubar.addItem(FactoryMethods.createUILabel("Zurück", VaadinIcon.ARROW_BACKWARD));
        back.addClickListener(e -> {
            UI.getCurrent().navigate(BudgetCategoryView.class);
        });

        MenuItem addKategorie = menubar.addItem(FactoryMethods.createUILabel("Ausgabe hinzufügen", VaadinIcon.PLUS));
        addKategorie.addClickListener(e -> {
            ViewItem c = new ViewItem(null, "", 0, this.currentBudget.getName(), this.currentBudget.getCategoryName(),
                    LocalDateTime.now());
            this.grid.getListDataView().addItem(c);
            editor.editItem(c);
        });

        datum.addValueChangeListener(e -> {
            this.month = e.getValue();
            this.loadData(this.budgetId, this.month);
        });
        DatePicker.DatePickerI18n monthFormat = new DatePicker.DatePickerI18n();
        monthFormat.setDateFormats("MM.yyyy", "MMyyyy", "yyyyMM", "yyyy-MM", "yyyy.MM");
        datum.setI18n(monthFormat);
        menubar.addItem(datum, "Monat");

        this.add(menubar);
    }

    private void createEditColumn() {

        Grid.Column<ViewItem> editColumn = this.grid.addComponentColumn(person -> {
            if (Long.valueOf(0L).equals(person.getId())) {
                return null;
            }
            Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
            editButton.addClickListener(e -> {
                if (editor.isOpen()) {
                    editor.cancel();
                    return;
                }
                editor.setBinder(binder);
                grid.getDataProvider().refreshAll();
                editor.editItem(person);
            });
            return editButton;
        });

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData(this.budgetId, this.month);
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton);
        actions.setPadding(false);
        editColumn.setEditorComponent(actions);
    }

    private Component createResponsiveView(ViewItem dto) {
        VerticalLayout vl = new VerticalLayout();
        vl.add(new Html("<div><b>Name:</b> " + dto.getName() + "</div>"));
        vl.add(new Html("<div><b>Kosten:</b> " + dto.getAmount() + "</div>"));
        vl.add(new Html("<div><b>Ausgegeben am:</b> " + dto.getViewDate() + "</div>"));
        Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
        editButton.addClickListener(e -> {
            if (editor.isOpen()) {
                editor.cancel();
                return;
            }
            this.editor.setBinder(respBinder);
            this.respCol.setEditorComponent(this.createResponsiveEditor());
            grid.getDataProvider().refreshAll();
            editor.editItem(dto);
        });
        if (dto.id == null || dto.id != 0) {
            vl.add(new HorizontalLayout(editButton));
        }
        return vl;
    }

    private VerticalLayout createResponsiveEditor() {
        VerticalLayout vl = new VerticalLayout();

        Div b = new Div();
        TextField nameTextField = new TextField();
        respBinder.forField(nameTextField).asRequired("Name muss gefüllt sein").bind("name");
        b.add(new Html("<b>Name:<b> "), nameTextField);
        vl.add(b);

        Div a = new Div();
        NumberField amountNumberField = new NumberField();
        respBinder.forField(amountNumberField).asRequired("Ausgabe muss > 0 sein").bind("amount");
        a.add(new Html("<b>Kosten:<b> "), amountNumberField);
        vl.add(a);

        Div d = new Div();
        DateTimePicker dateField = new DateTimePicker();
        respBinder.forField(dateField).asRequired("Datum angegeben sein").bind("date");
        d.add(new Html("<b>Ausgegeben am:<b> "), dateField);
        vl.add(d);

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData(budgetId, month);
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton);
        actions.setPadding(false);
        vl.add(actions);

        return vl;
    }

    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        this.budgetId = Long.parseLong(event.getLocation().getQueryParameters().getParameters().get("budgetId").get(0));
        this.month = LocalDate.from(DateTimeFormatter.ISO_DATE
                .parse(event.getLocation().getQueryParameters().getParameters().get("month").get(0)));
        this.datum.setValue(this.month);
        this.loadData(budgetId, this.month);
    }

    private void loadData(Long id, LocalDate month) {
        Optional<LocalDate> optMont = Optional.of(month);
        this.currentBudget = this.budgetController.getBudgetById(id, optMont);

        this.data = new ArrayList<>(this.itemController.getAllItemByBudgetCategory(this.currentBudget.getName(), optMont).getBody().stream().map(ViewItem::new).toList());
        this.data.add(0,
                new ViewItem(0L, "Budget: " + this.currentBudget.getName(), this.currentBudget.getAmount().doubleValue(),
                        this.currentBudget.getName(), this.currentBudget.getCategoryName(),
                        LocalDateTime.of(this.month, LocalTime.now())));
        this.grid.setItems(this.data);
    }

    @Override
    public BasicLayoutDto getGridDefinition(boolean small) {
        if (small) {
            return new BasicLayoutDto(new boolean[] {true, false, false, false, false } , respBinder, editor, grid);
        }
        return new BasicLayoutDto(new boolean[] {false, true, true, true, true}, binder, editor, grid);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class ViewItem {
        private Long id;
        private String name;
        private double amount;
        private String budgetname;
        private String budgetCategoryName;
        private LocalDateTime date;

        public ViewItem(ItemDto dto) {
            this.id = dto.getId();
            this.name = dto.getName();
            this.amount = dto.getAmount();
            this.budgetname = dto.getBudgetname();
            this.budgetCategoryName = dto.getBudgetCategoryName();
            this.date = dto.getDate();
        }

        public ItemDto mapToDto() {
            return new ItemDto(budgetId, name, amount, budgetname, budgetCategoryName, date);
        }

        public String getViewDate() {
            return this.date.format(DATE_TIME_FORMATTER);
        }
    }

}
