package de.lehrke.boas.everything.climate.views;

import java.time.LocalDateTime;


public record MessungDto(String ort, LocalDateTime date, Werte werte) {

}
