package de.lehrke.boas.everything.gamify.config;

import de.lehrke.boas.everything.gamify.service.GamifyService;
import de.lehrke.boas.everything.inventory.service.InventoryWorkflows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class GamifyConfig {

    @Value("${de.lehrke.boas.everything.gamify.url;http://localhost:8080}")
    private String restServiceUrl;
        
    @Bean
    public GamifyService gamifyService(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());
        
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(GamifyService.class);
    }
}
