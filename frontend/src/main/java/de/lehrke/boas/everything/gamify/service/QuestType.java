package de.lehrke.boas.everything.gamify.service;

public enum QuestType {
    URGENT,
    NORMAL,
    SIDE,
    PERPETUAL
}
