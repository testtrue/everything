package de.lehrke.boas.everything.climate.views;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;

import java.util.List;

public interface KlimaWorkflows {

    @GetExchange("/climate/")
    List<KlimaModel> getKlimaDaten(@RequestParam Granularitaet granularitaet);

}
