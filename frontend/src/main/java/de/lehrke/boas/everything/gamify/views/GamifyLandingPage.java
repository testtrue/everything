package de.lehrke.boas.everything.gamify.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.*;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetDto;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import de.lehrke.boas.everything.budgetmanagement.service.ItemDto;
import de.lehrke.boas.everything.budgetmanagement.service.ItemService;
import de.lehrke.boas.everything.budgetmanagement.views.BudgetCategoryView;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.utils.MenuItem;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import de.lehrke.boas.everything.gamify.service.GamifyService;
import de.lehrke.boas.everything.gamify.service.Player;
import jakarta.annotation.security.RolesAllowed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Route(value = "/gamify/", layout = MyLayout.class, registerAtStartup = true)
@PageTitle("Gamify your Life")
@RolesAllowed({ "budgetmanagement" })
@MenuItem("The Game")
public class GamifyLandingPage extends VerticalLayout implements HasUrlParameter<String> {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private LocalDate month;

    private final GamifyService gamifyService;

    private Player player;


    public GamifyLandingPage(GamifyService gamifyService) {
        this.gamifyService = gamifyService;
        this.month = LocalDate.now();
        this.createMenu();
    }

    private void createMenu() {
        MenuBar menubar = new MenuBar();
        menubar.setWidthFull();


        this.add(menubar);
    }

    @Override
    public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
        this.loadData();
    }

    private void loadData() {
        try {
            this.removeAll();
            this.player = gamifyService.getPlayer();
            this.add(new H1("Hello " + this.player.name()));
            this.add(new H2( "Your stats"));
            this.add(new Div("Exp: " + this.player.experience()));
            this.add(new Div("Open quests: " + this.player.questList().quests().size()));
        } catch (Exception e) {
            this.add(new H1("No Player found"));
            this.add(new RouterLink("Create new player", NewPlayerView.class));
        }

    }

}
