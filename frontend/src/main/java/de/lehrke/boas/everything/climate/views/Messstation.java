package de.lehrke.boas.everything.climate.views;


public record Messstation(String ort,
String sensorTyp,
Object sensor) {
}
