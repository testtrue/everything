package de.lehrke.boas.everything.gamify.service;

public record MiniGameRule(Integer value,AdvancementType advancementType) {
}
