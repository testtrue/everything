package de.lehrke.boas.everything.budgetmanagement.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class BudgetManagementConfig {

    @Value("${de.lehrke.boas.everything.budgetmanagement.url}")
    private String restServiceUrl;
        
    @Bean
    public BudgetCategoryService budgetCategoryController(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());
        
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(BudgetCategoryService.class);
    }

    @Bean
    public BudgetService budgetController(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());

        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(BudgetService.class);
    }

    @Bean
    public ItemService itemController(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());

        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(ItemService.class);
    }
}
