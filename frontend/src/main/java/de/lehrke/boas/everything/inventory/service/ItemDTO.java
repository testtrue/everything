package de.lehrke.boas.everything.inventory.service;

import lombok.Builder;

import java.util.List;

@Builder
public record ItemDTO(Long id, String name, Integer availableAmount, Integer threshhold, List<String> barcodes) {

}
