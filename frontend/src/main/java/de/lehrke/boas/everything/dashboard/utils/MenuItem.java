package de.lehrke.boas.everything.dashboard.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MenuItem {
    String value() default "";
    int order() default Integer.MAX_VALUE;
}
