package de.lehrke.boas.everything.gamify.service;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;

public interface GamifyService {

    @GetExchange("/player/")
    Player getPlayer();

    @PostExchange("/player/createPLayer")
    void createNewPlayer(@RequestBody String value);
}
