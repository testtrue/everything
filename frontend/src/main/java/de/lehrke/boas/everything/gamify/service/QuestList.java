package de.lehrke.boas.everything.gamify.service;

import java.util.List;
import java.util.Map;

public record QuestList(Map<QuestType, List<Quest>> quests,
        Map<QuestType, List<Quest>> archive) {
}
