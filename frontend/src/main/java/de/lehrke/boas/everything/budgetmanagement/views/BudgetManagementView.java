package de.lehrke.boas.everything.budgetmanagement.views;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import de.lehrke.boas.everything.dashboard.utils.MenuItem;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import jakarta.annotation.security.RolesAllowed;

@Route(layout = MyLayout.class)
@PageTitle("Budgetmanagement")
@RolesAllowed({"budgetmanagement"})
@MenuItem("Budget Management")
public class BudgetManagementView extends VerticalLayout {
    
    public BudgetManagementView() {
        this.add(new RouterLink("Budgetkategorien", BudgetCategoryView.class));
    }
}
