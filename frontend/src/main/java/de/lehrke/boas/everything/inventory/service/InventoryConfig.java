package de.lehrke.boas.everything.inventory.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Configuration
public class InventoryConfig {

    @Value("${de.lehrke.boas.everything.inventory.url}")
    private String restServiceUrl;
        
    @Bean
    public InventoryWorkflows inventoryWorkflows(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());
        
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(InventoryWorkflows.class);
    }
}
