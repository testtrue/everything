package de.lehrke.boas.everything.einkauf.service;

import java.math.BigDecimal;

public record EinkaufItem(String name, Integer menge, BigDecimal preis) {
}
