package de.lehrke.boas.everything.gamify.service;

public record TreatRule(Integer value,AdvancementType advancementType) {
}
