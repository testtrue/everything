package de.lehrke.boas.everything.inventory.service;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import java.util.List;
import java.util.Optional;

public interface InventoryWorkflows {
    
    @GetExchange("/items")
     List<ItemDTO> showAllItems();

    @GetExchange("/items/search")
     Optional<ItemDTO> searchItemByProductCode(@RequestParam String productCode);

    @PostExchange("/items/new")
     ItemDTO listNewItem(@RequestBody DomainDtos.NewItem event) ;

    @PostExchange("/items/stock")
     Optional<ItemDTO> stockItem(@RequestBody DomainDtos.InsertItem event) ;

    @PutExchange("/items/update")
     Optional<ItemDTO> modifyItem(DomainDtos.ModifyItem event) ;

    @PostExchange("/items/withdraw")
     Optional<ItemDTO> withdrawItem(DomainDtos.RemoveItem event);

    @GetExchange("/items/export")
     List<ItemDTO> exportShoppingList() ;

}
