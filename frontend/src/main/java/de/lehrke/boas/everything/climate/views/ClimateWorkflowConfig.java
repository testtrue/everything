package de.lehrke.boas.everything.climate.views;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;


@Configuration
public class ClimateWorkflowConfig {
    
    @Value("${de.lehrke.boas.everything.climate.url}")
    private String restServiceUrl;

    @Bean
    public KlimaWorkflows klimaWorkflows(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());
        
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(KlimaWorkflows.class);
    }

    @Bean
    public MessstationWorkflows messstationWorkflows(RestClient.Builder customRestClientBuilder) {

        // WebClientAdapter adapter = WebClientAdapter.create(webClientBuilder.baseUrl(restServiceUrl).build());
        RestClientAdapter adapter = RestClientAdapter.create(customRestClientBuilder.baseUrl(this.restServiceUrl).build());
        
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(adapter).build();

        return factory.createClient(MessstationWorkflows.class);
    }

}
