package de.lehrke.boas.everything.dashboard.views;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import com.vaadin.flow.spring.security.AuthenticationContext;
import de.lehrke.boas.everything.dashboard.utils.MenuItem;


@Route(value = "", layout = MyLayout.class)
@PageTitle("Home")
@AnonymousAllowed
@MenuItem(value = "Home", order = Integer.MIN_VALUE)
public class Homepage extends VerticalLayout {
    
    public Homepage(AuthenticationContext authContext) {
        Div someText = new Div();
        someText.setText("Home");

        
        
        this.add(someText);
    }
}
