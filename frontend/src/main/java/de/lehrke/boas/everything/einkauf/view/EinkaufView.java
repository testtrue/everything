package de.lehrke.boas.everything.einkauf.view;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.utils.MenuItem;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import de.lehrke.boas.everything.einkauf.service.Einkauf;
import de.lehrke.boas.everything.einkauf.service.KafkaService;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Route(layout = MyLayout.class)
@PageTitle("Einkauf")
@RolesAllowed({ "inventory" })
@Slf4j
@MenuItem("Einkauf")
public class EinkaufView extends BasicLayoutForView<EinkaufItemViewModel> {

    private final List<EinkaufItemViewModel> data = new ArrayList<>();
    Grid<EinkaufItemViewModel> grid = new Grid<>(EinkaufItemViewModel.class, false);

    private final Editor<EinkaufItemViewModel> editor = grid.getEditor();
    private final Binder<EinkaufItemViewModel> binder = new Binder<>(EinkaufItemViewModel.class);

    private final TextField name = new TextField("Name");
    private final NumberField amount = new NumberField("Menge");
    private final NumberField price = new NumberField("Preis");

    KafkaService kafkaService;

    public EinkaufView(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
        this.grid.setItems(this.data);

        MenuBar menuBar = new MenuBar();
        menuBar.addItem("Artikel hinzufügen", (event) -> {
            EinkaufItemViewModel e = new EinkaufItemViewModel();
            this.data.add(e);
            this.grid.getDataProvider().refreshAll();
            this.editor.editItem(e);

        });
        binder.forField(this.name).asRequired().bind("name");
        binder.forField(this.amount).asRequired().bind("menge");
        binder.forField(this.price).asRequired().bind("preis");
        this.editor.setBinder(binder);
        this.editor.setBuffered(true);
        this.add(menuBar);
        initGrid();
        this.add(new Button("Speichern", buttonClickEvent -> {
            try {
                Einkauf neuerEinkauf = new Einkauf(LocalDate.now(), this.data.stream().map(EinkaufItemViewModel::toEinkaufItem).toList());
                this.kafkaService.pushData(neuerEinkauf);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    private void initGrid() {
        this.grid.removeAllColumns();
        this.grid.setSelectionMode(Grid.SelectionMode.NONE);
        this.grid.setAllRowsVisible(true);

        ComponentEventListener<ClickEvent<Button>> click = (e) -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Notification.Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                this.editor.closeEditor();
            }
            this.grid.getDataProvider().refreshAll();
        };

        this.grid.addComponentColumn(v -> {
            VerticalLayout vl = new VerticalLayout();
            vl.add(new Div(v.getDisplayName()));
            vl.add(new Div(v.getDisplayMenge()));
            vl.add(new Div(v.getDisplayPreis()));
            return vl;
        }).setEditorComponent(v -> {
            VerticalLayout vl = new VerticalLayout();
            vl.add(this.name);
            vl.add(this.amount);
            vl.add(this.price);
            vl.add(new Button(FactoryMethods.createUILabel(VaadinIcon.CHECK), click));
            return vl;
        });
        this.grid.addColumn(EinkaufItemViewModel::getName).setHeader("Name").setEditorComponent(this.name);
        this.grid.addColumn(EinkaufItemViewModel::getMenge).setHeader("Menge").setEditorComponent(this.amount);
        this.grid.addColumn(EinkaufItemViewModel::getPreis).setHeader("Gesamtpreis").setEditorComponent(this.price);
        this.grid.addComponentColumn(v -> new Button(FactoryMethods.createUILabel(VaadinIcon.CLOSE), e -> {this.data.remove(v); this.grid.getDataProvider().refreshAll();}))
                .setEditorComponent(new Button(FactoryMethods.createUILabel(VaadinIcon.CHECK), click));

        this.add(grid);
    }

    @Override
    public BasicLayoutForView<EinkaufItemViewModel>.BasicLayoutDto getGridDefinition(boolean small) {
        if (small) {
            return new BasicLayoutDto(new boolean[] { true, false, false, false }, this.binder, this.editor, grid);
        }
        return new BasicLayoutDto(new boolean[] { false, true, true, true }, this.binder, this.editor, grid);
    }
}
