package de.lehrke.boas.everything.gamify.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Input;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import de.lehrke.boas.everything.gamify.service.GamifyService;
import jakarta.annotation.security.RolesAllowed;

import java.awt.*;

@Route(value = "/gamify/newplayer", layout = MyLayout.class, registerAtStartup = true)
@PageTitle("Gamify your Life")
@RolesAllowed({ "budgetmanagement" })
public class NewPlayerView extends VerticalLayout {

    private final GamifyService gamifyService;

    public NewPlayerView(GamifyService gamifyService) {
        this.gamifyService = gamifyService;
        TextField playerName = new TextField("Player Name");
        playerName.setWidth("100%");
        playerName.setRequired(true);
        playerName.setClearButtonVisible(true);
        playerName.setPrefixComponent(VaadinIcon.EDIT.create());
        this.add(playerName);
        Button save = new Button("Save");
        save.addClickListener(e -> {
            this.gamifyService.createNewPlayer(playerName.getValue());
        });
        save.setIcon(VaadinIcon.SAFE.create());
        this.add(save);
    }



}
