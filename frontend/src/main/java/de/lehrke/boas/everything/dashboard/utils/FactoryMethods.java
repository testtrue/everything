package de.lehrke.boas.everything.dashboard.utils;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public abstract class FactoryMethods {

    public static HorizontalLayout createUILabel(String text, VaadinIcon vaadinIcon) {
        HorizontalLayout d = new HorizontalLayout();
        Div i = new Div();
        Div t = new Div();

        i.add(vaadinIcon.create());
        t.add(text);
        d.add(i, t);
        d.getStyle().set(text, text);
        return d;
    }

    public static HorizontalLayout createUILabel(String text) {
        HorizontalLayout d = new HorizontalLayout();
        Div t = new Div();
        t.add(text);
        d.add(t);
        return d;
    }

    public static HorizontalLayout createUILabel(VaadinIcon icon) {
        HorizontalLayout d = new HorizontalLayout();
        Div i = new Div();
        i.add(icon.create());
        d.add(i);
        return d;
    }
}
