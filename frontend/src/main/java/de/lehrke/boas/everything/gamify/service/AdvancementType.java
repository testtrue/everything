package de.lehrke.boas.everything.gamify.service;

public enum AdvancementType {
    EXPERIENCE,
    LORE
}
