package de.lehrke.boas.everything.budgetmanagement.service;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface ItemService {

    @GetExchange("/items/{id}")
    public ResponseEntity<ItemDto> getItemById(@PathVariable Long id);

    @GetExchange("/items/")
    public ResponseEntity<List<ItemDto>> getAllItemByBudgetCategory(@RequestParam("budget") String categoryName, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth);

    @PostExchange("/items/")
    public ItemDto createItem(@RequestBody ItemDto itemDto);
}
