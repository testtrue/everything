package de.lehrke.boas.everything.budgetmanagement.service;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface BudgetService {

    @GetExchange("/budgets/")
    public List<BudgetDto> getAllBudgetsByBudgetCategory(@RequestParam("budgetCategoryName") Optional<String> name, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth);

    @GetExchange("/budgets/{id}")
    public BudgetDto getBudgetById(@PathVariable("id") Long id, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth);

    @GetExchange("/budgets/{id}")
    public BudgetDto updateBudget(@PathVariable Long id, @RequestBody BudgetDto budgetDto);

    @DeleteExchange("/budgets/{id}")
    public void deleteBudget(@PathVariable Long id);

    @PostExchange("/budgets/")
    public BudgetDto createBudgetCategory(@RequestBody BudgetDto budgetDto);
}
