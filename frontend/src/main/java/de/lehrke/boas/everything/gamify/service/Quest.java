package de.lehrke.boas.everything.gamify.service;

import java.util.UUID;

public record Quest(UUID id,
        String quest,
        QuestType questType,
        Integer advancement,
        AdvancementType type) {
}
