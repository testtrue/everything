package de.lehrke.boas.everything.budgetmanagement.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.Route;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryDto;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetCategoryService;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetDto;
import de.lehrke.boas.everything.budgetmanagement.service.BudgetService;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Route(value = "/budgetmanagement/budgetcategory", layout = MyLayout.class, registerAtStartup = true)
@PageTitle("Budgetmanagement")
@RolesAllowed({"budgetmanagement"})
@Slf4j
public class BudgetCategoryView extends BasicLayoutForView {

    private final BudgetCategoryService budgetCategoryController;
    private final BudgetService budgetController;
    private LocalDate month = LocalDate.now();

    private List<ModelForTreeGrid> data;
    private final TreeGrid<ModelForTreeGrid> grid = new TreeGrid<>(ModelForTreeGrid.class);
    private final Editor<ModelForTreeGrid> editor = this.grid.getEditor();
    private final Binder<ModelForTreeGrid> binderForBudgetCategory;
    private final Binder<ModelForTreeGrid> binderForBudget;
    private final Binder<ModelForTreeGrid> respBinderForBudgetCategory;
    private final Binder<ModelForTreeGrid> respBinderForBudget;
    private final Grid.Column<ModelForTreeGrid> nameCol;
    private final Grid.Column<ModelForTreeGrid> amountCol;
    private Column<ModelForTreeGrid> respCol;

    public BudgetCategoryView(BudgetCategoryService budgetCategoryController, BudgetService budgetController) {
        this.budgetCategoryController = budgetCategoryController;
        this.budgetController = budgetController;
        this.binderForBudget = this.getBudgetBinder();
        this.binderForBudgetCategory = this.getBudgetCategoryBinder();
        this.respBinderForBudget = new Binder<>(ModelForTreeGrid.class);
        this.respBinderForBudgetCategory = new Binder<>(ModelForTreeGrid.class);

        this.createMenu();

        grid.setAllRowsVisible(true);
        this.grid.removeAllColumns();
        editor.setBuffered(true);


        nameCol = this.grid.addHierarchyColumn(ModelForTreeGrid::getColumnName).setHeader("Name");
        respCol = this.grid.addComponentHierarchyColumn(category -> {
            VerticalLayout vl = new VerticalLayout();
            vl.add(new Html("<div><b>Name:</b> " + category.getName() + "</div>"));
            vl.add(new Html("<div><b>Verbleibendes Budget:</b> " + category.getRemainingAmount() + "</div>"));
            vl.add(new Html("<div><b>Budget:</b> " + category.getAmount() + "</div>"));


            HorizontalLayout actions = new HorizontalLayout();
            Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
            editButton.addClickListener(e -> {
                if (editor.isOpen()) {
                    editor.cancel();
                    return;
                }
                this.respCol.setEditorComponent((Component) null);
                if (category.getParent() == null) {
                    editor.setBinder(this.respBinderForBudgetCategory);
                    this.respCol.setEditorComponent(this.createResponsiveEditorBudgetCategory());
                } else {
                    editor.setBinder(this.respBinderForBudget);
                    this.respCol.setEditorComponent(this.createResponsiveEditorBudget());
                }
                grid.getDataProvider().refreshAll();
                editor.editItem(category);
            });

            if (category.getParent() == null) {
                Button neuesBudget = new Button(FactoryMethods.createUILabel("Neues Budget", VaadinIcon.PLUS));
                neuesBudget.addClickListener(e -> {
                    ModelForTreeGrid model = new ModelForTreeGrid();
                    model.setParent(category.getName());
                    this.grid.getTreeData().addItem(category, model);
                    this.grid.setTreeData(this.grid.getTreeData());
                    this.grid.getDataCommunicator().getKeyMapper().key(model);
                    this.grid.expand(category);
                    this.respCol.setEditorComponent((Component) null);
                    this.respCol.setEditorComponent(this.createResponsiveEditorBudget());
                    this.setBinderForModel(model);
                    editor.editItem(model);
                });
                actions.add(neuesBudget);
            } else {
                Button geheZuBudget = new Button(FactoryMethods.createUILabel("Öffnen", VaadinIcon.CHEVRON_RIGHT_SMALL));
                geheZuBudget.addClickListener(e -> {
                    Map<String, List<String>> parameters = new HashMap<>();
                    parameters.put("budgetId", List.of(category.id.toString()));
                    parameters.put("month", List.of(this.month.format(DateTimeFormatter.ISO_DATE)));
                    QueryParameters queryParameters = new QueryParameters(parameters);
                    UI.getCurrent().navigate(BudgetView.class, queryParameters);
                });
                actions.add(geheZuBudget);
            }
            actions.add(editButton);
            vl.add(actions);
            return vl;
        }).setHeader("Budgets");
        this.grid.addColumn(ModelForTreeGrid::getRemainingAmount).setHeader("Verbleibendes Budget");
        amountCol = this.grid.addColumn(ModelForTreeGrid::getAmount).setHeader("Budget");

        this.grid.addItemClickListener(i -> {

            if (this.isResponsive()) {
                return;
            }
            if (i.getItem() == null) {
                return;
            }
            String parent = i.getItem().parent;

            if (parent != null && !i.getColumn().getHeaderText().equals("")) {
                Map<String, List<String>> parameters = new HashMap<>();
                parameters.put("budgetId", List.of(i.getItem().id.toString()));
                parameters.put("month", List.of(this.month.format(DateTimeFormatter.ISO_DATE)));
                QueryParameters queryParameters = new QueryParameters(parameters);
                UI.getCurrent().navigate(BudgetView.class, queryParameters);
            }

        });

        this.grid.addThemeVariants(GridVariant.LUMO_ROW_STRIPES);

        this.grid.addComponentColumn(i -> {
            if (i.parent != null) {
                return null;
            }

            Button neuesBudget = new Button(FactoryMethods.createUILabel("Neues Budget", VaadinIcon.PLUS));
            neuesBudget.addClickListener(e -> {
                ModelForTreeGrid model = new ModelForTreeGrid();
                model.setParent(i.getName());
                this.grid.getTreeData().addItem(i, model);
                this.grid.setTreeData(this.grid.getTreeData());
                this.grid.getDataCommunicator().getKeyMapper().key(model);
                this.grid.expand(i);
                this.setBinderForModel(model);
                editor.editItem(model);
            });
            return neuesBudget;
        });

        editor.addSaveListener(e -> {
            ModelForTreeGrid model = e.getItem();
            if (model.getParent() == null) {
                BudgetCategoryDto dto = new BudgetCategoryDto(model.getId(), model.amount, model.getName(),
                        model.getRemainingAmount());
                if (dto.getId() == null) {
                    this.budgetCategoryController.createBudgetCategory(dto);
                } else {
                    this.budgetCategoryController.updateBudgetCategory(dto.getId(), dto);
                }
            } else {
                BudgetDto dto = new BudgetDto(model.getId(), BigDecimal.valueOf(model.getAmount()), model.getName(),
                        BigDecimal.ZERO, model.getParent());
                if (dto.getId() == null) {
                    this.budgetController.createBudgetCategory(dto);
                } else {
                    this.budgetController.updateBudget(dto.getId(), dto);
                }
            }
        });

        this.createEditColumn();
        this.add(this.grid);
        this.loadData();
    }

    private void createEditColumn() {

        Grid.Column<ModelForTreeGrid> editColumn = this.grid.addComponentColumn(person -> {
            Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
            editButton.addClickListener(e -> {
                if (editor.isOpen()) {
                    editor.cancel();
                    return;
                }
                grid.getDataProvider().refreshAll();
                setBinderForModel(person);
                editor.editItem(person);
            });
            return editButton;
        });

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE),
                e -> {
                    if (!editor.save()) {
                        Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                                .addThemeVariants(NotificationVariant.LUMO_ERROR);
                    } else {
                        Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                                .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                        this.loadData();
                    }
                });
        Button cancelButton = new Button(FactoryMethods.createUILabel(VaadinIcon.CLOSE), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        Button removeButton = new Button(VaadinIcon.TRASH.create(), e -> {
            ModelForTreeGrid item = editor.getItem();
            if (item.getParent() != null) {
                this.budgetController.deleteBudget(item.getId());
            } else {
                this.budgetCategoryController.deleteBudgetCategory(item.getId());
            }
            this.loadData();
        });

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton, removeButton);
        actions.setPadding(false);
        editColumn.setEditorComponent(actions);
    }

    private VerticalLayout createResponsiveEditorBudget() {
        VerticalLayout vl = new VerticalLayout();

        Div b = new Div();
        TextField budgetTextField = new TextField();
        respBinderForBudget.forField(budgetTextField).asRequired("Kategorie muss gefüllt sein").bind("name");
        b.add(new Html("<b>Name:<b> "), budgetTextField);
        vl.add(b);

        Div a = new Div();
        NumberField amountNumberField = new NumberField();
        respBinderForBudget.forField(amountNumberField).asRequired("Budget muss > 0 sein").bind("amount");
        a.add(new Html("<b>Budget:<b> "), amountNumberField);
        vl.add(a);

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData();
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton);
        actions.setPadding(false);
        vl.add(actions);

        return vl;
    }

    private VerticalLayout createResponsiveEditorBudgetCategory() {
        VerticalLayout vl = new VerticalLayout();

        Div t = new Div();
        TextField categoryTextField = new TextField();
        respBinderForBudgetCategory.forField(categoryTextField).asRequired("Kategorie muss gefüllt sein").bind("name");
        t.add(new Html("<b>Name:<b> "), categoryTextField);
        vl.add(t);

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData();
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton);
        actions.setPadding(false);
        vl.add(actions);

        return vl;
    }

    private void setBinderForModel(ModelForTreeGrid model) {
        Binder<ModelForTreeGrid> binder;
        if (this.isResponsive()) {
            binder = model.getParent() == null ? this.respBinderForBudgetCategory : this.respBinderForBudget;
        } else {
            binder = model.getParent() == null ? this.binderForBudgetCategory
                    : this.binderForBudget;
            binder.getBinding("name").ifPresentOrElse(b -> nameCol.setEditorComponent((Component) b.getField()),
                    () -> nameCol.setEditorComponent((Component) null));
            binder.getBinding("amount").ifPresentOrElse(b -> amountCol.setEditorComponent((Component) b.getField()),
                    () -> amountCol.setEditorComponent((Component) null));
        }
        editor.setBinder(binder);
    }

    private Binder<ModelForTreeGrid> getBudgetCategoryBinder() {
        Binder<ModelForTreeGrid> binder = new Binder<>(ModelForTreeGrid.class);

        TextField categoryTextField = new TextField();
        binder.forField(categoryTextField).asRequired("Kategorie muss gefüllt sein").bind("name");
        return binder;
    }

    private Binder<ModelForTreeGrid> getBudgetBinder() {
        Binder<ModelForTreeGrid> binder = new Binder<>(ModelForTreeGrid.class);

        TextField categoryTextField = new TextField();
        binder.forField(categoryTextField).asRequired("Kategorie muss gefüllt sein").bind("name");

        NumberField amountNumberField = new NumberField();
        binder.forField(amountNumberField).asRequired("Budget muss > 0 sein").bind("amount");

        return binder;
    }

    private void createMenu() {
        MenuBar menubar = new MenuBar();
        menubar.setWidthFull();
        MenuItem addKategorie = menubar.addItem(FactoryMethods.createUILabel("Neue Kategorie", VaadinIcon.PLUS));
        addKategorie.addClickListener(e -> {
            ModelForTreeGrid c = new ModelForTreeGrid();
            this.grid.getTreeData().addRootItems(c);
            this.grid.setTreeData(this.grid.getTreeData());
            this.setBinderForModel(c);
            editor.editItem(c);
        });

        DatePicker datum = new DatePicker(this.month);
        datum.addValueChangeListener(e -> {
            this.month = e.getValue();
            this.loadData();
        });
        DatePicker.DatePickerI18n monthFormat = new DatePicker.DatePickerI18n();
        monthFormat.setDateFormats("MM.yyyy", "MMyyyy", "yyyyMM", "yyyy-MM", "yyyy.MM");
        datum.setI18n(monthFormat);
        menubar.addItem(datum, "Monat");

        this.add(menubar);
    }

    private boolean isResponsive() {
        return this.grid.getColumns().get(1).isVisible();
    }

    private void loadData() {
        this.data = this.budgetCategoryController.getAllBudgetCategories(Optional.empty(), Optional.of(this.month))
                .stream().map(ModelForTreeGrid::new).toList();
        TreeData<ModelForTreeGrid> t = new TreeData<>();
        this.data.forEach(bc -> {
            t.addRootItems(bc);
            t.addItems(bc, loadBudgets(bc));
        });
        this.grid.setTreeData(t);
        this.grid.getDataProvider().refreshAll();
    }

    private List<ModelForTreeGrid> loadBudgets(ModelForTreeGrid dto) {
        return this.budgetController.getAllBudgetsByBudgetCategory(Optional.of(dto.getName()), Optional.of(this.month))
                .stream().map(ModelForTreeGrid::new).toList();
    }

    @Data
    public class ModelForTreeGrid {
        private Long id;
        private Double amount;
        private String name = "";
        private Double remainingAmount;
        private String parent;

        ModelForTreeGrid() {

        }

        ModelForTreeGrid(BudgetCategoryDto category) {
            this.id = category.getId();
            this.amount = category.getAmount();
            this.name = category.getName();
            this.remainingAmount = category.getRemainingAmount();
            this.parent = null;
        }

        ModelForTreeGrid(BudgetDto budget) {
            this.id = budget.getId();
            this.amount = budget.getAmount().doubleValue();
            this.name = budget.getName();
            this.remainingAmount = budget.getRemainingAmount().doubleValue();
            this.parent = budget.getCategoryName();
        }

        public String getColumnName() {
            return this.name;
        }

    }

    @Override
    public BasicLayoutDto getGridDefinition(boolean small) {
        if (small) {
            return new BasicLayoutDto(new boolean[]{false, true, false, false, false, false},
                    respBinderForBudget,
                    editor, grid);
        } else {
            return new BasicLayoutDto(new boolean[]{true, false, true, true, true, true}, binderForBudget,
                    editor,
                    grid);
        }
    }

}
