package de.lehrke.boas.everything.inventory.views;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.wontlost.zxing.Constants;
import com.wontlost.zxing.ZXingVaadinReader;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import de.lehrke.boas.everything.inventory.service.DomainDtos;
import de.lehrke.boas.everything.inventory.service.DomainDtos.InsertItem;
import de.lehrke.boas.everything.inventory.service.DomainDtos.RemoveItem;
import de.lehrke.boas.everything.inventory.service.InventoryWorkflows;
import de.lehrke.boas.everything.inventory.service.ItemDTO;
import de.lehrke.boas.everything.inventory.views.InventoryView.ViewItem;
import jakarta.annotation.security.RolesAllowed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.vaadin.olli.ClipboardHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Route(layout = MyLayout.class)
@PageTitle("Inventory")
@RolesAllowed({ "inventory" })
@Slf4j
@de.lehrke.boas.everything.dashboard.utils.MenuItem("Bestand")
public class InventoryView extends BasicLayoutForView<ViewItem> {

    private InventoryWorkflows inventoryWorkflows;
    private MenuBar menuBar;
    private Grid<ViewItem> grid;
    private Editor<ViewItem> editor;
    private Binder<ViewItem> binder = new Binder<>(ViewItem.class);
    private Binder<ViewItem> respBinder = new Binder<>(ViewItem.class);
    private List<ViewItem> data;
    private Column<ViewItem> nameColumn;
    private Column<ViewItem> threshholdColumn;
    private Column<ViewItem> respColumn;
    private Dialog insertItemsDialog = new Dialog();
    private Dialog withdrawItemsDialog = new Dialog();
    private ComboBox<ViewItem> selectableProductsForInsert;
    private ComboBox<ViewItem> selectableProductsForWithdraw;

    public InventoryView(InventoryWorkflows inventoryWorkflows) {
        this.inventoryWorkflows = inventoryWorkflows;
        this.createMenu();
        this.createDialogs();
        this.createGrid();
        this.configureEditor();
        this.createResponsiveEditor();

        this.loadData();

    }

    private void createMenu() {
        this.menuBar = new MenuBar();
        this.menuBar.setOpenOnHover(true);
        this.menuBar.setSizeFull();
        this.menuBar.addItem(FactoryMethods.createUILabel("Bestand hinzufügen", VaadinIcon.PLUS_CIRCLE), event -> {
            this.insertItemsDialog.open();
        });

        this.menuBar.addItem(FactoryMethods.createUILabel("Bestand entnehmen", VaadinIcon.MINUS_CIRCLE), event -> {
            this.withdrawItemsDialog.open();
        });

        this.menuBar.addItem(FactoryMethods.createUILabel("Neuer Gegenstand", VaadinIcon.PLUS), event -> {
            this.data.add(new ViewItem());
            this.grid.getDataProvider().refreshAll();
        });

        ClipboardHelper clipboardHelper = new ClipboardHelper(null,
                FactoryMethods.createUILabel("Einkaufsliste erstellen", VaadinIcon.LIST_UL));
        MenuItem exportListItem = this.menuBar.addItem(clipboardHelper);
        exportListItem.addClickListener(e -> {
            String list = this.inventoryWorkflows.exportShoppingList().stream()
                    .map(i -> String.format("%d %s",
                            i.threshhold().equals(i.availableAmount()) ? 1 : i.threshhold() - i.availableAmount(),
                            i.name()))
                    .collect(Collectors.joining("\n"));
            clipboardHelper.setContent(list);

            Notification.show("Liste kopiert", 10000, Position.TOP_END);
        });

        // this.add(clipboardHelper);

        this.add(this.menuBar);
    }

    private void createGrid() {
        this.grid = new Grid<>(ViewItem.class);
        this.editor = this.grid.getEditor();
        this.grid.removeAllColumns();
        this.grid.setAllRowsVisible(true);
        respColumn = this.grid.addComponentColumn(item -> {
            VerticalLayout vl = new VerticalLayout();
            vl.add(new Html("<div><b>Name:</b> " + item.getName() + "</div>"));
            vl.add(new Html("<div><b>Bestand:</b> " + item.getAvailableAmount() + "</div>"));
            vl.add(new Html("<div><b>Mindestbestand:</b> " + item.getThreshhold() + "</div>"));
            Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
            editButton.addClickListener(e -> {
                if (editor.isOpen()) {
                    editor.cancel();
                    return;
                }
                grid.getDataProvider().refreshAll();
                editor.editItem(item);
            });
            vl.add(editButton);
            return vl;
        }).setHeader("Produkte");
        this.nameColumn = this.grid.addColumn(ViewItem::getName).setHeader("Name");
        this.grid.addColumn(ViewItem::getAvailableAmount).setHeader("Bestand");
        this.threshholdColumn = this.grid.addColumn(ViewItem::getThreshhold).setHeader("Mindestbestand");
        Grid.Column<ViewItem> editColumn = this.grid.addComponentColumn(person -> {
            Button editButton = new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT));
            editButton.addClickListener(e -> {
                if (editor.isOpen()) {
                    editor.cancel();
                    return;
                }
                grid.getDataProvider().refreshAll();
                editor.editItem(person);
            });
            return editButton;
        });

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData();
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        Button editBarcodesButton = new Button(VaadinIcon.BARCODE.create(), e -> createEditBarcodesDialog().open());

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton, editBarcodesButton);
        actions.setPadding(false);
        editColumn.setEditorComponent(actions);
        this.add(grid);
    }

    private Dialog createEditBarcodesDialog() {
        Dialog editBarcodeDialog = new Dialog();
        editBarcodeDialog.setHeaderTitle("Barcodes hinzufügen");
        VerticalLayout vl = new VerticalLayout();
        vl.add(new Html("<div><b>Name:</b> " + editor.getItem().getName() + "</div>"));
        ZXingVaadinReader reader = this.createBarcodeReader();
        vl.add(reader);
        Grid<String> barcodes = new Grid<>(String.class, false);
        barcodes.addColumn(String::toString).setHeader("Barcodes");
        barcodes.setItems(editor.getItem().getBarcodes());
        reader.addValueChangeListener(e -> {
            editor.getItem().getBarcodes().add(e.getValue());
            barcodes.getDataProvider().refreshAll();
        });
        vl.add(barcodes);
        editBarcodeDialog.add(vl);
        editBarcodeDialog.getFooter().add(new Button(FactoryMethods.createUILabel("Schließen", VaadinIcon.CLOSE),
                e -> editBarcodeDialog.close()));
        return editBarcodeDialog;
    }

    private void createDialogs() {
        createInsertItemsDialog();

        createWithdrawItemsDialog();
    }

    private void createWithdrawItemsDialog() {
        this.withdrawItemsDialog.setHeaderTitle("Bestand entnehmen");

        VerticalLayout productSelection = new VerticalLayout();
        productSelection.add(new Html("<div><b>Produkt:</b></div>"));
        selectableProductsForWithdraw = new ComboBox<>();
        selectableProductsForWithdraw.setItemLabelGenerator(ViewItem::getName);
        Button searchProductByBarcode = new Button(FactoryMethods.createUILabel(VaadinIcon.SEARCH));

        HorizontalLayout selection = new HorizontalLayout(selectableProductsForWithdraw, searchProductByBarcode);
        productSelection.add(selection);
        Binder<ViewInsertItem> withdrawItemBinder = new Binder<>(ViewInsertItem.class);

        searchProductByBarcode.addClickListener(e -> {
            if (searchProductByBarcode.hasClassName("clicked")) {
                productSelection.remove(productSelection.getComponentAt(2));
                searchProductByBarcode.removeClassName("clicked");
            } else {
                ZXingVaadinReader reader = this.createBarcodeReader();
                productSelection.add(reader);
                reader.addValueChangeListener(c -> {
                    withdrawItemBinder.setBean(new ViewInsertItem(null, null, "Suche..."));
                    this.inventoryWorkflows.searchItemByProductCode(c.getValue())
                            .ifPresentOrElse(
                                    i -> withdrawItemBinder
                                            .setBean(new ViewInsertItem(i.id().toString(), 0d, i.name())),
                                    () -> withdrawItemBinder
                                            .setBean(new ViewInsertItem(null, null, "Kein Produkt gefunden")));

                });
                searchProductByBarcode.addClassName("clicked");
            }
        });

        TextField labelProdukt2 = new TextField();
        labelProdukt2.setReadOnly(true);
        withdrawItemBinder.forField(labelProdukt2).bindReadOnly("name");

        NumberField amountField2 = new NumberField("Anzahl");
        amountField2.setMin(0);
        withdrawItemBinder.forField(amountField2).asRequired().bind("amount");

        selectableProductsForWithdraw.addValueChangeListener(
                e -> {
                    ViewItem i = e.getValue();
                    withdrawItemBinder.setBean(new ViewInsertItem(i.getId().toString(), 0d, i.getName()));
                });

        VerticalLayout wLayout = new VerticalLayout(productSelection, labelProdukt2, amountField2);

        withdrawItemsDialog.add(wLayout);
        this.withdrawItemsDialog.getFooter().add(new Button(FactoryMethods.createUILabel("Abbrechen", VaadinIcon.CLOSE),
                e -> {
                    if (searchProductByBarcode.hasClassName("clicked")) {
                        productSelection.remove(productSelection.getComponentAt(2));
                        searchProductByBarcode.removeClassName("clicked");
                    }
                    this.withdrawItemsDialog.close();
                }));
        this.withdrawItemsDialog.getFooter()
                .add(new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
                    ViewInsertItem item = withdrawItemBinder.getBean();
                    if (item == null) {
                        Notification.show("Speichern nicht möglich", 10000, Position.TOP_END)
                                .addThemeVariants(NotificationVariant.LUMO_ERROR);
                        return;
                    }
                    this.inventoryWorkflows.withdrawItem(new RemoveItem(item.id, item.amount.intValue()));
                    this.withdrawItemsDialog.close();
                    this.loadData();
                    Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                            .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                }));
        this.withdrawItemsDialog.addDialogCloseActionListener(e -> {
            if (searchProductByBarcode.hasClassName("clicked")) {
                productSelection.remove(productSelection.getComponentAt(2));
                searchProductByBarcode.removeClassName("clicked");
            }
            e.getSource().close();
        });

        this.add(withdrawItemsDialog);
    }

    private void createInsertItemsDialog() {
        this.insertItemsDialog.setHeaderTitle("Bestand hinzufügen");

        VerticalLayout productSelection = new VerticalLayout();
        productSelection.add(new Html("<div><b>Produkt:</b></div>"));

        selectableProductsForInsert = new ComboBox<>();
        selectableProductsForInsert.setItemLabelGenerator(ViewItem::getName);

        Button searchProductByBarcode = new Button(FactoryMethods.createUILabel(VaadinIcon.SEARCH));

        HorizontalLayout selection = new HorizontalLayout(selectableProductsForInsert, searchProductByBarcode);
        productSelection.add(selection);
        Binder<ViewInsertItem> insertItemBinder = new Binder<>(ViewInsertItem.class);

        searchProductByBarcode.addClickListener(e -> {
            if (searchProductByBarcode.hasClassName("clicked")) {
                productSelection.remove(productSelection.getComponentAt(2));
                searchProductByBarcode.removeClassName("clicked");
            } else {
                ZXingVaadinReader reader = this.createBarcodeReader();
                productSelection.add(reader);
                reader.addValueChangeListener(c -> {
                    insertItemBinder.setBean(new ViewInsertItem(null, null, "Suche..."));
                    this.inventoryWorkflows.searchItemByProductCode(c.getValue())
                            .ifPresentOrElse(
                                    i -> insertItemBinder
                                            .setBean(new ViewInsertItem(i.id().toString(), 0d, i.name())),
                                    () -> insertItemBinder
                                            .setBean(new ViewInsertItem(null, null, "Kein Produkt gefunden")));

                });
                searchProductByBarcode.addClassName("clicked");
            }
        });

        TextField labelProdukt = new TextField();
        labelProdukt.setReadOnly(true);
        insertItemBinder.forField(labelProdukt).bindReadOnly("name");

        NumberField amountField = new NumberField("Anzahl");
        amountField.setMin(0);
        insertItemBinder.forField(amountField).asRequired().bind("amount");

        selectableProductsForInsert.addValueChangeListener(
                e -> {
                    ViewItem i = e.getValue();
                    if (i == null) {
                        return;
                    }
                    insertItemBinder.setBean(new ViewInsertItem(i.getId().toString(), 0d, i.getName()));
                });

        VerticalLayout iLayout = new VerticalLayout(productSelection, labelProdukt, amountField);
        insertItemsDialog.add(iLayout);
        this.insertItemsDialog.getFooter().add(new Button(FactoryMethods.createUILabel("Abbrechen", VaadinIcon.CLOSE),
                e -> {
                    if (searchProductByBarcode.hasClassName("clicked")) {
                        productSelection.remove(productSelection.getComponentAt(2));
                        searchProductByBarcode.removeClassName("clicked");
                    }
                    this.insertItemsDialog.close();
                }));

        this.insertItemsDialog.getFooter()
                .add(new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
                    ViewInsertItem item = insertItemBinder.getBean();
                    if (item == null) {
                        Notification.show("Speichern nicht möglich", 10000, Position.TOP_END)
                                .addThemeVariants(NotificationVariant.LUMO_ERROR);
                        return;
                    }
                    this.inventoryWorkflows.stockItem(new InsertItem(item.id, item.amount.intValue()));
                    this.insertItemsDialog.close();
                    this.loadData();
                    Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                            .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                }));
        this.insertItemsDialog.addDialogCloseActionListener(e -> {
            if (searchProductByBarcode.hasClassName("clicked")) {
                productSelection.remove(productSelection.getComponentAt(2));
                searchProductByBarcode.removeClassName("clicked");
            }
            e.getSource().close();
        });

        this.add(insertItemsDialog);
    }

    private void configureEditor() {
        this.editor.addSaveListener(i -> {
            ViewItem item = i.getItem();
            if (item.getId() != null) {
                this.inventoryWorkflows
                        .modifyItem(new DomainDtos.ModifyItem(item.getId().toString(), item.getName(),
                                item.getThreshhold().intValue(), item.getBarcodes()));
            } else {
                this.inventoryWorkflows
                        .listNewItem(
                                new DomainDtos.NewItem(item.getName(), item.getThreshhold().intValue(),
                                        List.of()));
            }
        });
        this.editor.setBuffered(true);

        TextField nameTextField = new TextField();
        binder.forField(nameTextField).asRequired().bind("name");
        this.nameColumn.setEditorComponent(nameTextField);

        NumberField threshholdNumberField = new NumberField();
        threshholdNumberField.setMin(0);
        binder.forField(threshholdNumberField).asRequired().bind("threshhold");
        this.threshholdColumn.setEditorComponent(threshholdNumberField);
    }

    private void createResponsiveEditor() {
        VerticalLayout vl = new VerticalLayout();

        Div t = new Div();
        TextField nameTextField2 = new TextField();
        respBinder.forField(nameTextField2).asRequired().bind("name");
        t.add(new Html("<b>Name:<b> "), nameTextField2);
        vl.add(t);

        NumberField threshholdNumberField2 = new NumberField();
        threshholdNumberField2.setMin(0);
        respBinder.forField(threshholdNumberField2).asRequired().bind("threshhold");
        Div th = new Div();
        th.add(new Html("<b>Mindestbestand:<b> "), threshholdNumberField2);
        vl.add(th);

        Button saveButton = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            if (!editor.save()) {
                Notification.show("Fehler beim Speichern", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_ERROR);
            } else {
                Notification.show("Speichern erfolgreich", 10000, Position.TOP_END)
                        .addThemeVariants(NotificationVariant.LUMO_SUCCESS);
                this.loadData();
            }
        });
        Button cancelButton = new Button(VaadinIcon.CLOSE.create(), e -> editor.cancel());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR);

        Button editBarcodesButton = new Button(VaadinIcon.BARCODE.create(), e -> createEditBarcodesDialog().open());

        HorizontalLayout actions = new HorizontalLayout(saveButton, cancelButton, editBarcodesButton);
        actions.setPadding(false);
        vl.add(actions);

        this.respColumn.setEditorComponent(vl);

    }

    private ZXingVaadinReader createBarcodeReader() {
        ZXingVaadinReader reader = new ZXingVaadinReader("video");
        reader.setFrom(Constants.From.camera);
        reader.setWidth("300");
        reader.setStyle("border: 1px solid gray");
        return reader;
    }

    private void loadData() {
        this.data = new ArrayList<>(this.inventoryWorkflows.showAllItems().stream().map(ViewItem::new).toList());
        this.grid.setItems(this.data);
        this.selectableProductsForInsert.setItems(this.data);
        this.selectableProductsForWithdraw.setItems(this.data);
    }

    @Data
    public class ViewItem {
        private Long id;
        private String name;
        private Integer availableAmount;
        private Double threshhold;
        private List<String> barcodes;

        public ViewItem() {
        }

        public ViewItem(ItemDTO itemDTO) {
            this.id = itemDTO.id();
            this.name = itemDTO.name();
            this.availableAmount = itemDTO.availableAmount();
            this.threshhold = itemDTO.threshhold().doubleValue();
            this.barcodes = new ArrayList<String>(itemDTO.barcodes());
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class ViewInsertItem {
        private String id;
        private Double amount;
        private String name;
    }

    @Override
    public BasicLayoutDto getGridDefinition(boolean small) {
        if (!small) {
            return new BasicLayoutDto(new boolean[] { false, true, true, true, true }, binder, this.editor, this.grid);
        } else {
            return new BasicLayoutDto(new boolean[] { true, false, false, false, false }, respBinder, this.editor,
                    this.grid);
        }
    }

}
