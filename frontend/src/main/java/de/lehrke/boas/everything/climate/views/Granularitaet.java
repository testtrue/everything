package de.lehrke.boas.everything.climate.views;

public enum Granularitaet {
    JAHR, 
    MONAT, 
    WOCHE, 
    TAG, 
    STUNDE, 
    MINUTE;
}
