package de.lehrke.boas.everything.climate.views;

import java.util.List;

public record KlimaModel(String ort, MessungDto aktuell, MessungDto durchschnitt, List<MessungDto> historie) {

}
