package de.lehrke.boas.everything.einkauf.service;

import java.time.LocalDate;
import java.util.List;

public record Einkauf(LocalDate datum, List<EinkaufItem> einkaufItems) {
}
