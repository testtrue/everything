package de.lehrke.boas.everything.gamify.service;

import java.util.UUID;

public record Player(UUID id,
        String name,
        Integer experience,
        Integer lore,
        Integer funBudget,
        QuestList questList,
        Unlocked unlocked,
        Rules rules) {
}
