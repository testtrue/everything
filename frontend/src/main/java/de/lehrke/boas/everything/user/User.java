package de.lehrke.boas.everything.user;

import com.vaadin.flow.spring.annotation.VaadinSessionScope;
import com.vaadin.flow.spring.security.AuthenticationContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.util.List;
import java.util.Optional;

@Component
@SessionScope
@VaadinSessionScope
@Slf4j
public class User {
    
    private AuthenticationContext authContext;

    public User(AuthenticationContext authContext) {
        this.authContext = authContext;
    }

    private Optional<OAuth2AuthenticatedPrincipal> getAuthenticatedUser() {
        return this.authContext.getAuthenticatedUser(OAuth2AuthenticatedPrincipal.class);
    }

    public String getUsername() {
        return this.getAuthenticatedUser().map(u -> (String) u.getAttribute("preferred_username")).get();
    }

    public String getFullName() {
        return this.getAuthenticatedUser().map(u -> (String) u.getAttribute("name")).get();
    }

    public String getGivenName() {
        return this.getAuthenticatedUser().map(u -> (String) u.getAttribute("given_name")).get();
    }

    public String getFamilyName() {
        return this.getAuthenticatedUser().map(u -> (String) u.getAttribute("family_name")).get();
    }

    public String getEmail() {
        return this.getAuthenticatedUser().map(u -> (String) u.getAttribute("email")).get();
    }

    public boolean hasRole(String role) {
        String roleToSearch = role.toUpperCase();
        Optional<OAuth2AuthenticatedPrincipal> user = this.getAuthenticatedUser();
        if (!user.isPresent()) {
            return false;
        }
        
        return ((List) user.get().getAttribute("dashboard")).stream().anyMatch(a -> a.equals(role));
    }

    public boolean isLoggedIn() {
        return this.authContext.isAuthenticated();
    }

    public void logout() {
        this.authContext.logout();
    }
}
