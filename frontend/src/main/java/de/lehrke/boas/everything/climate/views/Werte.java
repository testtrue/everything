package de.lehrke.boas.everything.climate.views;

public record Werte(
        Double temperatur,
        Double humidity) {
}
