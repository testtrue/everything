package de.lehrke.boas.everything.climate.views;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.GetExchange;

import java.util.List;
import java.util.Map;

public interface MessstationWorkflows {

    List<Messstation> ladeMessstationen();

    @GetExchange("/messstation/sensortypen")
    public Map<String,List<String>> getSupportedSensortypen();

    @PostMapping("/messstation/{ort}")
    public Boolean createMessstation(@PathVariable String ort, @RequestBody Map<String,String> sensor);

    @GetExchange("/messstation/")
    public List<Messstation> getMessstationen();
}
