package de.lehrke.boas.everything.climate.views;

import com.github.appreciated.apexcharts.ApexCharts;
import com.github.appreciated.apexcharts.ApexChartsBuilder;
import com.github.appreciated.apexcharts.config.TitleSubtitle;
import com.github.appreciated.apexcharts.config.builder.*;
import com.github.appreciated.apexcharts.config.chart.Type;
import com.github.appreciated.apexcharts.config.plotoptions.builder.BarBuilder;
import com.github.appreciated.apexcharts.config.series.SeriesType;
import com.github.appreciated.apexcharts.config.stroke.Curve;
import com.github.appreciated.apexcharts.config.xaxis.XAxisType;
import com.github.appreciated.apexcharts.helper.Series;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import jakarta.annotation.security.RolesAllowed;

import java.time.format.DateTimeFormatter;

@Route(layout = MyLayout.class)
@PageTitle("Wetter")
@RolesAllowed({ "inventory" })
@de.lehrke.boas.everything.dashboard.utils.MenuItem("Wetter")
public class KlimaView extends VerticalLayout {

    private KlimaWorkflows klimaWorkflows;
    private Grid<KlimaModel> grid = new Grid<>(KlimaModel.class);
    private MenuBar menuBar = new MenuBar();
    private Integer aktuelleGranularitaet = Granularitaet.values().length-1;

    public KlimaView(KlimaWorkflows klimaWorkflows) {
        this.klimaWorkflows = klimaWorkflows;
        this.grid.removeAllColumns();
        this.grid.setSelectionMode(SelectionMode.NONE);
        this.grid.setAllRowsVisible(true);
        this.grid.addComponentColumn(v -> {
            VerticalLayout vl = new VerticalLayout();
            vl.add(new Html("<div><h2>" + v.ort() + "</h2></div>"));
            vl.add(new Html("<div><b>" + v.aktuell().date().format(DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm"))
                    + "</b></div>"));
            vl.add(new Html("<div><b>Aktuelle Temperatur: </b> " + v.aktuell().werte().temperatur() + "</div>"));
            vl.add(new Html("<div><b> Aktuelle Luftfeuchtigkeit: </b>" + v.aktuell().werte().humidity() + "</div>"));
            vl.add(new Html(
                    "<div><b>Durchschnitts Temperatur: </b> " + v.durchschnitt().werte().temperatur() + "</div>"));
            vl.add(new Html(
                    "<div><b> Durchschnitts Luftfeuchtigkeit: </b>" + v.durchschnitt().werte().humidity() + "</div>"));

            Div c = new Div();
            TitleSubtitle titleSubtitle = new TitleSubtitle();
            titleSubtitle.setText("Verlauf");

            
            Series<Double> temperatur = new Series<>("Temperatur", SeriesType.AREA,
                    v.historie().stream().map(h -> h.werte().temperatur()).toList().toArray(new Double[0]));
            Series<Double> humidity = new Series<>("Luftfeuchtigkeit", SeriesType.AREA,
                    v.historie().stream().map(h -> h.werte().humidity()).toList().toArray(new Double[0]));
        
            final ApexCharts apexCharts = ApexChartsBuilder.get().withTheme(ThemeBuilder.get().build()).withTitle(titleSubtitle)
                    .withChart(ChartBuilder.get().withType(Type.AREA).withHeight("300px").withWidth("400px").build())
                    .withPlotOptions(
                            PlotOptionsBuilder.get().withBar(BarBuilder.get().withHorizontal(false).build()).build())
                    .withSeries(temperatur, humidity)
                    .withXaxis(XAxisBuilder.get().withType(XAxisType.DATETIME)
                            .withCategories(v.historie().stream().map(h -> h.date().toString()).toList()).build())
                    .withStroke(StrokeBuilder.get().withCurve(Curve.SMOOTH).build())
                    .build();
            c.add(apexCharts);
            vl.add(c);
            return vl;
        });
        MenuItem minus = this.menuBar.addItem(FactoryMethods.createUILabel(VaadinIcon.MINUS_CIRCLE));
        MenuItem plus = this.menuBar.addItem(FactoryMethods.createUILabel(VaadinIcon.PLUS_CIRCLE));
        plus.addClickListener(e -> {
                if(this.aktuelleGranularitaet < Granularitaet.values().length -1) {
                        this.aktuelleGranularitaet++;
                        this.loadData();
                }
        });
        minus.addClickListener(e -> {
                if(this.aktuelleGranularitaet > 0) {
                        this.aktuelleGranularitaet--;
                        this.loadData();
                }
        });

        this.loadData();
        this.add(this.menuBar, this.grid);
    }

    private void loadData() {
        this.grid.setItems(this.klimaWorkflows.getKlimaDaten(Granularitaet.values()[this.aktuelleGranularitaet]));
    }

}
