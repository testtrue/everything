package de.lehrke.boas.everything.gamify.service;

public record FunBudgetRule(Integer value,AdvancementType advancementType) {
}
