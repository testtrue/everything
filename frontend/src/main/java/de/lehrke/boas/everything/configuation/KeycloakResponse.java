package de.lehrke.boas.everything.configuation;

public record KeycloakResponse(String access_token, Long expires_in, Long refresh_expires_in, String refresh_token, String token_type, String id_token, String session_state, String scope) {
}
