package de.lehrke.boas.everything.gamify.service;

public record Rules(TreatRule treatRule,
        MiniGameRule minigameRule,
        BigTreatRule bigTreatRule,
        FunBudgetRule funBudgetRule) {
}
