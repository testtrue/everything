package de.lehrke.boas.everything.budgetmanagement.service;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BudgetCategoryService {

    @GetExchange("/budgetcategories/")
    public List<BudgetCategoryDto> getAllBudgetCategories(@RequestParam("categoryName") Optional<String> name, @RequestParam("month") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> optMonth);

    @GetExchange("/budgetcategories/{id}")
    public BudgetCategoryDto getBudgetCategoryById(@PathVariable Long id);

    @GetExchange("/budgetcategories/{id}")
    public BudgetCategoryDto updateBudgetCategory(@PathVariable Long id, @RequestBody BudgetCategoryDto budgetCategoryDto) ;

    @PostExchange("/budgetcategories/")
    public BudgetCategoryDto createBudgetCategory(@RequestBody BudgetCategoryDto budgetCategoryDto);

    @DeleteExchange("/budgetcategories/{id}")
    public void deleteBudgetCategory(@PathVariable Long id);

}
