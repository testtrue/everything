package de.lehrke.boas.everything.configuation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
@EnableWebSecurity
public class SecurityConfig {

    private final KeycloakLogoutHandler keycloakLogoutHandler;

    public SecurityConfig(KeycloakLogoutHandler keycloakLogoutHandler) {
        this.keycloakLogoutHandler = keycloakLogoutHandler;
    }

    @Bean
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(
                new SessionRegistryImpl());
    }

    @Bean
    @Profile("default")
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*").allowedHeaders("*").allowedMethods("PUT", "POST", "GET", "OPTIONS", "DELETE").allowCredentials(false);
            }
        };
    }

    //@Bean
    // public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    //     http.cors().
    //     and().
    //         csrf().disable().
    //         authorizeHttpRequests().
    //             requestMatchers("/images/*.png").permitAll().
    //             requestMatchers("/api/**").hasAnyRole("budgetmanagement").
    //             requestMatchers("/actuator/**").authenticated().
    //             anyRequest().permitAll();

    //     http.oauth2Login().
    //         loginPage("/login").permitAll().
    //     and().
    //         logout().addLogoutHandler(keycloakLogoutHandler)
    //             .logoutSuccessUrl("/");
    //     http.oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt);
    //     return http.build();
    // } 

}
