package de.lehrke.boas.everything.dashboard.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.auth.AnonymousAllowed;
import de.lehrke.boas.everything.budgetmanagement.views.BudgetManagementView;
import de.lehrke.boas.everything.climate.views.KlimaView;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.einkauf.view.EinkaufView;
import de.lehrke.boas.everything.inventory.views.InventoryView;
import de.lehrke.boas.everything.user.User;
import jakarta.annotation.security.RolesAllowed;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

@Slf4j
public class MyLayout extends AppLayout {

    private Tabs menu = new Tabs();
    private final VerticalLayout menuLayout = new VerticalLayout();
    private User user;
    private final H1 appTitle = new H1("Lehrke");
    private final MenuBar userHandling = new MenuBar();

    public MyLayout(@Autowired(required = false) User user) {
        this.user = user;
        this.userHandling.addThemeVariants(MenuBarVariant.LUMO_TERTIARY_INLINE);
        this.setPrimarySection(Section.NAVBAR);
        HorizontalLayout navbar = new HorizontalLayout();
        Button drawer = new Button(null, new Icon(VaadinIcon.MENU), e -> this.setDrawerOpened(!this.isDrawerOpened()));
        navbar.add(drawer, appTitle, this.userHandling);
        this.addToNavbar(true, navbar);
        this.addToDrawer(this.menuLayout);
        this.menuLayout.add(this.menu);

        Tabs newMenu = this.createMenu();
        this.menuLayout.replace(this.menu, newMenu);
        this.menu = newMenu;

    }

    private Tabs createMenu() {
        Tabs newMenu = new Tabs();
        newMenu.setOrientation(Orientation.VERTICAL);
        Reflections reflections = new Reflections("de.lehrke.boas.everything");
        Set<Class<?>> pages = reflections.getTypesAnnotatedWith(de.lehrke.boas.everything.dashboard.utils.MenuItem.class);
        List<Class<?>> orderedPages =
                (new ArrayList<>(pages)).stream().
                        sorted(Comparator.comparingInt(c -> c.getAnnotation(de.lehrke.boas.everything.dashboard.utils.MenuItem.class).order())
                        ).toList();

        for (Class<?> page : orderedPages) {

            if (!(Component.class.isAssignableFrom(page))) {
                continue;
            }
            Class<? extends Component> pageClass = (Class<? extends Component>) page;
            de.lehrke.boas.everything.dashboard.utils.MenuItem title = page.getAnnotation(de.lehrke.boas.everything.dashboard.utils.MenuItem.class);
            RolesAllowed roles = page.getAnnotation(RolesAllowed.class);
            if (page.getAnnotation(AnonymousAllowed.class) != null) {
                newMenu.add(createTab(title.value(), pageClass));
            } else if (roles != null) {
                Arrays.asList(roles.value()).forEach(r -> {
                    if (this.user.hasRole(r)) {
                        newMenu.add(createTab(title.value(), pageClass));
                    }
                });
            }
        }
        return newMenu;
    }

    private static Tab createTab(String text,
            Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();

        Tabs newMenu = this.createMenu();
        this.menuLayout.replace(this.menu, newMenu);
        this.menu = newMenu;

        // Select the tab corresponding to currently shown view
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
        this.userHandling.removeAll();
        if (!user.isLoggedIn()) {
            Button login = new Button("login",
                    e -> UI.getCurrent().getPage().setLocation("/oauth2/authorization/keycloak"));
            this.userHandling.addItem(login);
        } else {
            MenuItem menuItem = this.userHandling.addItem(new Avatar(user.getFullName()));
            SubMenu subMenu = menuItem.getSubMenu();
            subMenu.addItem(user.getUsername());
            subMenu.addItem("logout", e -> this.user.logout());
        }
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

}
