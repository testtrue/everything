package de.lehrke.boas.everything.configuation;

import com.vaadin.flow.spring.security.VaadinWebSecurity;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.*;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.client.endpoint.OAuth2RefreshTokenGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.client.RestClient;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebSecurity
public class VaadingSecurityConfig extends VaadinWebSecurity {

    private final KeycloakLogoutHandler keycloakLogoutHandler;
    
    public VaadingSecurityConfig(KeycloakLogoutHandler keycloakLogoutHandler) {
        this.keycloakLogoutHandler = keycloakLogoutHandler;
    }

    @Bean
    public GrantedAuthoritiesMapper userAuthoritiesMapperForKeycloak() {
        return authorities -> {
            Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
            var authority = authorities.iterator().next();
            if (authority instanceof OidcUserAuthority oidcUserAuthority) {
                var userInfo = oidcUserAuthority.getUserInfo();
                if (userInfo.hasClaim("dashboard")) {
                    var realmAccess = userInfo.getClaimAsStringList("dashboard");
                    mappedAuthorities.addAll(realmAccess.stream()
                            .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                            .toList());
                }
            }
            return mappedAuthorities;
        };
    }

    @Bean
    public OAuth2AuthorizedClientManager authorizedClientManager(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizedClientRepository authorizedClientRepository) {

        OAuth2AuthorizedClientProvider authorizedClientProvider =
                OAuth2AuthorizedClientProviderBuilder.builder()
                        .authorizationCode()
                        .refreshToken()
                        .clientCredentials()
                        .build();

        DefaultOAuth2AuthorizedClientManager authorizedClientManager =
                new DefaultOAuth2AuthorizedClientManager(
                        clientRegistrationRepository, authorizedClientRepository);
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

        return authorizedClientManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(
            r -> r.requestMatchers(new AntPathRequestMatcher("/images/*.png")).permitAll()
        ).oauth2Login(
                Customizer.withDefaults()
        ).logout(
                l -> l.addLogoutHandler(keycloakLogoutHandler)
                        .invalidateHttpSession(true)
                        .clearAuthentication(true)
                        .logoutSuccessUrl("/oauth2/authorization/keycloak")
        );

        super.configure(http);
    }

    @Bean
    public RestClient.Builder customRestClientBuilder(OAuth2AuthorizedClientService clientService) {

        return RestClient.builder()
            .requestInterceptor((request, body, execution) -> {
                request.getHeaders().add("Authorization", this.getHeader(SecurityContextHolder.getContext().getAuthentication(), clientService));
                return execution.execute(request, body);
            })
            ;

    }

    private String getHeader (Authentication a, OAuth2AuthorizedClientService b) {
        if (a instanceof OidcUser) {
            return "Bearer " + ((OidcUser)a).getIdToken().toString();
        } else if (a instanceof OAuth2AuthenticationToken ) {
            String clientRegistrationId = ((OAuth2AuthenticationToken)a).getAuthorizedClientRegistrationId();
            OAuth2AuthorizedClient client = b.loadAuthorizedClient(clientRegistrationId, a.getName());

            if (Instant.now().isAfter(client.getAccessToken().getExpiresAt())) {

                OAuth2AccessTokenResponse token = this.keycloakLogoutHandler.
                                                            getTokenResponse(new OAuth2RefreshTokenGrantRequest(client.getClientRegistration(), client.getAccessToken(), client.getRefreshToken()));

                OAuth2AuthorizedClient newClient = new OAuth2AuthorizedClient(client.getClientRegistration(), a.getName(),token.getAccessToken(), token.getRefreshToken());
                b.saveAuthorizedClient(newClient, a);
                client = b.loadAuthorizedClient(clientRegistrationId, a.getName());
            }
            return "Bearer " + client.getAccessToken().getTokenValue();
        }
        return "";
    }

}
