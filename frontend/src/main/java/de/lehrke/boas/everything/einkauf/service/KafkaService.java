package de.lehrke.boas.everything.einkauf.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class KafkaService {

    KafkaTemplate<String, String> kafkaQueue;
    private final ObjectMapper objectMapper;


    public KafkaService(KafkaTemplate<String, String> kafkaQueue, ObjectMapper objectMapper) {
        this.kafkaQueue = kafkaQueue;
        this.objectMapper = objectMapper;
    }


    public void pushData(Einkauf data) throws JsonProcessingException {


        this.kafkaQueue.send("einkauf", this.objectMapper.writeValueAsString(data));
    }

}
