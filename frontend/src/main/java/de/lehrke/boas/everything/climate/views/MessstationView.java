package de.lehrke.boas.everything.climate.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.lehrke.boas.everything.climate.views.MessstationView.MessstationViewModel;
import de.lehrke.boas.everything.dashboard.utils.BasicLayoutForView;
import de.lehrke.boas.everything.dashboard.utils.FactoryMethods;
import de.lehrke.boas.everything.dashboard.utils.MenuItem;
import de.lehrke.boas.everything.dashboard.views.MyLayout;
import jakarta.annotation.security.RolesAllowed;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Route(layout = MyLayout.class)
@PageTitle("Wetter")
@RolesAllowed({ "inventory" })
@MenuItem("Messstationen")
public class MessstationView extends BasicLayoutForView<MessstationViewModel> {

    private MessstationWorkflows messstationWorkflows;

    private MenuBar menuBar = new MenuBar();
    private Grid<MessstationViewModel> grid = new Grid<>(MessstationViewModel.class);
    private Editor<MessstationViewModel> editor = grid.getEditor();
    private Binder<MessstationViewModel> binder = editor.getBinder();
    private Dialog neueMessstationDialog = new Dialog();

    private Map<String,List<String>> supportedTypen = new HashMap<>(); 

    public MessstationView(
            MessstationWorkflows messstationWorkflows) {
        this.messstationWorkflows = messstationWorkflows;
        this.grid.removeAllColumns();
        this.initMenuBar();
        this.initDialog();
        this.initGrid();
        loadData();
    }

    private void loadData() {
        this.grid.setItems(new ArrayList<>(
                this.messstationWorkflows.ladeMessstationen().stream().map(MessstationViewModel::new).toList()));
    }

    private void initMenuBar() {
        this.menuBar.setSizeFull();

        this.menuBar.addItem(FactoryMethods.createUILabel("Neue Messstation", VaadinIcon.PLUS), e -> {
            this.binder.setBean(new MessstationViewModel());
            this.neueMessstationDialog.open();
        });

        this.menuBar.addItem(FactoryMethods.createUILabel("Zurück", VaadinIcon.ARROW_BACKWARD), e -> {
            UI.getCurrent().navigate(KlimaView.class);
        });
        this.add(menuBar);
    }

    private void initDialog() {
        this.neueMessstationDialog.setHeaderTitle("Neue Messstation");

        TextField ortInput = new TextField("Ort");
        this.binder.forField(ortInput).asRequired().bind("ort");

        TextField urlInput = new TextField("Sensor");
        this.binder.forField(urlInput).asRequired().bind(b -> b.getMetadata().get("url"),
                (b, v) -> b.metadata.put("url", v));

        Button speichern = new Button(FactoryMethods.createUILabel("Speichern", VaadinIcon.FILE), e -> {
            MessstationViewModel bean = this.binder.getBean();

            Map<String, String> parametes = new HashMap<>(bean.getMetadata());
            parametes.put("type", bean.getSensorTyp());

            this.messstationWorkflows.createMessstation(bean.getOrt(), parametes);
            this.loadData();
            this.neueMessstationDialog.close();
        });

        Button abbrechen = new Button(FactoryMethods.createUILabel("Abbrechen", VaadinIcon.CLOSE), e -> {
            this.neueMessstationDialog.close();
        });
        HorizontalLayout hl = new HorizontalLayout(abbrechen, speichern);
        VerticalLayout vl = new VerticalLayout(ortInput, urlInput, hl);
        this.neueMessstationDialog.add(vl);
    }

    private void initGrid() {
        this.grid.setAllRowsVisible(true);
        this.grid.addColumn(MessstationViewModel::getOrt).setHeader("Ort");
        this.grid.addColumn(v -> v.metadata.get("url")).setHeader("Sensor");
        this.grid.addComponentColumn(v -> new Button(FactoryMethods.createUILabel("Bearbeiten", VaadinIcon.EDIT), e -> {
            this.binder.setBean(v);
            this.neueMessstationDialog.open();
        }));
        this.add(this.grid);
    }

    @Override
    public BasicLayoutForView<MessstationViewModel>.BasicLayoutDto getGridDefinition(boolean small) {
        return new BasicLayoutDto(new boolean[] { true, true }, this.binder, this.editor, grid);
    }

    @Data
    public class MessstationViewModel {
        String ort;
        String sensorTyp;
        Map<String, String> metadata;

        public MessstationViewModel() {
            this.metadata = new HashMap<>();
        }

        public MessstationViewModel(Messstation messstationImpl) {
            this.ort = messstationImpl.ort();
            this.sensorTyp = messstationImpl.sensorTyp();
            this.metadata = new HashMap<>();
            if (supportedTypen.containsKey(this.sensorTyp)) {
                List<String> fields = supportedTypen.get(this.sensorTyp);
                fields.forEach(s -> {
                    try {
                        this.metadata.put(s, Messstation.class.getField(s).get(Messstation.class.getField("sensor").get(messstationImpl)).toString());
                    } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
                            | SecurityException e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }
}
