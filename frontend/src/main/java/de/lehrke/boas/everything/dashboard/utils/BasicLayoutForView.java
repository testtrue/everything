package de.lehrke.boas.everything.dashboard.utils;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.editor.Editor;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;
import lombok.Data;

public abstract class BasicLayoutForView<T> extends VerticalLayout {

    private Registration listener;
    private int breakpointPx = 1000;

    public void adjustVisibleGridColumns(boolean small) {

        BasicLayoutForView<T>.BasicLayoutDto gridDefinition = this.getGridDefinition(small);
        // Change which columns are visible depending on browser width
        if (gridDefinition == null) {
            return;
        }
        if (gridDefinition.binder != null && gridDefinition.editor != null) {
            gridDefinition.editor.setBinder(gridDefinition.binder);
        }
        boolean[] visibleCols = gridDefinition.gridVisibility;
        if (visibleCols != null) {
            for (int c = 0; c < visibleCols.length; c++) {
                gridDefinition.grid.getColumns().get(c).setVisible(visibleCols[c]);
            }
        }
    }

        @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);

                // Add browser window listener to observe width change
        getUI().ifPresent(ui -> listener = ui.getPage().addBrowserWindowResizeListener(event -> {
            this.adjustVisibleGridColumns(event.getWidth() < breakpointPx);
        }));

        // Adjust Grid according to initial width of the screen
        getUI().ifPresent(ui -> ui.getPage().retrieveExtendedClientDetails(receiver -> {
            int browserWidth = receiver.getBodyClientWidth();

            this.adjustVisibleGridColumns(browserWidth < breakpointPx);
        }));
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Listener needs to be eventually removed in order to avoid resource leak
        listener.remove();
        super.onDetach(detachEvent);
    }

    public abstract BasicLayoutDto getGridDefinition(boolean small);
    
    @Data
    public class BasicLayoutDto {
        private final boolean[] gridVisibility;
        private final Binder<T> binder;
        private final Editor<T> editor;
        private final Grid<T> grid;
    }
}
