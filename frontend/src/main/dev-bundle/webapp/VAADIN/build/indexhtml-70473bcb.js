(function(){const e=document.createElement("link").relList;if(e&&e.supports&&e.supports("modulepreload"))return;for(const o of document.querySelectorAll('link[rel="modulepreload"]'))i(o);new MutationObserver(o=>{for(const r of o)if(r.type==="childList")for(const s of r.addedNodes)s.tagName==="LINK"&&s.rel==="modulepreload"&&i(s)}).observe(document,{childList:!0,subtree:!0});function t(o){const r={};return o.integrity&&(r.integrity=o.integrity),o.referrerPolicy&&(r.referrerPolicy=o.referrerPolicy),o.crossOrigin==="use-credentials"?r.credentials="include":o.crossOrigin==="anonymous"?r.credentials="omit":r.credentials="same-origin",r}function i(o){if(o.ep)return;o.ep=!0;const r=t(o);fetch(o.href,r)}})();window.Vaadin=window.Vaadin||{};window.Vaadin.featureFlags=window.Vaadin.featureFlags||{};window.Vaadin.featureFlags.exampleFeatureFlag=!1;window.Vaadin.featureFlags.collaborationEngineBackend=!1;const $i="modulepreload",Ti=function(n,e){return new URL(n,e).href},vt={},be=function(e,t,i){if(!t||t.length===0)return e();const o=document.getElementsByTagName("link");return Promise.all(t.map(r=>{if(r=Ti(r,i),r in vt)return;vt[r]=!0;const s=r.endsWith(".css"),l=s?'[rel="stylesheet"]':"";if(!!i)for(let c=o.length-1;c>=0;c--){const u=o[c];if(u.href===r&&(!s||u.rel==="stylesheet"))return}else if(document.querySelector(`link[href="${r}"]${l}`))return;const d=document.createElement("link");if(d.rel=s?"stylesheet":$i,s||(d.as="script",d.crossOrigin=""),d.href=r,document.head.appendChild(d),s)return new Promise((c,u)=>{d.addEventListener("load",c),d.addEventListener("error",()=>u(new Error(`Unable to preload CSS for ${r}`)))})})).then(()=>e()).catch(r=>{const s=new Event("vite:preloadError",{cancelable:!0});if(s.payload=r,window.dispatchEvent(s),!s.defaultPrevented)throw r})};function we(n){return n=n||[],Array.isArray(n)?n:[n]}function L(n){return`[Vaadin.Router] ${n}`}function xi(n){if(typeof n!="object")return String(n);const e=Object.prototype.toString.call(n).match(/ (.*)\]$/)[1];return e==="Object"||e==="Array"?`${e} ${JSON.stringify(n)}`:e}const Se="module",Ee="nomodule",tt=[Se,Ee];function _t(n){if(!n.match(/.+\.[m]?js$/))throw new Error(L(`Unsupported type for bundle "${n}": .js or .mjs expected.`))}function Kt(n){if(!n||!k(n.path))throw new Error(L('Expected route config to be an object with a "path" string property, or an array of such objects'));const e=n.bundle,t=["component","redirect","bundle"];if(!B(n.action)&&!Array.isArray(n.children)&&!B(n.children)&&!Ce(e)&&!t.some(i=>k(n[i])))throw new Error(L(`Expected route config "${n.path}" to include either "${t.join('", "')}" or "action" function but none found.`));if(e)if(k(e))_t(e);else if(tt.some(i=>i in e))tt.forEach(i=>i in e&&_t(e[i]));else throw new Error(L('Expected route bundle to include either "'+Ee+'" or "'+Se+'" keys, or both'));n.redirect&&["bundle","component"].forEach(i=>{i in n&&console.warn(L(`Route config "${n.path}" has both "redirect" and "${i}" properties, and "redirect" will always override the latter. Did you mean to only use "${i}"?`))})}function yt(n){we(n).forEach(e=>Kt(e))}function bt(n,e){let t=document.head.querySelector('script[src="'+n+'"][async]');return t||(t=document.createElement("script"),t.setAttribute("src",n),e===Se?t.setAttribute("type",Se):e===Ee&&t.setAttribute(Ee,""),t.async=!0),new Promise((i,o)=>{t.onreadystatechange=t.onload=r=>{t.__dynamicImportLoaded=!0,i(r)},t.onerror=r=>{t.parentNode&&t.parentNode.removeChild(t),o(r)},t.parentNode===null?document.head.appendChild(t):t.__dynamicImportLoaded&&i()})}function Ri(n){return k(n)?bt(n):Promise.race(tt.filter(e=>e in n).map(e=>bt(n[e],e)))}function ie(n,e){return!window.dispatchEvent(new CustomEvent(`vaadin-router-${n}`,{cancelable:n==="go",detail:e}))}function Ce(n){return typeof n=="object"&&!!n}function B(n){return typeof n=="function"}function k(n){return typeof n=="string"}function Yt(n){const e=new Error(L(`Page not found (${n.pathname})`));return e.context=n,e.code=404,e}const K=new class{};function Ni(n){const e=n.port,t=n.protocol,r=t==="http:"&&e==="80"||t==="https:"&&e==="443"?n.hostname:n.host;return`${t}//${r}`}function wt(n){if(n.defaultPrevented||n.button!==0||n.shiftKey||n.ctrlKey||n.altKey||n.metaKey)return;let e=n.target;const t=n.composedPath?n.composedPath():n.path||[];for(let l=0;l<t.length;l++){const a=t[l];if(a.nodeName&&a.nodeName.toLowerCase()==="a"){e=a;break}}for(;e&&e.nodeName.toLowerCase()!=="a";)e=e.parentNode;if(!e||e.nodeName.toLowerCase()!=="a"||e.target&&e.target.toLowerCase()!=="_self"||e.hasAttribute("download")||e.hasAttribute("router-ignore")||e.pathname===window.location.pathname&&e.hash!==""||(e.origin||Ni(e))!==window.location.origin)return;const{pathname:o,search:r,hash:s}=e;ie("go",{pathname:o,search:r,hash:s})&&(n.preventDefault(),n&&n.type==="click"&&window.scrollTo(0,0))}const ki={activate(){window.document.addEventListener("click",wt)},inactivate(){window.document.removeEventListener("click",wt)}},Li=/Trident/.test(navigator.userAgent);Li&&!B(window.PopStateEvent)&&(window.PopStateEvent=function(n,e){e=e||{};var t=document.createEvent("Event");return t.initEvent(n,!!e.bubbles,!!e.cancelable),t.state=e.state||null,t},window.PopStateEvent.prototype=window.Event.prototype);function St(n){if(n.state==="vaadin-router-ignore")return;const{pathname:e,search:t,hash:i}=window.location;ie("go",{pathname:e,search:t,hash:i})}const Ai={activate(){window.addEventListener("popstate",St)},inactivate(){window.removeEventListener("popstate",St)}};var Q=ti,Ii=at,Pi=Mi,Oi=Qt,Di=ei,Jt="/",Xt="./",Vi=new RegExp(["(\\\\.)","(?:\\:(\\w+)(?:\\(((?:\\\\.|[^\\\\()])+)\\))?|\\(((?:\\\\.|[^\\\\()])+)\\))([+*?])?"].join("|"),"g");function at(n,e){for(var t=[],i=0,o=0,r="",s=e&&e.delimiter||Jt,l=e&&e.delimiters||Xt,a=!1,d;(d=Vi.exec(n))!==null;){var c=d[0],u=d[1],h=d.index;if(r+=n.slice(o,h),o=h+c.length,u){r+=u[1],a=!0;continue}var p="",G=n[o],ke=d[2],q=d[3],wi=d[4],he=d[5];if(!a&&r.length){var Le=r.length-1;l.indexOf(r[Le])>-1&&(p=r[Le],r=r.slice(0,Le))}r&&(t.push(r),r="",a=!1);var Si=p!==""&&G!==void 0&&G!==p,Ei=he==="+"||he==="*",Ci=he==="?"||he==="*",mt=p||s,gt=q||wi;t.push({name:ke||i++,prefix:p,delimiter:mt,optional:Ci,repeat:Ei,partial:Si,pattern:gt?Ui(gt):"[^"+D(mt)+"]+?"})}return(r||o<n.length)&&t.push(r+n.substr(o)),t}function Mi(n,e){return Qt(at(n,e))}function Qt(n){for(var e=new Array(n.length),t=0;t<n.length;t++)typeof n[t]=="object"&&(e[t]=new RegExp("^(?:"+n[t].pattern+")$"));return function(i,o){for(var r="",s=o&&o.encode||encodeURIComponent,l=0;l<n.length;l++){var a=n[l];if(typeof a=="string"){r+=a;continue}var d=i?i[a.name]:void 0,c;if(Array.isArray(d)){if(!a.repeat)throw new TypeError('Expected "'+a.name+'" to not repeat, but got array');if(d.length===0){if(a.optional)continue;throw new TypeError('Expected "'+a.name+'" to not be empty')}for(var u=0;u<d.length;u++){if(c=s(d[u],a),!e[l].test(c))throw new TypeError('Expected all "'+a.name+'" to match "'+a.pattern+'"');r+=(u===0?a.prefix:a.delimiter)+c}continue}if(typeof d=="string"||typeof d=="number"||typeof d=="boolean"){if(c=s(String(d),a),!e[l].test(c))throw new TypeError('Expected "'+a.name+'" to match "'+a.pattern+'", but got "'+c+'"');r+=a.prefix+c;continue}if(a.optional){a.partial&&(r+=a.prefix);continue}throw new TypeError('Expected "'+a.name+'" to be '+(a.repeat?"an array":"a string"))}return r}}function D(n){return n.replace(/([.+*?=^!:${}()[\]|/\\])/g,"\\$1")}function Ui(n){return n.replace(/([=!:$/()])/g,"\\$1")}function Zt(n){return n&&n.sensitive?"":"i"}function Fi(n,e){if(!e)return n;var t=n.source.match(/\((?!\?)/g);if(t)for(var i=0;i<t.length;i++)e.push({name:i,prefix:null,delimiter:null,optional:!1,repeat:!1,partial:!1,pattern:null});return n}function Bi(n,e,t){for(var i=[],o=0;o<n.length;o++)i.push(ti(n[o],e,t).source);return new RegExp("(?:"+i.join("|")+")",Zt(t))}function zi(n,e,t){return ei(at(n,t),e,t)}function ei(n,e,t){t=t||{};for(var i=t.strict,o=t.start!==!1,r=t.end!==!1,s=D(t.delimiter||Jt),l=t.delimiters||Xt,a=[].concat(t.endsWith||[]).map(D).concat("$").join("|"),d=o?"^":"",c=n.length===0,u=0;u<n.length;u++){var h=n[u];if(typeof h=="string")d+=D(h),c=u===n.length-1&&l.indexOf(h[h.length-1])>-1;else{var p=h.repeat?"(?:"+h.pattern+")(?:"+D(h.delimiter)+"(?:"+h.pattern+"))*":h.pattern;e&&e.push(h),h.optional?h.partial?d+=D(h.prefix)+"("+p+")?":d+="(?:"+D(h.prefix)+"("+p+"))?":d+=D(h.prefix)+"("+p+")"}}return r?(i||(d+="(?:"+s+")?"),d+=a==="$"?"$":"(?="+a+")"):(i||(d+="(?:"+s+"(?="+a+"))?"),c||(d+="(?="+s+"|"+a+")")),new RegExp(d,Zt(t))}function ti(n,e,t){return n instanceof RegExp?Fi(n,e):Array.isArray(n)?Bi(n,e,t):zi(n,e,t)}Q.parse=Ii;Q.compile=Pi;Q.tokensToFunction=Oi;Q.tokensToRegExp=Di;const{hasOwnProperty:Hi}=Object.prototype,it=new Map;it.set("|false",{keys:[],pattern:/(?:)/});function Et(n){try{return decodeURIComponent(n)}catch{return n}}function Wi(n,e,t,i,o){t=!!t;const r=`${n}|${t}`;let s=it.get(r);if(!s){const d=[];s={keys:d,pattern:Q(n,d,{end:t,strict:n===""})},it.set(r,s)}const l=s.pattern.exec(e);if(!l)return null;const a=Object.assign({},o);for(let d=1;d<l.length;d++){const c=s.keys[d-1],u=c.name,h=l[d];(h!==void 0||!Hi.call(a,u))&&(c.repeat?a[u]=h?h.split(c.delimiter).map(Et):[]:a[u]=h&&Et(h))}return{path:l[0],keys:(i||[]).concat(s.keys),params:a}}function ii(n,e,t,i,o){let r,s,l=0,a=n.path||"";return a.charAt(0)==="/"&&(t&&(a=a.substr(1)),t=!0),{next(d){if(n===d)return{done:!0};const c=n.__children=n.__children||n.children;if(!r&&(r=Wi(a,e,!c,i,o),r))return{done:!1,value:{route:n,keys:r.keys,params:r.params,path:r.path}};if(r&&c)for(;l<c.length;){if(!s){const h=c[l];h.parent=n;let p=r.path.length;p>0&&e.charAt(p)==="/"&&(p+=1),s=ii(h,e.substr(p),t,r.keys,r.params)}const u=s.next(d);if(!u.done)return{done:!1,value:u.value};s=null,l++}return{done:!0}}}}function ji(n){if(B(n.route.action))return n.route.action(n)}function Gi(n,e){let t=e;for(;t;)if(t=t.parent,t===n)return!0;return!1}function qi(n){let e=`Path '${n.pathname}' is not properly resolved due to an error.`;const t=(n.route||{}).path;return t&&(e+=` Resolution had failed on route: '${t}'`),e}function Ki(n,e){const{route:t,path:i}=e;if(t&&!t.__synthetic){const o={path:i,route:t};if(!n.chain)n.chain=[];else if(t.parent){let r=n.chain.length;for(;r--&&n.chain[r].route&&n.chain[r].route!==t.parent;)n.chain.pop()}n.chain.push(o)}}class oe{constructor(e,t={}){if(Object(e)!==e)throw new TypeError("Invalid routes");this.baseUrl=t.baseUrl||"",this.errorHandler=t.errorHandler,this.resolveRoute=t.resolveRoute||ji,this.context=Object.assign({resolver:this},t.context),this.root=Array.isArray(e)?{path:"",__children:e,parent:null,__synthetic:!0}:e,this.root.parent=null}getRoutes(){return[...this.root.__children]}setRoutes(e){yt(e);const t=[...we(e)];this.root.__children=t}addRoutes(e){return yt(e),this.root.__children.push(...we(e)),this.getRoutes()}removeRoutes(){this.setRoutes([])}resolve(e){const t=Object.assign({},this.context,k(e)?{pathname:e}:e),i=ii(this.root,this.__normalizePathname(t.pathname),this.baseUrl),o=this.resolveRoute;let r=null,s=null,l=t;function a(d,c=r.value.route,u){const h=u===null&&r.value.route;return r=s||i.next(h),s=null,!d&&(r.done||!Gi(c,r.value.route))?(s=r,Promise.resolve(K)):r.done?Promise.reject(Yt(t)):(l=Object.assign(l?{chain:l.chain?l.chain.slice(0):[]}:{},t,r.value),Ki(l,r.value),Promise.resolve(o(l)).then(p=>p!=null&&p!==K?(l.result=p.result||p,l):a(d,c,p)))}return t.next=a,Promise.resolve().then(()=>a(!0,this.root)).catch(d=>{const c=qi(l);if(d?console.warn(c):d=new Error(c),d.context=d.context||l,d instanceof DOMException||(d.code=d.code||500),this.errorHandler)return l.result=this.errorHandler(d),l;throw d})}static __createUrl(e,t){return new URL(e,t)}get __effectiveBaseUrl(){return this.baseUrl?this.constructor.__createUrl(this.baseUrl,document.baseURI||document.URL).href.replace(/[^\/]*$/,""):""}__normalizePathname(e){if(!this.baseUrl)return e;const t=this.__effectiveBaseUrl,i=this.constructor.__createUrl(e,t).href;if(i.slice(0,t.length)===t)return i.slice(t.length)}}oe.pathToRegexp=Q;const{pathToRegexp:Ct}=oe,$t=new Map;function ni(n,e,t){const i=e.name||e.component;if(i&&(n.has(i)?n.get(i).push(e):n.set(i,[e])),Array.isArray(t))for(let o=0;o<t.length;o++){const r=t[o];r.parent=e,ni(n,r,r.__children||r.children)}}function Tt(n,e){const t=n.get(e);if(t&&t.length>1)throw new Error(`Duplicate route with name "${e}". Try seting unique 'name' route properties.`);return t&&t[0]}function xt(n){let e=n.path;return e=Array.isArray(e)?e[0]:e,e!==void 0?e:""}function Yi(n,e={}){if(!(n instanceof oe))throw new TypeError("An instance of Resolver is expected");const t=new Map;return(i,o)=>{let r=Tt(t,i);if(!r&&(t.clear(),ni(t,n.root,n.root.__children),r=Tt(t,i),!r))throw new Error(`Route "${i}" not found`);let s=$t.get(r.fullPath);if(!s){let a=xt(r),d=r.parent;for(;d;){const p=xt(d);p&&(a=p.replace(/\/$/,"")+"/"+a.replace(/^\//,"")),d=d.parent}const c=Ct.parse(a),u=Ct.tokensToFunction(c),h=Object.create(null);for(let p=0;p<c.length;p++)k(c[p])||(h[c[p].name]=!0);s={toPath:u,keys:h},$t.set(a,s),r.fullPath=a}let l=s.toPath(o,e)||"/";if(e.stringifyQueryParams&&o){const a={},d=Object.keys(o);for(let u=0;u<d.length;u++){const h=d[u];s.keys[h]||(a[h]=o[h])}const c=e.stringifyQueryParams(a);c&&(l+=c.charAt(0)==="?"?c:`?${c}`)}return l}}let Rt=[];function Ji(n){Rt.forEach(e=>e.inactivate()),n.forEach(e=>e.activate()),Rt=n}const Xi=n=>{const e=getComputedStyle(n).getPropertyValue("animation-name");return e&&e!=="none"},Qi=(n,e)=>{const t=()=>{n.removeEventListener("animationend",t),e()};n.addEventListener("animationend",t)};function Nt(n,e){return n.classList.add(e),new Promise(t=>{if(Xi(n)){const i=n.getBoundingClientRect(),o=`height: ${i.bottom-i.top}px; width: ${i.right-i.left}px`;n.setAttribute("style",`position: absolute; ${o}`),Qi(n,()=>{n.classList.remove(e),n.removeAttribute("style"),t()})}else n.classList.remove(e),t()})}const Zi=256;function Ae(n){return n!=null}function en(n){const e=Object.assign({},n);return delete e.next,e}function R({pathname:n="",search:e="",hash:t="",chain:i=[],params:o={},redirectFrom:r,resolver:s},l){const a=i.map(d=>d.route);return{baseUrl:s&&s.baseUrl||"",pathname:n,search:e,hash:t,routes:a,route:l||a.length&&a[a.length-1]||null,params:o,redirectFrom:r,getUrl:(d={})=>me(V.pathToRegexp.compile(oi(a))(Object.assign({},o,d)),s)}}function kt(n,e){const t=Object.assign({},n.params);return{redirect:{pathname:e,from:n.pathname,params:t}}}function tn(n,e){e.location=R(n);const t=n.chain.map(i=>i.route).indexOf(n.route);return n.chain[t].element=e,e}function fe(n,e,t){if(B(n))return n.apply(t,e)}function Lt(n,e,t){return i=>{if(i&&(i.cancel||i.redirect))return i;if(t)return fe(t[n],e,t)}}function nn(n,e){if(!Array.isArray(n)&&!Ce(n))throw new Error(L(`Incorrect "children" value for the route ${e.path}: expected array or object, but got ${n}`));e.__children=[];const t=we(n);for(let i=0;i<t.length;i++)Kt(t[i]),e.__children.push(t[i])}function ue(n){if(n&&n.length){const e=n[0].parentNode;for(let t=0;t<n.length;t++)e.removeChild(n[t])}}function me(n,e){const t=e.__effectiveBaseUrl;return t?e.constructor.__createUrl(n.replace(/^\//,""),t).pathname:n}function oi(n){return n.map(e=>e.path).reduce((e,t)=>t.length?e.replace(/\/$/,"")+"/"+t.replace(/^\//,""):e,"")}class V extends oe{constructor(e,t){const i=document.head.querySelector("base"),o=i&&i.getAttribute("href");super([],Object.assign({baseUrl:o&&oe.__createUrl(o,document.URL).pathname.replace(/[^\/]*$/,"")},t)),this.resolveRoute=s=>this.__resolveRoute(s);const r=V.NavigationTrigger;V.setTriggers.apply(V,Object.keys(r).map(s=>r[s])),this.baseUrl,this.ready,this.ready=Promise.resolve(e),this.location,this.location=R({resolver:this}),this.__lastStartedRenderId=0,this.__navigationEventHandler=this.__onNavigationEvent.bind(this),this.setOutlet(e),this.subscribe(),this.__createdByRouter=new WeakMap,this.__addedByRouter=new WeakMap}__resolveRoute(e){const t=e.route;let i=Promise.resolve();B(t.children)&&(i=i.then(()=>t.children(en(e))).then(r=>{!Ae(r)&&!B(t.children)&&(r=t.children),nn(r,t)}));const o={redirect:r=>kt(e,r),component:r=>{const s=document.createElement(r);return this.__createdByRouter.set(s,!0),s}};return i.then(()=>{if(this.__isLatestRender(e))return fe(t.action,[e,o],t)}).then(r=>{if(Ae(r)&&(r instanceof HTMLElement||r.redirect||r===K))return r;if(k(t.redirect))return o.redirect(t.redirect);if(t.bundle)return Ri(t.bundle).then(()=>{},()=>{throw new Error(L(`Bundle not found: ${t.bundle}. Check if the file name is correct`))})}).then(r=>{if(Ae(r))return r;if(k(t.component))return o.component(t.component)})}setOutlet(e){e&&this.__ensureOutlet(e),this.__outlet=e}getOutlet(){return this.__outlet}setRoutes(e,t=!1){return this.__previousContext=void 0,this.__urlForName=void 0,super.setRoutes(e),t||this.__onNavigationEvent(),this.ready}render(e,t){const i=++this.__lastStartedRenderId,o=Object.assign({search:"",hash:""},k(e)?{pathname:e}:e,{__renderId:i});return this.ready=this.resolve(o).then(r=>this.__fullyResolveChain(r)).then(r=>{if(this.__isLatestRender(r)){const s=this.__previousContext;if(r===s)return this.__updateBrowserHistory(s,!0),this.location;if(this.location=R(r),t&&this.__updateBrowserHistory(r,i===1),ie("location-changed",{router:this,location:this.location}),r.__skipAttach)return this.__copyUnchangedElements(r,s),this.__previousContext=r,this.location;this.__addAppearingContent(r,s);const l=this.__animateIfNeeded(r);return this.__runOnAfterEnterCallbacks(r),this.__runOnAfterLeaveCallbacks(r,s),l.then(()=>{if(this.__isLatestRender(r))return this.__removeDisappearingContent(),this.__previousContext=r,this.location})}}).catch(r=>{if(i===this.__lastStartedRenderId)throw t&&this.__updateBrowserHistory(o),ue(this.__outlet&&this.__outlet.children),this.location=R(Object.assign(o,{resolver:this})),ie("error",Object.assign({router:this,error:r},o)),r}),this.ready}__fullyResolveChain(e,t=e){return this.__findComponentContextAfterAllRedirects(t).then(i=>{const r=i!==t?i:e,l=me(oi(i.chain),i.resolver)===i.pathname,a=(d,c=d.route,u)=>d.next(void 0,c,u).then(h=>h===null||h===K?l?d:c.parent!==null?a(d,c.parent,h):h:h);return a(i).then(d=>{if(d===null||d===K)throw Yt(r);return d&&d!==K&&d!==i?this.__fullyResolveChain(r,d):this.__amendWithOnBeforeCallbacks(i)})})}__findComponentContextAfterAllRedirects(e){const t=e.result;return t instanceof HTMLElement?(tn(e,t),Promise.resolve(e)):t.redirect?this.__redirect(t.redirect,e.__redirectCount,e.__renderId).then(i=>this.__findComponentContextAfterAllRedirects(i)):t instanceof Error?Promise.reject(t):Promise.reject(new Error(L(`Invalid route resolution result for path "${e.pathname}". Expected redirect object or HTML element, but got: "${xi(t)}". Double check the action return value for the route.`)))}__amendWithOnBeforeCallbacks(e){return this.__runOnBeforeCallbacks(e).then(t=>t===this.__previousContext||t===e?t:this.__fullyResolveChain(t))}__runOnBeforeCallbacks(e){const t=this.__previousContext||{},i=t.chain||[],o=e.chain;let r=Promise.resolve();const s=()=>({cancel:!0}),l=a=>kt(e,a);if(e.__divergedChainIndex=0,e.__skipAttach=!1,i.length){for(let a=0;a<Math.min(i.length,o.length)&&!(i[a].route!==o[a].route||i[a].path!==o[a].path&&i[a].element!==o[a].element||!this.__isReusableElement(i[a].element,o[a].element));a=++e.__divergedChainIndex);if(e.__skipAttach=o.length===i.length&&e.__divergedChainIndex==o.length&&this.__isReusableElement(e.result,t.result),e.__skipAttach){for(let a=o.length-1;a>=0;a--)r=this.__runOnBeforeLeaveCallbacks(r,e,{prevent:s},i[a]);for(let a=0;a<o.length;a++)r=this.__runOnBeforeEnterCallbacks(r,e,{prevent:s,redirect:l},o[a]),i[a].element.location=R(e,i[a].route)}else for(let a=i.length-1;a>=e.__divergedChainIndex;a--)r=this.__runOnBeforeLeaveCallbacks(r,e,{prevent:s},i[a])}if(!e.__skipAttach)for(let a=0;a<o.length;a++)a<e.__divergedChainIndex?a<i.length&&i[a].element&&(i[a].element.location=R(e,i[a].route)):(r=this.__runOnBeforeEnterCallbacks(r,e,{prevent:s,redirect:l},o[a]),o[a].element&&(o[a].element.location=R(e,o[a].route)));return r.then(a=>{if(a){if(a.cancel)return this.__previousContext.__renderId=e.__renderId,this.__previousContext;if(a.redirect)return this.__redirect(a.redirect,e.__redirectCount,e.__renderId)}return e})}__runOnBeforeLeaveCallbacks(e,t,i,o){const r=R(t);return e.then(s=>{if(this.__isLatestRender(t))return Lt("onBeforeLeave",[r,i,this],o.element)(s)}).then(s=>{if(!(s||{}).redirect)return s})}__runOnBeforeEnterCallbacks(e,t,i,o){const r=R(t,o.route);return e.then(s=>{if(this.__isLatestRender(t))return Lt("onBeforeEnter",[r,i,this],o.element)(s)})}__isReusableElement(e,t){return e&&t?this.__createdByRouter.get(e)&&this.__createdByRouter.get(t)?e.localName===t.localName:e===t:!1}__isLatestRender(e){return e.__renderId===this.__lastStartedRenderId}__redirect(e,t,i){if(t>Zi)throw new Error(L(`Too many redirects when rendering ${e.from}`));return this.resolve({pathname:this.urlForPath(e.pathname,e.params),redirectFrom:e.from,__redirectCount:(t||0)+1,__renderId:i})}__ensureOutlet(e=this.__outlet){if(!(e instanceof Node))throw new TypeError(L(`Expected router outlet to be a valid DOM Node (but got ${e})`))}__updateBrowserHistory({pathname:e,search:t="",hash:i=""},o){if(window.location.pathname!==e||window.location.search!==t||window.location.hash!==i){const r=o?"replaceState":"pushState";window.history[r](null,document.title,e+t+i),window.dispatchEvent(new PopStateEvent("popstate",{state:"vaadin-router-ignore"}))}}__copyUnchangedElements(e,t){let i=this.__outlet;for(let o=0;o<e.__divergedChainIndex;o++){const r=t&&t.chain[o].element;if(r)if(r.parentNode===i)e.chain[o].element=r,i=r;else break}return i}__addAppearingContent(e,t){this.__ensureOutlet(),this.__removeAppearingContent();const i=this.__copyUnchangedElements(e,t);this.__appearingContent=[],this.__disappearingContent=Array.from(i.children).filter(r=>this.__addedByRouter.get(r)&&r!==e.result);let o=i;for(let r=e.__divergedChainIndex;r<e.chain.length;r++){const s=e.chain[r].element;s&&(o.appendChild(s),this.__addedByRouter.set(s,!0),o===i&&this.__appearingContent.push(s),o=s)}}__removeDisappearingContent(){this.__disappearingContent&&ue(this.__disappearingContent),this.__disappearingContent=null,this.__appearingContent=null}__removeAppearingContent(){this.__disappearingContent&&this.__appearingContent&&(ue(this.__appearingContent),this.__disappearingContent=null,this.__appearingContent=null)}__runOnAfterLeaveCallbacks(e,t){if(t)for(let i=t.chain.length-1;i>=e.__divergedChainIndex&&this.__isLatestRender(e);i--){const o=t.chain[i].element;if(o)try{const r=R(e);fe(o.onAfterLeave,[r,{},t.resolver],o)}finally{this.__disappearingContent.indexOf(o)>-1&&ue(o.children)}}}__runOnAfterEnterCallbacks(e){for(let t=e.__divergedChainIndex;t<e.chain.length&&this.__isLatestRender(e);t++){const i=e.chain[t].element||{},o=R(e,e.chain[t].route);fe(i.onAfterEnter,[o,{},e.resolver],i)}}__animateIfNeeded(e){const t=(this.__disappearingContent||[])[0],i=(this.__appearingContent||[])[0],o=[],r=e.chain;let s;for(let l=r.length;l>0;l--)if(r[l-1].route.animate){s=r[l-1].route.animate;break}if(t&&i&&s){const l=Ce(s)&&s.leave||"leaving",a=Ce(s)&&s.enter||"entering";o.push(Nt(t,l)),o.push(Nt(i,a))}return Promise.all(o).then(()=>e)}subscribe(){window.addEventListener("vaadin-router-go",this.__navigationEventHandler)}unsubscribe(){window.removeEventListener("vaadin-router-go",this.__navigationEventHandler)}__onNavigationEvent(e){const{pathname:t,search:i,hash:o}=e?e.detail:window.location;k(this.__normalizePathname(t))&&(e&&e.preventDefault&&e.preventDefault(),this.render({pathname:t,search:i,hash:o},!0))}static setTriggers(...e){Ji(e)}urlForName(e,t){return this.__urlForName||(this.__urlForName=Yi(this)),me(this.__urlForName(e,t),this)}urlForPath(e,t){return me(V.pathToRegexp.compile(e)(t),this)}static go(e){const{pathname:t,search:i,hash:o}=k(e)?this.__createUrl(e,"http://a"):e;return ie("go",{pathname:t,search:i,hash:o})}}const on=/\/\*[\*!]\s+vaadin-dev-mode:start([\s\S]*)vaadin-dev-mode:end\s+\*\*\//i,ge=window.Vaadin&&window.Vaadin.Flow&&window.Vaadin.Flow.clients;function rn(){function n(){return!0}return ri(n)}function sn(){try{return an()?!0:ln()?ge?!dn():!rn():!1}catch{return!1}}function an(){return localStorage.getItem("vaadin.developmentmode.force")}function ln(){return["localhost","127.0.0.1"].indexOf(window.location.hostname)>=0}function dn(){return!!(ge&&Object.keys(ge).map(e=>ge[e]).filter(e=>e.productionMode).length>0)}function ri(n,e){if(typeof n!="function")return;const t=on.exec(n.toString());if(t)try{n=new Function(t[1])}catch(i){console.log("vaadin-development-mode-detector: uncommentAndRun() failed",i)}return n(e)}window.Vaadin=window.Vaadin||{};const At=function(n,e){if(window.Vaadin.developmentMode)return ri(n,e)};window.Vaadin.developmentMode===void 0&&(window.Vaadin.developmentMode=sn());function cn(){}const hn=function(){if(typeof At=="function")return At(cn)};window.Vaadin=window.Vaadin||{};window.Vaadin.registrations=window.Vaadin.registrations||[];window.Vaadin.registrations.push({is:"@vaadin/router",version:"1.7.4"});hn();V.NavigationTrigger={POPSTATE:Ai,CLICK:ki};var Ie,b;(function(n){n.CONNECTED="connected",n.LOADING="loading",n.RECONNECTING="reconnecting",n.CONNECTION_LOST="connection-lost"})(b||(b={}));class un{constructor(e){this.stateChangeListeners=new Set,this.loadingCount=0,this.connectionState=e,this.serviceWorkerMessageListener=this.serviceWorkerMessageListener.bind(this),navigator.serviceWorker&&(navigator.serviceWorker.addEventListener("message",this.serviceWorkerMessageListener),navigator.serviceWorker.ready.then(t=>{var i;(i=t==null?void 0:t.active)===null||i===void 0||i.postMessage({method:"Vaadin.ServiceWorker.isConnectionLost",id:"Vaadin.ServiceWorker.isConnectionLost"})}))}addStateChangeListener(e){this.stateChangeListeners.add(e)}removeStateChangeListener(e){this.stateChangeListeners.delete(e)}loadingStarted(){this.state=b.LOADING,this.loadingCount+=1}loadingFinished(){this.decreaseLoadingCount(b.CONNECTED)}loadingFailed(){this.decreaseLoadingCount(b.CONNECTION_LOST)}decreaseLoadingCount(e){this.loadingCount>0&&(this.loadingCount-=1,this.loadingCount===0&&(this.state=e))}get state(){return this.connectionState}set state(e){if(e!==this.connectionState){const t=this.connectionState;this.connectionState=e,this.loadingCount=0;for(const i of this.stateChangeListeners)i(t,this.connectionState)}}get online(){return this.connectionState===b.CONNECTED||this.connectionState===b.LOADING}get offline(){return!this.online}serviceWorkerMessageListener(e){typeof e.data=="object"&&e.data.id==="Vaadin.ServiceWorker.isConnectionLost"&&(e.data.result===!0&&(this.state=b.CONNECTION_LOST),navigator.serviceWorker.removeEventListener("message",this.serviceWorkerMessageListener))}}const pn=n=>!!(n==="localhost"||n==="[::1]"||n.match(/^127\.\d+\.\d+\.\d+$/)),pe=window;if(!(!((Ie=pe.Vaadin)===null||Ie===void 0)&&Ie.connectionState)){let n;pn(window.location.hostname)?n=!0:n=navigator.onLine,pe.Vaadin=pe.Vaadin||{},pe.Vaadin.connectionState=new un(n?b.CONNECTED:b.CONNECTION_LOST)}function $(n,e,t,i){var o=arguments.length,r=o<3?e:i===null?i=Object.getOwnPropertyDescriptor(e,t):i,s;if(typeof Reflect=="object"&&typeof Reflect.decorate=="function")r=Reflect.decorate(n,e,t,i);else for(var l=n.length-1;l>=0;l--)(s=n[l])&&(r=(o<3?s(r):o>3?s(e,t,r):s(e,t))||r);return o>3&&r&&Object.defineProperty(e,t,r),r}/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const fn=!1,ve=window,lt=ve.ShadowRoot&&(ve.ShadyCSS===void 0||ve.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,dt=Symbol(),It=new WeakMap;class si{constructor(e,t,i){if(this._$cssResult$=!0,i!==dt)throw new Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=e,this._strings=t}get styleSheet(){let e=this._styleSheet;const t=this._strings;if(lt&&e===void 0){const i=t!==void 0&&t.length===1;i&&(e=It.get(t)),e===void 0&&((this._styleSheet=e=new CSSStyleSheet).replaceSync(this.cssText),i&&It.set(t,e))}return e}toString(){return this.cssText}}const mn=n=>{if(n._$cssResult$===!0)return n.cssText;if(typeof n=="number")return n;throw new Error(`Value passed to 'css' function must be a 'css' function result: ${n}. Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.`)},gn=n=>new si(typeof n=="string"?n:String(n),void 0,dt),x=(n,...e)=>{const t=n.length===1?n[0]:e.reduce((i,o,r)=>i+mn(o)+n[r+1],n[0]);return new si(t,n,dt)},vn=(n,e)=>{lt?n.adoptedStyleSheets=e.map(t=>t instanceof CSSStyleSheet?t:t.styleSheet):e.forEach(t=>{const i=document.createElement("style"),o=ve.litNonce;o!==void 0&&i.setAttribute("nonce",o),i.textContent=t.cssText,n.appendChild(i)})},_n=n=>{let e="";for(const t of n.cssRules)e+=t.cssText;return gn(e)},Pt=lt||fn?n=>n:n=>n instanceof CSSStyleSheet?_n(n):n;/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var Pe,Oe,De,ai;const I=window;let li,M;const Ot=I.trustedTypes,yn=Ot?Ot.emptyScript:"",_e=I.reactiveElementPolyfillSupportDevMode;{const n=(Pe=I.litIssuedWarnings)!==null&&Pe!==void 0?Pe:I.litIssuedWarnings=new Set;M=(e,t)=>{t+=` See https://lit.dev/msg/${e} for more information.`,n.has(t)||(console.warn(t),n.add(t))},M("dev-mode","Lit is in dev mode. Not recommended for production!"),!((Oe=I.ShadyDOM)===null||Oe===void 0)&&Oe.inUse&&_e===void 0&&M("polyfill-support-missing","Shadow DOM is being polyfilled via `ShadyDOM` but the `polyfill-support` module has not been loaded."),li=e=>({then:(t,i)=>{M("request-update-promise",`The \`requestUpdate\` method should no longer return a Promise but does so on \`${e}\`. Use \`updateComplete\` instead.`),t!==void 0&&t(!1)}})}const Ve=n=>{I.emitLitDebugLogEvents&&I.dispatchEvent(new CustomEvent("lit-debug",{detail:n}))},di=(n,e)=>n,nt={toAttribute(n,e){switch(e){case Boolean:n=n?yn:null;break;case Object:case Array:n=n==null?n:JSON.stringify(n);break}return n},fromAttribute(n,e){let t=n;switch(e){case Boolean:t=n!==null;break;case Number:t=n===null?null:Number(n);break;case Object:case Array:try{t=JSON.parse(n)}catch{t=null}break}return t}},ci=(n,e)=>e!==n&&(e===e||n===n),Me={attribute:!0,type:String,converter:nt,reflect:!1,hasChanged:ci},ot="finalized";class P extends HTMLElement{constructor(){super(),this.__instanceProperties=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this.__reflectingProperty=null,this.__initialize()}static addInitializer(e){var t;this.finalize(),((t=this._initializers)!==null&&t!==void 0?t:this._initializers=[]).push(e)}static get observedAttributes(){this.finalize();const e=[];return this.elementProperties.forEach((t,i)=>{const o=this.__attributeNameForProperty(i,t);o!==void 0&&(this.__attributeToPropertyMap.set(o,i),e.push(o))}),e}static createProperty(e,t=Me){var i;if(t.state&&(t.attribute=!1),this.finalize(),this.elementProperties.set(e,t),!t.noAccessor&&!this.prototype.hasOwnProperty(e)){const o=typeof e=="symbol"?Symbol():`__${e}`,r=this.getPropertyDescriptor(e,o,t);r!==void 0&&(Object.defineProperty(this.prototype,e,r),this.hasOwnProperty("__reactivePropertyKeys")||(this.__reactivePropertyKeys=new Set((i=this.__reactivePropertyKeys)!==null&&i!==void 0?i:[])),this.__reactivePropertyKeys.add(e))}}static getPropertyDescriptor(e,t,i){return{get(){return this[t]},set(o){const r=this[e];this[t]=o,this.requestUpdate(e,r,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(e){return this.elementProperties.get(e)||Me}static finalize(){if(this.hasOwnProperty(ot))return!1;this[ot]=!0;const e=Object.getPrototypeOf(this);if(e.finalize(),e._initializers!==void 0&&(this._initializers=[...e._initializers]),this.elementProperties=new Map(e.elementProperties),this.__attributeToPropertyMap=new Map,this.hasOwnProperty(di("properties"))){const t=this.properties,i=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const o of i)this.createProperty(o,t[o])}this.elementStyles=this.finalizeStyles(this.styles);{const t=(i,o=!1)=>{this.prototype.hasOwnProperty(i)&&M(o?"renamed-api":"removed-api",`\`${i}\` is implemented on class ${this.name}. It has been ${o?"renamed":"removed"} in this version of LitElement.`)};t("initialize"),t("requestUpdateInternal"),t("_getUpdateComplete",!0)}return!0}static finalizeStyles(e){const t=[];if(Array.isArray(e)){const i=new Set(e.flat(1/0).reverse());for(const o of i)t.unshift(Pt(o))}else e!==void 0&&t.push(Pt(e));return t}static __attributeNameForProperty(e,t){const i=t.attribute;return i===!1?void 0:typeof i=="string"?i:typeof e=="string"?e.toLowerCase():void 0}__initialize(){var e;this.__updatePromise=new Promise(t=>this.enableUpdating=t),this._$changedProperties=new Map,this.__saveInstanceProperties(),this.requestUpdate(),(e=this.constructor._initializers)===null||e===void 0||e.forEach(t=>t(this))}addController(e){var t,i;((t=this.__controllers)!==null&&t!==void 0?t:this.__controllers=[]).push(e),this.renderRoot!==void 0&&this.isConnected&&((i=e.hostConnected)===null||i===void 0||i.call(e))}removeController(e){var t;(t=this.__controllers)===null||t===void 0||t.splice(this.__controllers.indexOf(e)>>>0,1)}__saveInstanceProperties(){this.constructor.elementProperties.forEach((e,t)=>{this.hasOwnProperty(t)&&(this.__instanceProperties.set(t,this[t]),delete this[t])})}createRenderRoot(){var e;const t=(e=this.shadowRoot)!==null&&e!==void 0?e:this.attachShadow(this.constructor.shadowRootOptions);return vn(t,this.constructor.elementStyles),t}connectedCallback(){var e;this.renderRoot===void 0&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),(e=this.__controllers)===null||e===void 0||e.forEach(t=>{var i;return(i=t.hostConnected)===null||i===void 0?void 0:i.call(t)})}enableUpdating(e){}disconnectedCallback(){var e;(e=this.__controllers)===null||e===void 0||e.forEach(t=>{var i;return(i=t.hostDisconnected)===null||i===void 0?void 0:i.call(t)})}attributeChangedCallback(e,t,i){this._$attributeToProperty(e,i)}__propertyToAttribute(e,t,i=Me){var o;const r=this.constructor.__attributeNameForProperty(e,i);if(r!==void 0&&i.reflect===!0){const l=(((o=i.converter)===null||o===void 0?void 0:o.toAttribute)!==void 0?i.converter:nt).toAttribute(t,i.type);this.constructor.enabledWarnings.indexOf("migration")>=0&&l===void 0&&M("undefined-attribute-value",`The attribute value for the ${e} property is undefined on element ${this.localName}. The attribute will be removed, but in the previous version of \`ReactiveElement\`, the attribute would not have changed.`),this.__reflectingProperty=e,l==null?this.removeAttribute(r):this.setAttribute(r,l),this.__reflectingProperty=null}}_$attributeToProperty(e,t){var i;const o=this.constructor,r=o.__attributeToPropertyMap.get(e);if(r!==void 0&&this.__reflectingProperty!==r){const s=o.getPropertyOptions(r),l=typeof s.converter=="function"?{fromAttribute:s.converter}:((i=s.converter)===null||i===void 0?void 0:i.fromAttribute)!==void 0?s.converter:nt;this.__reflectingProperty=r,this[r]=l.fromAttribute(t,s.type),this.__reflectingProperty=null}}requestUpdate(e,t,i){let o=!0;return e!==void 0&&(i=i||this.constructor.getPropertyOptions(e),(i.hasChanged||ci)(this[e],t)?(this._$changedProperties.has(e)||this._$changedProperties.set(e,t),i.reflect===!0&&this.__reflectingProperty!==e&&(this.__reflectingProperties===void 0&&(this.__reflectingProperties=new Map),this.__reflectingProperties.set(e,i))):o=!1),!this.isUpdatePending&&o&&(this.__updatePromise=this.__enqueueUpdate()),li(this.localName)}async __enqueueUpdate(){this.isUpdatePending=!0;try{await this.__updatePromise}catch(t){Promise.reject(t)}const e=this.scheduleUpdate();return e!=null&&await e,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var e,t;if(!this.isUpdatePending)return;if(Ve==null||Ve({kind:"update"}),!this.hasUpdated){const r=[];if((e=this.constructor.__reactivePropertyKeys)===null||e===void 0||e.forEach(s=>{var l;this.hasOwnProperty(s)&&!(!((l=this.__instanceProperties)===null||l===void 0)&&l.has(s))&&r.push(s)}),r.length)throw new Error(`The following properties on element ${this.localName} will not trigger updates as expected because they are set using class fields: ${r.join(", ")}. Native class fields and some compiled output will overwrite accessors used for detecting changes. See https://lit.dev/msg/class-field-shadowing for more information.`)}this.__instanceProperties&&(this.__instanceProperties.forEach((r,s)=>this[s]=r),this.__instanceProperties=void 0);let i=!1;const o=this._$changedProperties;try{i=this.shouldUpdate(o),i?(this.willUpdate(o),(t=this.__controllers)===null||t===void 0||t.forEach(r=>{var s;return(s=r.hostUpdate)===null||s===void 0?void 0:s.call(r)}),this.update(o)):this.__markUpdated()}catch(r){throw i=!1,this.__markUpdated(),r}i&&this._$didUpdate(o)}willUpdate(e){}_$didUpdate(e){var t;(t=this.__controllers)===null||t===void 0||t.forEach(i=>{var o;return(o=i.hostUpdated)===null||o===void 0?void 0:o.call(i)}),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(e)),this.updated(e),this.isUpdatePending&&this.constructor.enabledWarnings.indexOf("change-in-update")>=0&&M("change-in-update",`Element ${this.localName} scheduled an update (generally because a property was set) after an update completed, causing a new update to be scheduled. This is inefficient and should be avoided unless the next update can only be scheduled as a side effect of the previous update.`)}__markUpdated(){this._$changedProperties=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this.__updatePromise}shouldUpdate(e){return!0}update(e){this.__reflectingProperties!==void 0&&(this.__reflectingProperties.forEach((t,i)=>this.__propertyToAttribute(i,this[i],t)),this.__reflectingProperties=void 0),this.__markUpdated()}updated(e){}firstUpdated(e){}}ai=ot;P[ai]=!0;P.elementProperties=new Map;P.elementStyles=[];P.shadowRootOptions={mode:"open"};_e==null||_e({ReactiveElement:P});{P.enabledWarnings=["change-in-update"];const n=function(e){e.hasOwnProperty(di("enabledWarnings"))||(e.enabledWarnings=e.enabledWarnings.slice())};P.enableWarning=function(e){n(this),this.enabledWarnings.indexOf(e)<0&&this.enabledWarnings.push(e)},P.disableWarning=function(e){n(this);const t=this.enabledWarnings.indexOf(e);t>=0&&this.enabledWarnings.splice(t,1)}}((De=I.reactiveElementVersions)!==null&&De!==void 0?De:I.reactiveElementVersions=[]).push("1.6.3");I.reactiveElementVersions.length>1&&M("multiple-versions","Multiple versions of Lit loaded. Loading multiple versions is not recommended.");/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var Ue,Fe,Be,ze;const C=window,f=n=>{C.emitLitDebugLogEvents&&C.dispatchEvent(new CustomEvent("lit-debug",{detail:n}))};let bn=0,$e;(Ue=C.litIssuedWarnings)!==null&&Ue!==void 0||(C.litIssuedWarnings=new Set),$e=(n,e)=>{e+=n?` See https://lit.dev/msg/${n} for more information.`:"",C.litIssuedWarnings.has(e)||(console.warn(e),C.litIssuedWarnings.add(e))},$e("dev-mode","Lit is in dev mode. Not recommended for production!");const T=!((Fe=C.ShadyDOM)===null||Fe===void 0)&&Fe.inUse&&((Be=C.ShadyDOM)===null||Be===void 0?void 0:Be.noPatch)===!0?C.ShadyDOM.wrap:n=>n,Y=C.trustedTypes,Dt=Y?Y.createPolicy("lit-html",{createHTML:n=>n}):void 0,wn=n=>n,Re=(n,e,t)=>wn,Sn=n=>{if(W!==Re)throw new Error("Attempted to overwrite existing lit-html security policy. setSanitizeDOMValueFactory should be called at most once.");W=n},En=()=>{W=Re},rt=(n,e,t)=>W(n,e,t),st="$lit$",O=`lit$${String(Math.random()).slice(9)}$`,hi="?"+O,Cn=`<${hi}>`,z=document,re=()=>z.createComment(""),se=n=>n===null||typeof n!="object"&&typeof n!="function",ui=Array.isArray,$n=n=>ui(n)||typeof(n==null?void 0:n[Symbol.iterator])=="function",He=`[ 	
\f\r]`,Tn=`[^ 	
\f\r"'\`<>=]`,xn=`[^\\s"'>=/]`,ee=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,Vt=1,We=2,Rn=3,Mt=/-->/g,Ut=/>/g,U=new RegExp(`>|${He}(?:(${xn}+)(${He}*=${He}*(?:${Tn}|("|')|))|$)`,"g"),Nn=0,Ft=1,kn=2,Bt=3,je=/'/g,Ge=/"/g,pi=/^(?:script|style|textarea|title)$/i,Ln=1,Te=2,ct=1,xe=2,An=3,In=4,Pn=5,ht=6,On=7,fi=n=>(e,...t)=>(e.some(i=>i===void 0)&&console.warn(`Some template strings are undefined.
This is probably caused by illegal octal escape sequences.`),{_$litType$:n,strings:e,values:t}),E=fi(Ln),mo=fi(Te),H=Symbol.for("lit-noChange"),y=Symbol.for("lit-nothing"),zt=new WeakMap,F=z.createTreeWalker(z,129,null,!1);let W=Re;function mi(n,e){if(!Array.isArray(n)||!n.hasOwnProperty("raw")){let t="invalid template strings array";throw t=`
          Internal Error: expected template strings to be an array
          with a 'raw' field. Faking a template strings array by
          calling html or svg like an ordinary function is effectively
          the same as calling unsafeHtml and can lead to major security
          issues, e.g. opening your code up to XSS attacks.
          If you're using the html or svg tagged template functions normally
          and still seeing this error, please file a bug at
          https://github.com/lit/lit/issues/new?template=bug_report.md
          and include information about your build tooling, if any.
        `.trim().replace(/\n */g,`
`),new Error(t)}return Dt!==void 0?Dt.createHTML(e):e}const Dn=(n,e)=>{const t=n.length-1,i=[];let o=e===Te?"<svg>":"",r,s=ee;for(let a=0;a<t;a++){const d=n[a];let c=-1,u,h=0,p;for(;h<d.length&&(s.lastIndex=h,p=s.exec(d),p!==null);)if(h=s.lastIndex,s===ee){if(p[Vt]==="!--")s=Mt;else if(p[Vt]!==void 0)s=Ut;else if(p[We]!==void 0)pi.test(p[We])&&(r=new RegExp(`</${p[We]}`,"g")),s=U;else if(p[Rn]!==void 0)throw new Error("Bindings in tag names are not supported. Please use static templates instead. See https://lit.dev/docs/templates/expressions/#static-expressions")}else s===U?p[Nn]===">"?(s=r??ee,c=-1):p[Ft]===void 0?c=-2:(c=s.lastIndex-p[kn].length,u=p[Ft],s=p[Bt]===void 0?U:p[Bt]==='"'?Ge:je):s===Ge||s===je?s=U:s===Mt||s===Ut?s=ee:(s=U,r=void 0);console.assert(c===-1||s===U||s===je||s===Ge,"unexpected parse state B");const G=s===U&&n[a+1].startsWith("/>")?" ":"";o+=s===ee?d+Cn:c>=0?(i.push(u),d.slice(0,c)+st+d.slice(c)+O+G):d+O+(c===-2?(i.push(void 0),a):G)}const l=o+(n[t]||"<?>")+(e===Te?"</svg>":"");return[mi(n,l),i]};class ae{constructor({strings:e,["_$litType$"]:t},i){this.parts=[];let o,r=0,s=0;const l=e.length-1,a=this.parts,[d,c]=Dn(e,t);if(this.el=ae.createElement(d,i),F.currentNode=this.el.content,t===Te){const u=this.el.content,h=u.firstChild;h.remove(),u.append(...h.childNodes)}for(;(o=F.nextNode())!==null&&a.length<l;){if(o.nodeType===1){{const u=o.localName;if(/^(?:textarea|template)$/i.test(u)&&o.innerHTML.includes(O)){const h=`Expressions are not supported inside \`${u}\` elements. See https://lit.dev/msg/expression-in-${u} for more information.`;if(u==="template")throw new Error(h);$e("",h)}}if(o.hasAttributes()){const u=[];for(const h of o.getAttributeNames())if(h.endsWith(st)||h.startsWith(O)){const p=c[s++];if(u.push(h),p!==void 0){const ke=o.getAttribute(p.toLowerCase()+st).split(O),q=/([.?@])?(.*)/.exec(p);a.push({type:ct,index:r,name:q[2],strings:ke,ctor:q[1]==="."?Mn:q[1]==="?"?Fn:q[1]==="@"?Bn:Ne})}else a.push({type:ht,index:r})}for(const h of u)o.removeAttribute(h)}if(pi.test(o.tagName)){const u=o.textContent.split(O),h=u.length-1;if(h>0){o.textContent=Y?Y.emptyScript:"";for(let p=0;p<h;p++)o.append(u[p],re()),F.nextNode(),a.push({type:xe,index:++r});o.append(u[h],re())}}}else if(o.nodeType===8)if(o.data===hi)a.push({type:xe,index:r});else{let h=-1;for(;(h=o.data.indexOf(O,h+1))!==-1;)a.push({type:On,index:r}),h+=O.length-1}r++}f==null||f({kind:"template prep",template:this,clonableTemplate:this.el,parts:this.parts,strings:e})}static createElement(e,t){const i=z.createElement("template");return i.innerHTML=e,i}}function J(n,e,t=n,i){var o,r,s,l;if(e===H)return e;let a=i!==void 0?(o=t.__directives)===null||o===void 0?void 0:o[i]:t.__directive;const d=se(e)?void 0:e._$litDirective$;return(a==null?void 0:a.constructor)!==d&&((r=a==null?void 0:a._$notifyDirectiveConnectionChanged)===null||r===void 0||r.call(a,!1),d===void 0?a=void 0:(a=new d(n),a._$initialize(n,t,i)),i!==void 0?((s=(l=t).__directives)!==null&&s!==void 0?s:l.__directives=[])[i]=a:t.__directive=a),a!==void 0&&(e=J(n,a._$resolve(n,e.values),a,i)),e}class Vn{constructor(e,t){this._$parts=[],this._$disconnectableChildren=void 0,this._$template=e,this._$parent=t}get parentNode(){return this._$parent.parentNode}get _$isConnected(){return this._$parent._$isConnected}_clone(e){var t;const{el:{content:i},parts:o}=this._$template,r=((t=e==null?void 0:e.creationScope)!==null&&t!==void 0?t:z).importNode(i,!0);F.currentNode=r;let s=F.nextNode(),l=0,a=0,d=o[0];for(;d!==void 0;){if(l===d.index){let c;d.type===xe?c=new ce(s,s.nextSibling,this,e):d.type===ct?c=new d.ctor(s,d.name,d.strings,this,e):d.type===ht&&(c=new zn(s,this,e)),this._$parts.push(c),d=o[++a]}l!==(d==null?void 0:d.index)&&(s=F.nextNode(),l++)}return F.currentNode=z,r}_update(e){let t=0;for(const i of this._$parts)i!==void 0&&(f==null||f({kind:"set part",part:i,value:e[t],valueIndex:t,values:e,templateInstance:this}),i.strings!==void 0?(i._$setValue(e,i,t),t+=i.strings.length-2):i._$setValue(e[t])),t++}}class ce{constructor(e,t,i,o){var r;this.type=xe,this._$committedValue=y,this._$disconnectableChildren=void 0,this._$startNode=e,this._$endNode=t,this._$parent=i,this.options=o,this.__isConnected=(r=o==null?void 0:o.isConnected)!==null&&r!==void 0?r:!0,this._textSanitizer=void 0}get _$isConnected(){var e,t;return(t=(e=this._$parent)===null||e===void 0?void 0:e._$isConnected)!==null&&t!==void 0?t:this.__isConnected}get parentNode(){let e=T(this._$startNode).parentNode;const t=this._$parent;return t!==void 0&&(e==null?void 0:e.nodeType)===11&&(e=t.parentNode),e}get startNode(){return this._$startNode}get endNode(){return this._$endNode}_$setValue(e,t=this){var i;if(this.parentNode===null)throw new Error("This `ChildPart` has no `parentNode` and therefore cannot accept a value. This likely means the element containing the part was manipulated in an unsupported way outside of Lit's control such that the part's marker nodes were ejected from DOM. For example, setting the element's `innerHTML` or `textContent` can do this.");if(e=J(this,e,t),se(e))e===y||e==null||e===""?(this._$committedValue!==y&&(f==null||f({kind:"commit nothing to child",start:this._$startNode,end:this._$endNode,parent:this._$parent,options:this.options}),this._$clear()),this._$committedValue=y):e!==this._$committedValue&&e!==H&&this._commitText(e);else if(e._$litType$!==void 0)this._commitTemplateResult(e);else if(e.nodeType!==void 0){if(((i=this.options)===null||i===void 0?void 0:i.host)===e){this._commitText("[probable mistake: rendered a template's host in itself (commonly caused by writing ${this} in a template]"),console.warn("Attempted to render the template host",e,"inside itself. This is almost always a mistake, and in dev mode ","we render some warning text. In production however, we'll ","render it, which will usually result in an error, and sometimes ","in the element disappearing from the DOM.");return}this._commitNode(e)}else $n(e)?this._commitIterable(e):this._commitText(e)}_insert(e){return T(T(this._$startNode).parentNode).insertBefore(e,this._$endNode)}_commitNode(e){var t;if(this._$committedValue!==e){if(this._$clear(),W!==Re){const i=(t=this._$startNode.parentNode)===null||t===void 0?void 0:t.nodeName;if(i==="STYLE"||i==="SCRIPT"){let o="Forbidden";throw i==="STYLE"?o="Lit does not support binding inside style nodes. This is a security risk, as style injection attacks can exfiltrate data and spoof UIs. Consider instead using css`...` literals to compose styles, and make do dynamic styling with css custom properties, ::parts, <slot>s, and by mutating the DOM rather than stylesheets.":o="Lit does not support binding inside script nodes. This is a security risk, as it could allow arbitrary code execution.",new Error(o)}}f==null||f({kind:"commit node",start:this._$startNode,parent:this._$parent,value:e,options:this.options}),this._$committedValue=this._insert(e)}}_commitText(e){if(this._$committedValue!==y&&se(this._$committedValue)){const t=T(this._$startNode).nextSibling;this._textSanitizer===void 0&&(this._textSanitizer=rt(t,"data","property")),e=this._textSanitizer(e),f==null||f({kind:"commit text",node:t,value:e,options:this.options}),t.data=e}else{const t=z.createTextNode("");this._commitNode(t),this._textSanitizer===void 0&&(this._textSanitizer=rt(t,"data","property")),e=this._textSanitizer(e),f==null||f({kind:"commit text",node:t,value:e,options:this.options}),t.data=e}this._$committedValue=e}_commitTemplateResult(e){var t;const{values:i,["_$litType$"]:o}=e,r=typeof o=="number"?this._$getTemplate(e):(o.el===void 0&&(o.el=ae.createElement(mi(o.h,o.h[0]),this.options)),o);if(((t=this._$committedValue)===null||t===void 0?void 0:t._$template)===r)f==null||f({kind:"template updating",template:r,instance:this._$committedValue,parts:this._$committedValue._$parts,options:this.options,values:i}),this._$committedValue._update(i);else{const s=new Vn(r,this),l=s._clone(this.options);f==null||f({kind:"template instantiated",template:r,instance:s,parts:s._$parts,options:this.options,fragment:l,values:i}),s._update(i),f==null||f({kind:"template instantiated and updated",template:r,instance:s,parts:s._$parts,options:this.options,fragment:l,values:i}),this._commitNode(l),this._$committedValue=s}}_$getTemplate(e){let t=zt.get(e.strings);return t===void 0&&zt.set(e.strings,t=new ae(e)),t}_commitIterable(e){ui(this._$committedValue)||(this._$committedValue=[],this._$clear());const t=this._$committedValue;let i=0,o;for(const r of e)i===t.length?t.push(o=new ce(this._insert(re()),this._insert(re()),this,this.options)):o=t[i],o._$setValue(r),i++;i<t.length&&(this._$clear(o&&T(o._$endNode).nextSibling,i),t.length=i)}_$clear(e=T(this._$startNode).nextSibling,t){var i;for((i=this._$notifyConnectionChanged)===null||i===void 0||i.call(this,!1,!0,t);e&&e!==this._$endNode;){const o=T(e).nextSibling;T(e).remove(),e=o}}setConnected(e){var t;if(this._$parent===void 0)this.__isConnected=e,(t=this._$notifyConnectionChanged)===null||t===void 0||t.call(this,e);else throw new Error("part.setConnected() may only be called on a RootPart returned from render().")}}class Ne{constructor(e,t,i,o,r){this.type=ct,this._$committedValue=y,this._$disconnectableChildren=void 0,this.element=e,this.name=t,this._$parent=o,this.options=r,i.length>2||i[0]!==""||i[1]!==""?(this._$committedValue=new Array(i.length-1).fill(new String),this.strings=i):this._$committedValue=y,this._sanitizer=void 0}get tagName(){return this.element.tagName}get _$isConnected(){return this._$parent._$isConnected}_$setValue(e,t=this,i,o){const r=this.strings;let s=!1;if(r===void 0)e=J(this,e,t,0),s=!se(e)||e!==this._$committedValue&&e!==H,s&&(this._$committedValue=e);else{const l=e;e=r[0];let a,d;for(a=0;a<r.length-1;a++)d=J(this,l[i+a],t,a),d===H&&(d=this._$committedValue[a]),s||(s=!se(d)||d!==this._$committedValue[a]),d===y?e=y:e!==y&&(e+=(d??"")+r[a+1]),this._$committedValue[a]=d}s&&!o&&this._commitValue(e)}_commitValue(e){e===y?T(this.element).removeAttribute(this.name):(this._sanitizer===void 0&&(this._sanitizer=W(this.element,this.name,"attribute")),e=this._sanitizer(e??""),f==null||f({kind:"commit attribute",element:this.element,name:this.name,value:e,options:this.options}),T(this.element).setAttribute(this.name,e??""))}}class Mn extends Ne{constructor(){super(...arguments),this.type=An}_commitValue(e){this._sanitizer===void 0&&(this._sanitizer=W(this.element,this.name,"property")),e=this._sanitizer(e),f==null||f({kind:"commit property",element:this.element,name:this.name,value:e,options:this.options}),this.element[this.name]=e===y?void 0:e}}const Un=Y?Y.emptyScript:"";class Fn extends Ne{constructor(){super(...arguments),this.type=In}_commitValue(e){f==null||f({kind:"commit boolean attribute",element:this.element,name:this.name,value:!!(e&&e!==y),options:this.options}),e&&e!==y?T(this.element).setAttribute(this.name,Un):T(this.element).removeAttribute(this.name)}}class Bn extends Ne{constructor(e,t,i,o,r){if(super(e,t,i,o,r),this.type=Pn,this.strings!==void 0)throw new Error(`A \`<${e.localName}>\` has a \`@${t}=...\` listener with invalid content. Event listeners in templates must have exactly one expression and no surrounding text.`)}_$setValue(e,t=this){var i;if(e=(i=J(this,e,t,0))!==null&&i!==void 0?i:y,e===H)return;const o=this._$committedValue,r=e===y&&o!==y||e.capture!==o.capture||e.once!==o.once||e.passive!==o.passive,s=e!==y&&(o===y||r);f==null||f({kind:"commit event listener",element:this.element,name:this.name,value:e,options:this.options,removeListener:r,addListener:s,oldListener:o}),r&&this.element.removeEventListener(this.name,this,o),s&&this.element.addEventListener(this.name,this,e),this._$committedValue=e}handleEvent(e){var t,i;typeof this._$committedValue=="function"?this._$committedValue.call((i=(t=this.options)===null||t===void 0?void 0:t.host)!==null&&i!==void 0?i:this.element,e):this._$committedValue.handleEvent(e)}}class zn{constructor(e,t,i){this.element=e,this.type=ht,this._$disconnectableChildren=void 0,this._$parent=t,this.options=i}get _$isConnected(){return this._$parent._$isConnected}_$setValue(e){f==null||f({kind:"commit to element binding",element:this.element,value:e,options:this.options}),J(this,e)}}const qe=C.litHtmlPolyfillSupportDevMode;qe==null||qe(ae,ce);((ze=C.litHtmlVersions)!==null&&ze!==void 0?ze:C.litHtmlVersions=[]).push("2.8.0");C.litHtmlVersions.length>1&&$e("multiple-versions","Multiple versions of Lit loaded. Loading multiple versions is not recommended.");const ye=(n,e,t)=>{var i,o;if(e==null)throw new TypeError(`The container to render into may not be ${e}`);const r=bn++,s=(i=t==null?void 0:t.renderBefore)!==null&&i!==void 0?i:e;let l=s._$litPart$;if(f==null||f({kind:"begin render",id:r,value:n,container:e,options:t,part:l}),l===void 0){const a=(o=t==null?void 0:t.renderBefore)!==null&&o!==void 0?o:null;s._$litPart$=l=new ce(e.insertBefore(re(),a),a,void 0,t??{})}return l._$setValue(n),f==null||f({kind:"end render",id:r,value:n,container:e,options:t,part:l}),l};ye.setSanitizer=Sn,ye.createSanitizer=rt,ye._testOnlyClearSanitizerFactoryDoNotCallOrElse=En;/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var Ke,Ye,Je;let ut;{const n=(Ke=globalThis.litIssuedWarnings)!==null&&Ke!==void 0?Ke:globalThis.litIssuedWarnings=new Set;ut=(e,t)=>{t+=` See https://lit.dev/msg/${e} for more information.`,n.has(t)||(console.warn(t),n.add(t))}}class j extends P{constructor(){super(...arguments),this.renderOptions={host:this},this.__childPart=void 0}createRenderRoot(){var e,t;const i=super.createRenderRoot();return(e=(t=this.renderOptions).renderBefore)!==null&&e!==void 0||(t.renderBefore=i.firstChild),i}update(e){const t=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(e),this.__childPart=ye(t,this.renderRoot,this.renderOptions)}connectedCallback(){var e;super.connectedCallback(),(e=this.__childPart)===null||e===void 0||e.setConnected(!0)}disconnectedCallback(){var e;super.disconnectedCallback(),(e=this.__childPart)===null||e===void 0||e.setConnected(!1)}render(){return H}}j.finalized=!0;j._$litElement$=!0;(Ye=globalThis.litElementHydrateSupport)===null||Ye===void 0||Ye.call(globalThis,{LitElement:j});const Xe=globalThis.litElementPolyfillSupportDevMode;Xe==null||Xe({LitElement:j});j.finalize=function(){if(!P.finalize.call(this))return!1;const e=(t,i,o=!1)=>{if(t.hasOwnProperty(i)){const r=(typeof t=="function"?t:t.constructor).name;ut(o?"renamed-api":"removed-api",`\`${i}\` is implemented on class ${r}. It has been ${o?"renamed":"removed"} in this version of LitElement.`)}};return e(this,"render"),e(this,"getStyles",!0),e(this.prototype,"adoptStyles"),!0};((Je=globalThis.litElementVersions)!==null&&Je!==void 0?Je:globalThis.litElementVersions=[]).push("3.3.3");globalThis.litElementVersions.length>1&&ut("multiple-versions","Multiple versions of Lit loaded. Loading multiple versions is not recommended.");/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const Hn=(n,e)=>e.kind==="method"&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(t){t.createProperty(e.key,n)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){typeof e.initializer=="function"&&(this[e.key]=e.initializer.call(this))},finisher(t){t.createProperty(e.key,n)}},Wn=(n,e,t)=>{e.constructor.createProperty(t,n)};function _(n){return(e,t)=>t!==void 0?Wn(n,e,t):Hn(n,e)}/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */function Z(n){return _({...n,state:!0})}/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const jn=({finisher:n,descriptor:e})=>(t,i)=>{var o;if(i!==void 0){const r=t.constructor;e!==void 0&&Object.defineProperty(t,i,e(i)),n==null||n(r,i)}else{const r=(o=t.originalKey)!==null&&o!==void 0?o:t.key,s=e!=null?{kind:"method",placement:"prototype",key:r,descriptor:e(t.key)}:{...t,key:r};return n!=null&&(s.finisher=function(l){n(l,r)}),s}};/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */function Gn(n,e){return jn({descriptor:t=>{const i={get(){var o,r;return(r=(o=this.renderRoot)===null||o===void 0?void 0:o.querySelector(n))!==null&&r!==void 0?r:null},enumerable:!0,configurable:!0};if(e){const o=typeof t=="symbol"?Symbol():`__${t}`;i.get=function(){var r,s;return this[o]===void 0&&(this[o]=(s=(r=this.renderRoot)===null||r===void 0?void 0:r.querySelector(n))!==null&&s!==void 0?s:null),this[o]}}return i}})}/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */var Qe;const qn=window;((Qe=qn.HTMLSlotElement)===null||Qe===void 0?void 0:Qe.prototype.assignedElements)!=null;/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */const Kn={ATTRIBUTE:1,CHILD:2,PROPERTY:3,BOOLEAN_ATTRIBUTE:4,EVENT:5,ELEMENT:6},Yn=n=>(...e)=>({_$litDirective$:n,values:e});class Jn{constructor(e){}get _$isConnected(){return this._$parent._$isConnected}_$initialize(e,t,i){this.__part=e,this._$parent=t,this.__attributeIndex=i}_$resolve(e,t){return this.update(e,t)}update(e,t){return this.render(...t)}}/**
 * @license
 * Copyright 2018 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */class Xn extends Jn{constructor(e){var t;if(super(e),e.type!==Kn.ATTRIBUTE||e.name!=="class"||((t=e.strings)===null||t===void 0?void 0:t.length)>2)throw new Error("`classMap()` can only be used in the `class` attribute and must be the only part in the attribute.")}render(e){return" "+Object.keys(e).filter(t=>e[t]).join(" ")+" "}update(e,[t]){var i,o;if(this._previousClasses===void 0){this._previousClasses=new Set,e.strings!==void 0&&(this._staticClasses=new Set(e.strings.join(" ").split(/\s/).filter(s=>s!=="")));for(const s in t)t[s]&&!(!((i=this._staticClasses)===null||i===void 0)&&i.has(s))&&this._previousClasses.add(s);return this.render(t)}const r=e.element.classList;this._previousClasses.forEach(s=>{s in t||(r.remove(s),this._previousClasses.delete(s))});for(const s in t){const l=!!t[s];l!==this._previousClasses.has(s)&&!(!((o=this._staticClasses)===null||o===void 0)&&o.has(s))&&(l?(r.add(s),this._previousClasses.add(s)):(r.remove(s),this._previousClasses.delete(s)))}return H}}const gi=Yn(Xn),Ze="css-loading-indicator";var N;(function(n){n.IDLE="",n.FIRST="first",n.SECOND="second",n.THIRD="third"})(N||(N={}));class w extends j{constructor(){super(),this.firstDelay=450,this.secondDelay=1500,this.thirdDelay=5e3,this.expandedDuration=2e3,this.onlineText="Online",this.offlineText="Connection lost",this.reconnectingText="Connection lost, trying to reconnect...",this.offline=!1,this.reconnecting=!1,this.expanded=!1,this.loading=!1,this.loadingBarState=N.IDLE,this.applyDefaultThemeState=!0,this.firstTimeout=0,this.secondTimeout=0,this.thirdTimeout=0,this.expandedTimeout=0,this.lastMessageState=b.CONNECTED,this.connectionStateListener=()=>{this.expanded=this.updateConnectionState(),this.expandedTimeout=this.timeoutFor(this.expandedTimeout,this.expanded,()=>{this.expanded=!1},this.expandedDuration)}}static create(){var e,t;const i=window;return!((e=i.Vaadin)===null||e===void 0)&&e.connectionIndicator||(i.Vaadin=i.Vaadin||{},i.Vaadin.connectionIndicator=document.createElement("vaadin-connection-indicator"),document.body.appendChild(i.Vaadin.connectionIndicator)),(t=i.Vaadin)===null||t===void 0?void 0:t.connectionIndicator}render(){return E`
      <div class="v-loading-indicator ${this.loadingBarState}" style=${this.getLoadingBarStyle()}></div>

      <div
        class="v-status-message ${gi({active:this.reconnecting})}"
      >
        <span class="text"> ${this.renderMessage()} </span>
      </div>
    `}connectedCallback(){var e;super.connectedCallback();const t=window;!((e=t.Vaadin)===null||e===void 0)&&e.connectionState&&(this.connectionStateStore=t.Vaadin.connectionState,this.connectionStateStore.addStateChangeListener(this.connectionStateListener),this.updateConnectionState()),this.updateTheme()}disconnectedCallback(){super.disconnectedCallback(),this.connectionStateStore&&this.connectionStateStore.removeStateChangeListener(this.connectionStateListener),this.updateTheme()}get applyDefaultTheme(){return this.applyDefaultThemeState}set applyDefaultTheme(e){e!==this.applyDefaultThemeState&&(this.applyDefaultThemeState=e,this.updateTheme())}createRenderRoot(){return this}updateConnectionState(){var e;const t=(e=this.connectionStateStore)===null||e===void 0?void 0:e.state;return this.offline=t===b.CONNECTION_LOST,this.reconnecting=t===b.RECONNECTING,this.updateLoading(t===b.LOADING),this.loading?!1:t!==this.lastMessageState?(this.lastMessageState=t,!0):!1}updateLoading(e){this.loading=e,this.loadingBarState=N.IDLE,this.firstTimeout=this.timeoutFor(this.firstTimeout,e,()=>{this.loadingBarState=N.FIRST},this.firstDelay),this.secondTimeout=this.timeoutFor(this.secondTimeout,e,()=>{this.loadingBarState=N.SECOND},this.secondDelay),this.thirdTimeout=this.timeoutFor(this.thirdTimeout,e,()=>{this.loadingBarState=N.THIRD},this.thirdDelay)}renderMessage(){return this.reconnecting?this.reconnectingText:this.offline?this.offlineText:this.onlineText}updateTheme(){if(this.applyDefaultThemeState&&this.isConnected){if(!document.getElementById(Ze)){const e=document.createElement("style");e.id=Ze,e.textContent=this.getDefaultStyle(),document.head.appendChild(e)}}else{const e=document.getElementById(Ze);e&&document.head.removeChild(e)}}getDefaultStyle(){return`
      @keyframes v-progress-start {
        0% {
          width: 0%;
        }
        100% {
          width: 50%;
        }
      }
      @keyframes v-progress-delay {
        0% {
          width: 50%;
        }
        100% {
          width: 90%;
        }
      }
      @keyframes v-progress-wait {
        0% {
          width: 90%;
          height: 4px;
        }
        3% {
          width: 91%;
          height: 7px;
        }
        100% {
          width: 96%;
          height: 7px;
        }
      }
      @keyframes v-progress-wait-pulse {
        0% {
          opacity: 1;
        }
        50% {
          opacity: 0.1;
        }
        100% {
          opacity: 1;
        }
      }
      .v-loading-indicator,
      .v-status-message {
        position: fixed;
        z-index: 251;
        left: 0;
        right: auto;
        top: 0;
        background-color: var(--lumo-primary-color, var(--material-primary-color, blue));
        transition: none;
      }
      .v-loading-indicator {
        width: 50%;
        height: 4px;
        opacity: 1;
        pointer-events: none;
        animation: v-progress-start 1000ms 200ms both;
      }
      .v-loading-indicator[style*='none'] {
        display: block !important;
        width: 100%;
        opacity: 0;
        animation: none;
        transition: opacity 500ms 300ms, width 300ms;
      }
      .v-loading-indicator.second {
        width: 90%;
        animation: v-progress-delay 3.8s forwards;
      }
      .v-loading-indicator.third {
        width: 96%;
        animation: v-progress-wait 5s forwards, v-progress-wait-pulse 1s 4s infinite backwards;
      }

      vaadin-connection-indicator[offline] .v-loading-indicator,
      vaadin-connection-indicator[reconnecting] .v-loading-indicator {
        display: none;
      }

      .v-status-message {
        opacity: 0;
        width: 100%;
        max-height: var(--status-height-collapsed, 8px);
        overflow: hidden;
        background-color: var(--status-bg-color-online, var(--lumo-primary-color, var(--material-primary-color, blue)));
        color: var(
          --status-text-color-online,
          var(--lumo-primary-contrast-color, var(--material-primary-contrast-color, #fff))
        );
        font-size: 0.75rem;
        font-weight: 600;
        line-height: 1;
        transition: all 0.5s;
        padding: 0 0.5em;
      }

      vaadin-connection-indicator[offline] .v-status-message,
      vaadin-connection-indicator[reconnecting] .v-status-message {
        opacity: 1;
        background-color: var(--status-bg-color-offline, var(--lumo-shade, #333));
        color: var(
          --status-text-color-offline,
          var(--lumo-primary-contrast-color, var(--material-primary-contrast-color, #fff))
        );
        background-image: repeating-linear-gradient(
          45deg,
          rgba(255, 255, 255, 0),
          rgba(255, 255, 255, 0) 10px,
          rgba(255, 255, 255, 0.1) 10px,
          rgba(255, 255, 255, 0.1) 20px
        );
      }

      vaadin-connection-indicator[reconnecting] .v-status-message {
        animation: show-reconnecting-status 2s;
      }

      vaadin-connection-indicator[offline] .v-status-message:hover,
      vaadin-connection-indicator[reconnecting] .v-status-message:hover,
      vaadin-connection-indicator[expanded] .v-status-message {
        max-height: var(--status-height, 1.75rem);
      }

      vaadin-connection-indicator[expanded] .v-status-message {
        opacity: 1;
      }

      .v-status-message span {
        display: flex;
        align-items: center;
        justify-content: center;
        height: var(--status-height, 1.75rem);
      }

      vaadin-connection-indicator[reconnecting] .v-status-message span::before {
        content: '';
        width: 1em;
        height: 1em;
        border-top: 2px solid
          var(--status-spinner-color, var(--lumo-primary-color, var(--material-primary-color, blue)));
        border-left: 2px solid
          var(--status-spinner-color, var(--lumo-primary-color, var(--material-primary-color, blue)));
        border-right: 2px solid transparent;
        border-bottom: 2px solid transparent;
        border-radius: 50%;
        box-sizing: border-box;
        animation: v-spin 0.4s linear infinite;
        margin: 0 0.5em;
      }

      @keyframes v-spin {
        100% {
          transform: rotate(360deg);
        }
      }
    `}getLoadingBarStyle(){switch(this.loadingBarState){case N.IDLE:return"display: none";case N.FIRST:case N.SECOND:case N.THIRD:return"display: block";default:return""}}timeoutFor(e,t,i,o){return e!==0&&window.clearTimeout(e),t?window.setTimeout(i,o):0}static get instance(){return w.create()}}$([_({type:Number})],w.prototype,"firstDelay",void 0);$([_({type:Number})],w.prototype,"secondDelay",void 0);$([_({type:Number})],w.prototype,"thirdDelay",void 0);$([_({type:Number})],w.prototype,"expandedDuration",void 0);$([_({type:String})],w.prototype,"onlineText",void 0);$([_({type:String})],w.prototype,"offlineText",void 0);$([_({type:String})],w.prototype,"reconnectingText",void 0);$([_({type:Boolean,reflect:!0})],w.prototype,"offline",void 0);$([_({type:Boolean,reflect:!0})],w.prototype,"reconnecting",void 0);$([_({type:Boolean,reflect:!0})],w.prototype,"expanded",void 0);$([_({type:Boolean,reflect:!0})],w.prototype,"loading",void 0);$([_({type:String})],w.prototype,"loadingBarState",void 0);$([_({type:Boolean})],w.prototype,"applyDefaultTheme",null);customElements.get("vaadin-connection-indicator")===void 0&&customElements.define("vaadin-connection-indicator",w);w.instance;const le=window;le.Vaadin=le.Vaadin||{};le.Vaadin.registrations=le.Vaadin.registrations||[];le.Vaadin.registrations.push({is:"@vaadin/common-frontend",version:"0.0.18"});class Ht extends Error{}const te=window.document.body,v=window;class Qn{constructor(e){this.response=void 0,this.pathname="",this.isActive=!1,this.baseRegex=/^\//,te.$=te.$||[],this.config=e||{},v.Vaadin=v.Vaadin||{},v.Vaadin.Flow=v.Vaadin.Flow||{},v.Vaadin.Flow.clients={TypeScript:{isActive:()=>this.isActive}};const t=document.head.querySelector("base");this.baseRegex=new RegExp(`^${(document.baseURI||t&&t.href||"/").replace(/^https?:\/\/[^/]+/i,"")}`),this.appShellTitle=document.title,this.addConnectionIndicator()}get serverSideRoutes(){return[{path:"(.*)",action:this.action}]}loadingStarted(){this.isActive=!0,v.Vaadin.connectionState.loadingStarted()}loadingFinished(){this.isActive=!1,v.Vaadin.connectionState.loadingFinished()}get action(){return async e=>{if(this.pathname=e.pathname,v.Vaadin.connectionState.online)try{await this.flowInit()}catch(t){if(t instanceof Ht)return v.Vaadin.connectionState.state=b.CONNECTION_LOST,this.offlineStubAction();throw t}else return this.offlineStubAction();return this.container.onBeforeEnter=(t,i)=>this.flowNavigate(t,i),this.container.onBeforeLeave=(t,i)=>this.flowLeave(t,i),this.container}}async flowLeave(e,t){const{connectionState:i}=v.Vaadin;return this.pathname===e.pathname||!this.isFlowClientLoaded()||i.offline?Promise.resolve({}):new Promise(o=>{this.loadingStarted(),this.container.serverConnected=r=>{o(t&&r?t.prevent():{}),this.loadingFinished()},te.$server.leaveNavigation(this.getFlowRoutePath(e),this.getFlowRouteQuery(e))})}async flowNavigate(e,t){return this.response?new Promise(i=>{this.loadingStarted(),this.container.serverConnected=(o,r)=>{t&&o?i(t.prevent()):t&&t.redirect&&r?i(t.redirect(r.pathname)):(this.container.style.display="",i(this.container)),this.loadingFinished()},te.$server.connectClient(this.container.localName,this.container.id,this.getFlowRoutePath(e),this.getFlowRouteQuery(e),this.appShellTitle,history.state)}):Promise.resolve(this.container)}getFlowRoutePath(e){return decodeURIComponent(e.pathname).replace(this.baseRegex,"")}getFlowRouteQuery(e){return e.search&&e.search.substring(1)||""}async flowInit(e=!1){if(!this.isFlowClientLoaded()){this.loadingStarted(),this.response=await this.flowInitUi(e),this.response.appConfig.clientRouting=!e;const{pushScript:t,appConfig:i}=this.response;typeof t=="string"&&await this.loadScript(t);const{appId:o}=i;await(await be(()=>import("./FlowBootstrap-feff2646.js"),[],import.meta.url)).init(this.response),typeof this.config.imports=="function"&&(this.injectAppIdScript(o),await this.config.imports());const s=await be(()=>import("./FlowClient-e0ae8105.js"),[],import.meta.url);if(await this.flowInitClient(s),!e){const l=`flow-container-${o.toLowerCase()}`;this.container=document.createElement(l),te.$[o]=this.container,this.container.id=o}this.loadingFinished()}return this.container&&!this.container.isConnected&&(this.container.style.display="none",document.body.appendChild(this.container)),this.response}async loadScript(e){return new Promise((t,i)=>{const o=document.createElement("script");o.onload=()=>t(),o.onerror=i,o.src=e,document.body.appendChild(o)})}injectAppIdScript(e){const t=e.substring(0,e.lastIndexOf("-")),i=document.createElement("script");i.type="module",i.setAttribute("data-app-id",t),document.body.append(i)}async flowInitClient(e){return e.init(),new Promise(t=>{const i=setInterval(()=>{Object.keys(v.Vaadin.Flow.clients).filter(r=>r!=="TypeScript").reduce((r,s)=>r||v.Vaadin.Flow.clients[s].isActive(),!1)||(clearInterval(i),t())},5)})}async flowInitUi(e){const t=v.Vaadin&&v.Vaadin.TypeScript&&v.Vaadin.TypeScript.initial;return t?(v.Vaadin.TypeScript.initial=void 0,Promise.resolve(t)):new Promise((i,o)=>{const s=new XMLHttpRequest,l=e?"&serverSideRouting":"",a=`?v-r=init&location=${encodeURIComponent(this.getFlowRoutePath(location))}&query=${encodeURIComponent(this.getFlowRouteQuery(location))}${l}`;s.open("GET",a),s.onerror=()=>o(new Ht(`Invalid server response when initializing Flow UI.
        ${s.status}
        ${s.responseText}`)),s.onload=()=>{const d=s.getResponseHeader("content-type");d&&d.indexOf("application/json")!==-1?i(JSON.parse(s.responseText)):s.onerror()},s.send()})}addConnectionIndicator(){w.create(),v.addEventListener("online",()=>{if(!this.isFlowClientLoaded()){v.Vaadin.connectionState.state=b.RECONNECTING;const e=new XMLHttpRequest;e.open("HEAD","sw.js"),e.onload=()=>{v.Vaadin.connectionState.state=b.CONNECTED},e.onerror=()=>{v.Vaadin.connectionState.state=b.CONNECTION_LOST},setTimeout(()=>e.send(),50)}}),v.addEventListener("offline",()=>{this.isFlowClientLoaded()||(v.Vaadin.connectionState.state=b.CONNECTION_LOST)})}async offlineStubAction(){const e=document.createElement("iframe"),t="./offline-stub.html";e.setAttribute("src",t),e.setAttribute("style","width: 100%; height: 100%; border: 0"),this.response=void 0;let i;const o=()=>{i!==void 0&&(v.Vaadin.connectionState.removeStateChangeListener(i),i=void 0)};return e.onBeforeEnter=(r,s,l)=>{i=()=>{v.Vaadin.connectionState.online&&(o(),l.render(r,!1))},v.Vaadin.connectionState.addStateChangeListener(i)},e.onBeforeLeave=(r,s,l)=>{o()},e}isFlowClientLoaded(){return this.response!==void 0}}const{serverSideRoutes:Zn}=new Qn({imports:()=>be(()=>import("./generated-flow-imports-3a3b4c8d.js"),["./generated-flow-imports-3a3b4c8d.js","./custom-element-1c6555a1.js"],import.meta.url)}),eo=[...Zn],to=new V(document.querySelector("#outlet"));to.setRoutes(eo);var io=function(){var n=document.getSelection();if(!n.rangeCount)return function(){};for(var e=document.activeElement,t=[],i=0;i<n.rangeCount;i++)t.push(n.getRangeAt(i));switch(e.tagName.toUpperCase()){case"INPUT":case"TEXTAREA":e.blur();break;default:e=null;break}return n.removeAllRanges(),function(){n.type==="Caret"&&n.removeAllRanges(),n.rangeCount||t.forEach(function(o){n.addRange(o)}),e&&e.focus()}},Wt={"text/plain":"Text","text/html":"Url",default:"Text"},no="Copy to clipboard: #{key}, Enter";function oo(n){var e=(/mac os x/i.test(navigator.userAgent)?"⌘":"Ctrl")+"+C";return n.replace(/#{\s*key\s*}/g,e)}function ro(n,e){var t,i,o,r,s,l,a=!1;e||(e={}),t=e.debug||!1;try{o=io(),r=document.createRange(),s=document.getSelection(),l=document.createElement("span"),l.textContent=n,l.style.all="unset",l.style.position="fixed",l.style.top=0,l.style.clip="rect(0, 0, 0, 0)",l.style.whiteSpace="pre",l.style.webkitUserSelect="text",l.style.MozUserSelect="text",l.style.msUserSelect="text",l.style.userSelect="text",l.addEventListener("copy",function(c){if(c.stopPropagation(),e.format)if(c.preventDefault(),typeof c.clipboardData>"u"){t&&console.warn("unable to use e.clipboardData"),t&&console.warn("trying IE specific stuff"),window.clipboardData.clearData();var u=Wt[e.format]||Wt.default;window.clipboardData.setData(u,n)}else c.clipboardData.clearData(),c.clipboardData.setData(e.format,n);e.onCopy&&(c.preventDefault(),e.onCopy(c.clipboardData))}),document.body.appendChild(l),r.selectNodeContents(l),s.addRange(r);var d=document.execCommand("copy");if(!d)throw new Error("copy command was unsuccessful");a=!0}catch(c){t&&console.error("unable to copy using execCommand: ",c),t&&console.warn("trying IE specific stuff");try{window.clipboardData.setData(e.format||"text",n),e.onCopy&&e.onCopy(window.clipboardData),a=!0}catch(u){t&&console.error("unable to copy using clipboardData: ",u),t&&console.error("falling back to prompt"),i=oo("message"in e?e.message:no),window.prompt(i,n)}}finally{s&&(typeof s.removeRange=="function"?s.removeRange(r):s.removeAllRanges()),l&&document.body.removeChild(l),o()}return a}const pt=1e3,ft=(n,e)=>{const t=Array.from(n.querySelectorAll(e.join(", "))),i=Array.from(n.querySelectorAll("*")).filter(o=>o.shadowRoot).flatMap(o=>ft(o.shadowRoot,e));return[...t,...i]};let jt=!1;const de=(n,e)=>{jt||(window.addEventListener("message",o=>{o.data==="validate-license"&&window.location.reload()},!1),jt=!0);const t=n._overlayElement;if(t){if(t.shadowRoot){const o=t.shadowRoot.querySelector("slot:not([name])");if(o&&o.assignedElements().length>0){de(o.assignedElements()[0],e);return}}de(t,e);return}const i=e.messageHtml?e.messageHtml:`${e.message} <p>Component: ${e.product.name} ${e.product.version}</p>`.replace(/https:([^ ]*)/g,"<a href='https:$1'>https:$1</a>");n.isConnected&&(n.outerHTML=`<no-license style="display:flex;align-items:center;text-align:center;justify-content:center;"><div>${i}</div></no-license>`)},ne={},Gt={},X={},vi={},A=n=>`${n.name}_${n.version}`,qt=n=>{const{cvdlName:e,version:t}=n.constructor,i={name:e,version:t},o=n.tagName.toLowerCase();ne[e]=ne[e]??[],ne[e].push(o);const r=X[A(i)];r&&setTimeout(()=>de(n,r),pt),X[A(i)]||vi[A(i)]||Gt[A(i)]||(Gt[A(i)]=!0,window.Vaadin.devTools.checkLicense(i))},so=n=>{vi[A(n)]=!0,console.debug("License check ok for",n)},_i=n=>{const e=n.product.name;X[A(n.product)]=n,console.error("License check failed for",e);const t=ne[e];(t==null?void 0:t.length)>0&&ft(document,t).forEach(i=>{setTimeout(()=>de(i,X[A(n.product)]),pt)})},ao=n=>{const e=n.message,t=n.product.name;n.messageHtml=`No license found. <a target=_blank onclick="javascript:window.open(this.href);return false;" href="${e}">Go here to start a trial or retrieve your license.</a>`,X[A(n.product)]=n,console.error("No license found when checking",t);const i=ne[t];(i==null?void 0:i.length)>0&&ft(document,i).forEach(o=>{setTimeout(()=>de(o,X[A(n.product)]),pt)})},lo=()=>{window.Vaadin.devTools.createdCvdlElements.forEach(n=>{qt(n)}),window.Vaadin.devTools.createdCvdlElements={push:n=>{qt(n)}}};var co=Object.defineProperty,ho=Object.getOwnPropertyDescriptor,S=(n,e,t,i)=>{for(var o=i>1?void 0:i?ho(e,t):e,r=n.length-1,s;r>=0;r--)(s=n[r])&&(o=(i?s(e,t,o):s(o))||o);return i&&o&&co(e,t,o),o};const yi=class bi extends Object{constructor(e){super(),this.status="unavailable",e&&(this.webSocket=new WebSocket(e),this.webSocket.onmessage=t=>this.handleMessage(t),this.webSocket.onerror=t=>this.handleError(t),this.webSocket.onclose=t=>{this.status!=="error"&&this.setStatus("unavailable"),this.webSocket=void 0}),setInterval(()=>{this.webSocket&&self.status!=="error"&&this.status!=="unavailable"&&this.webSocket.send("")},bi.HEARTBEAT_INTERVAL)}onHandshake(){}onReload(){}onConnectionError(e){}onStatusChange(e){}onMessage(e){console.error("Unknown message received from the live reload server:",e)}handleMessage(e){let t;try{t=JSON.parse(e.data)}catch(i){this.handleError(`[${i.name}: ${i.message}`);return}t.command==="hello"?(this.setStatus("active"),this.onHandshake()):t.command==="reload"?this.status==="active"&&this.onReload():t.command==="license-check-ok"?so(t.data):t.command==="license-check-failed"?_i(t.data):t.command==="license-check-nokey"?ao(t.data):this.onMessage(t)}handleError(e){console.error(e),this.setStatus("error"),e instanceof Event&&this.webSocket?this.onConnectionError(`Error in WebSocket connection to ${this.webSocket.url}`):this.onConnectionError(e)}setActive(e){!e&&this.status==="active"?this.setStatus("inactive"):e&&this.status==="inactive"&&this.setStatus("active")}setStatus(e){this.status!==e&&(this.status=e,this.onStatusChange(e))}send(e,t){const i=JSON.stringify({command:e,data:t});this.webSocket?this.webSocket.readyState!==WebSocket.OPEN?this.webSocket.addEventListener("open",()=>this.webSocket.send(i)):this.webSocket.send(i):console.error(`Unable to send message ${e}. No websocket is available`)}setFeature(e,t){this.send("setFeature",{featureId:e,enabled:t})}sendTelemetry(e){this.send("reportTelemetry",{browserData:e})}sendLicenseCheck(e){this.send("checkLicense",e)}sendShowComponentCreateLocation(e){this.send("showComponentCreateLocation",e)}sendShowComponentAttachLocation(e){this.send("showComponentAttachLocation",e)}};yi.HEARTBEAT_INTERVAL=18e4;let et=yi;const uo=x`
  .popup {
    width: auto;
    position: fixed;
    background-color: var(--dev-tools-background-color-active-blurred);
    color: var(--dev-tools-text-color-primary);
    padding: 0.1875rem 0.75rem 0.1875rem 1rem;
    background-clip: padding-box;
    border-radius: var(--dev-tools-border-radius);
    overflow: hidden;
    margin: 0.5rem;
    width: 30rem;
    max-width: calc(100% - 1rem);
    max-height: calc(100vh - 1rem);
    flex-shrink: 1;
    background-color: var(--dev-tools-background-color-active);
    color: var(--dev-tools-text-color);
    transition: var(--dev-tools-transition-duration);
    transform-origin: bottom right;
    display: flex;
    flex-direction: column;
    box-shadow: var(--dev-tools-box-shadow);
    outline: none;
  }
`,m=class g extends j{constructor(){super(),this.expanded=!1,this.messages=[],this.notifications=[],this.frontendStatus="unavailable",this.javaStatus="unavailable",this.tabs=[{id:"log",title:"Log",render:this.renderLog,activate:this.activateLog},{id:"info",title:"Info",render:this.renderInfo},{id:"features",title:"Feature Flags",render:this.renderFeatures}],this.activeTab="log",this.serverInfo={flowVersion:"",vaadinVersion:"",javaVersion:"",osVersion:"",productName:""},this.features=[],this.unreadErrors=!1,this.componentPickActive=!1,this.nextMessageId=1,this.transitionDuration=0,window.Vaadin.Flow&&this.tabs.push({id:"code",title:"Code",render:this.renderCode})}static get styles(){return[x`
        :host {
          --dev-tools-font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen-Sans, Ubuntu, Cantarell,
            'Helvetica Neue', sans-serif;
          --dev-tools-font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, 'Liberation Mono', 'Courier New',
            monospace;

          --dev-tools-font-size: 0.8125rem;
          --dev-tools-font-size-small: 0.75rem;

          --dev-tools-text-color: rgba(255, 255, 255, 0.8);
          --dev-tools-text-color-secondary: rgba(255, 255, 255, 0.65);
          --dev-tools-text-color-emphasis: rgba(255, 255, 255, 0.95);
          --dev-tools-text-color-active: rgba(255, 255, 255, 1);

          --dev-tools-background-color-inactive: rgba(45, 45, 45, 0.25);
          --dev-tools-background-color-active: rgba(45, 45, 45, 0.98);
          --dev-tools-background-color-active-blurred: rgba(45, 45, 45, 0.85);

          --dev-tools-border-radius: 0.5rem;
          --dev-tools-box-shadow: 0 0 0 1px rgba(255, 255, 255, 0.05), 0 4px 12px -2px rgba(0, 0, 0, 0.4);

          --dev-tools-blue-hsl: ${this.BLUE_HSL};
          --dev-tools-blue-color: hsl(var(--dev-tools-blue-hsl));
          --dev-tools-green-hsl: ${this.GREEN_HSL};
          --dev-tools-green-color: hsl(var(--dev-tools-green-hsl));
          --dev-tools-grey-hsl: ${this.GREY_HSL};
          --dev-tools-grey-color: hsl(var(--dev-tools-grey-hsl));
          --dev-tools-yellow-hsl: ${this.YELLOW_HSL};
          --dev-tools-yellow-color: hsl(var(--dev-tools-yellow-hsl));
          --dev-tools-red-hsl: ${this.RED_HSL};
          --dev-tools-red-color: hsl(var(--dev-tools-red-hsl));

          /* Needs to be in ms, used in JavaScript as well */
          --dev-tools-transition-duration: 180ms;

          all: initial;

          direction: ltr;
          cursor: default;
          font: normal 400 var(--dev-tools-font-size) / 1.125rem var(--dev-tools-font-family);
          color: var(--dev-tools-text-color);
          -webkit-user-select: none;
          -moz-user-select: none;
          user-select: none;

          position: fixed;
          z-index: 20000;
          pointer-events: none;
          bottom: 0;
          right: 0;
          width: 100%;
          height: 100%;
          display: flex;
          flex-direction: column-reverse;
          align-items: flex-end;
        }

        .dev-tools {
          pointer-events: auto;
          display: flex;
          align-items: center;
          position: fixed;
          z-index: inherit;
          right: 0.5rem;
          bottom: 0.5rem;
          min-width: 1.75rem;
          height: 1.75rem;
          max-width: 1.75rem;
          border-radius: 0.5rem;
          padding: 0.375rem;
          box-sizing: border-box;
          background-color: var(--dev-tools-background-color-inactive);
          box-shadow: 0 0 0 1px rgba(255, 255, 255, 0.05);
          color: var(--dev-tools-text-color);
          transition: var(--dev-tools-transition-duration);
          white-space: nowrap;
          line-height: 1rem;
        }

        .dev-tools:hover,
        .dev-tools.active {
          background-color: var(--dev-tools-background-color-active);
          box-shadow: var(--dev-tools-box-shadow);
        }

        .dev-tools.active {
          max-width: calc(100% - 1rem);
        }

        .dev-tools .dev-tools-icon {
          flex: none;
          pointer-events: none;
          display: inline-block;
          width: 1rem;
          height: 1rem;
          fill: #fff;
          transition: var(--dev-tools-transition-duration);
          margin: 0;
        }

        .dev-tools.active .dev-tools-icon {
          opacity: 0;
          position: absolute;
          transform: scale(0.5);
        }

        .dev-tools .status-blip {
          flex: none;
          display: block;
          width: 6px;
          height: 6px;
          border-radius: 50%;
          z-index: 20001;
          background: var(--dev-tools-grey-color);
          position: absolute;
          top: -1px;
          right: -1px;
        }

        .dev-tools .status-description {
          overflow: hidden;
          text-overflow: ellipsis;
          padding: 0 0.25rem;
        }

        .dev-tools.error {
          background-color: hsla(var(--dev-tools-red-hsl), 0.15);
          animation: bounce 0.5s;
          animation-iteration-count: 2;
        }

        .switch {
          display: inline-flex;
          align-items: center;
        }

        .switch input {
          opacity: 0;
          width: 0;
          height: 0;
          position: absolute;
        }

        .switch .slider {
          display: block;
          flex: none;
          width: 28px;
          height: 18px;
          border-radius: 9px;
          background-color: rgba(255, 255, 255, 0.3);
          transition: var(--dev-tools-transition-duration);
          margin-right: 0.5rem;
        }

        .switch:focus-within .slider,
        .switch .slider:hover {
          background-color: rgba(255, 255, 255, 0.35);
          transition: none;
        }

        .switch input:focus-visible ~ .slider {
          box-shadow: 0 0 0 2px var(--dev-tools-background-color-active), 0 0 0 4px var(--dev-tools-blue-color);
        }

        .switch .slider::before {
          content: '';
          display: block;
          margin: 2px;
          width: 14px;
          height: 14px;
          background-color: #fff;
          transition: var(--dev-tools-transition-duration);
          border-radius: 50%;
        }

        .switch input:checked + .slider {
          background-color: var(--dev-tools-green-color);
        }

        .switch input:checked + .slider::before {
          transform: translateX(10px);
        }

        .switch input:disabled + .slider::before {
          background-color: var(--dev-tools-grey-color);
        }

        .window.hidden {
          opacity: 0;
          transform: scale(0);
          position: absolute;
        }

        .window.visible {
          transform: none;
          opacity: 1;
          pointer-events: auto;
        }

        .window.visible ~ .dev-tools {
          opacity: 0;
          pointer-events: none;
        }

        .window.visible ~ .dev-tools .dev-tools-icon,
        .window.visible ~ .dev-tools .status-blip {
          transition: none;
          opacity: 0;
        }

        .window {
          border-radius: var(--dev-tools-border-radius);
          overflow: hidden;
          margin: 0.5rem;
          width: 30rem;
          max-width: calc(100% - 1rem);
          max-height: calc(100vh - 1rem);
          flex-shrink: 1;
          background-color: var(--dev-tools-background-color-active);
          color: var(--dev-tools-text-color);
          transition: var(--dev-tools-transition-duration);
          transform-origin: bottom right;
          display: flex;
          flex-direction: column;
          box-shadow: var(--dev-tools-box-shadow);
          outline: none;
        }

        .window-toolbar {
          display: flex;
          flex: none;
          align-items: center;
          padding: 0.375rem;
          white-space: nowrap;
          order: 1;
          background-color: rgba(0, 0, 0, 0.2);
          gap: 0.5rem;
        }

        .tab {
          color: var(--dev-tools-text-color-secondary);
          font: inherit;
          font-size: var(--dev-tools-font-size-small);
          font-weight: 500;
          line-height: 1;
          padding: 0.25rem 0.375rem;
          background: none;
          border: none;
          margin: 0;
          border-radius: 0.25rem;
          transition: var(--dev-tools-transition-duration);
        }

        .tab:hover,
        .tab.active {
          color: var(--dev-tools-text-color-active);
        }

        .tab.active {
          background-color: rgba(255, 255, 255, 0.12);
        }

        .tab.unreadErrors::after {
          content: '•';
          color: hsl(var(--dev-tools-red-hsl));
          font-size: 1.5rem;
          position: absolute;
          transform: translate(0, -50%);
        }

        .ahreflike {
          font-weight: 500;
          color: var(--dev-tools-text-color-secondary);
          text-decoration: underline;
          cursor: pointer;
        }

        .ahreflike:hover {
          color: var(--dev-tools-text-color-emphasis);
        }

        .button {
          all: initial;
          font-family: inherit;
          font-size: var(--dev-tools-font-size-small);
          line-height: 1;
          white-space: nowrap;
          background-color: rgba(0, 0, 0, 0.2);
          color: inherit;
          font-weight: 600;
          padding: 0.25rem 0.375rem;
          border-radius: 0.25rem;
        }

        .button:focus,
        .button:hover {
          color: var(--dev-tools-text-color-emphasis);
        }

        .minimize-button {
          flex: none;
          width: 1rem;
          height: 1rem;
          color: inherit;
          background-color: transparent;
          border: 0;
          padding: 0;
          margin: 0 0 0 auto;
          opacity: 0.8;
        }

        .minimize-button:hover {
          opacity: 1;
        }

        .minimize-button svg {
          max-width: 100%;
        }

        .message.information {
          --dev-tools-notification-color: var(--dev-tools-blue-color);
        }

        .message.warning {
          --dev-tools-notification-color: var(--dev-tools-yellow-color);
        }

        .message.error {
          --dev-tools-notification-color: var(--dev-tools-red-color);
        }

        .message {
          display: flex;
          padding: 0.1875rem 0.75rem 0.1875rem 2rem;
          background-clip: padding-box;
        }

        .message.log {
          padding-left: 0.75rem;
        }

        .message-content {
          margin-right: 0.5rem;
          -webkit-user-select: text;
          -moz-user-select: text;
          user-select: text;
        }

        .message-heading {
          position: relative;
          display: flex;
          align-items: center;
          margin: 0.125rem 0;
        }

        .message.log {
          color: var(--dev-tools-text-color-secondary);
        }

        .message:not(.log) .message-heading {
          font-weight: 500;
        }

        .message.has-details .message-heading {
          color: var(--dev-tools-text-color-emphasis);
          font-weight: 600;
        }

        .message-heading::before {
          position: absolute;
          margin-left: -1.5rem;
          display: inline-block;
          text-align: center;
          font-size: 0.875em;
          font-weight: 600;
          line-height: calc(1.25em - 2px);
          width: 14px;
          height: 14px;
          box-sizing: border-box;
          border: 1px solid transparent;
          border-radius: 50%;
        }

        .message.information .message-heading::before {
          content: 'i';
          border-color: currentColor;
          color: var(--dev-tools-notification-color);
        }

        .message.warning .message-heading::before,
        .message.error .message-heading::before {
          content: '!';
          color: var(--dev-tools-background-color-active);
          background-color: var(--dev-tools-notification-color);
        }

        .features-tray {
          padding: 0.75rem;
          flex: auto;
          overflow: auto;
          animation: fade-in var(--dev-tools-transition-duration) ease-in;
          user-select: text;
        }

        .features-tray p {
          margin-top: 0;
          color: var(--dev-tools-text-color-secondary);
        }

        .features-tray .feature {
          display: flex;
          align-items: center;
          gap: 1rem;
          padding-bottom: 0.5em;
        }

        .message .message-details {
          font-weight: 400;
          color: var(--dev-tools-text-color-secondary);
          margin: 0.25rem 0;
        }

        .message .message-details[hidden] {
          display: none;
        }

        .message .message-details p {
          display: inline;
          margin: 0;
          margin-right: 0.375em;
          word-break: break-word;
        }

        .message .persist {
          color: var(--dev-tools-text-color-secondary);
          white-space: nowrap;
          margin: 0.375rem 0;
          display: flex;
          align-items: center;
          position: relative;
          -webkit-user-select: none;
          -moz-user-select: none;
          user-select: none;
        }

        .message .persist::before {
          content: '';
          width: 1em;
          height: 1em;
          border-radius: 0.2em;
          margin-right: 0.375em;
          background-color: rgba(255, 255, 255, 0.3);
        }

        .message .persist:hover::before {
          background-color: rgba(255, 255, 255, 0.4);
        }

        .message .persist.on::before {
          background-color: rgba(255, 255, 255, 0.9);
        }

        .message .persist.on::after {
          content: '';
          order: -1;
          position: absolute;
          width: 0.75em;
          height: 0.25em;
          border: 2px solid var(--dev-tools-background-color-active);
          border-width: 0 0 2px 2px;
          transform: translate(0.05em, -0.05em) rotate(-45deg) scale(0.8, 0.9);
        }

        .message .dismiss-message {
          font-weight: 600;
          align-self: stretch;
          display: flex;
          align-items: center;
          padding: 0 0.25rem;
          margin-left: 0.5rem;
          color: var(--dev-tools-text-color-secondary);
        }

        .message .dismiss-message:hover {
          color: var(--dev-tools-text-color);
        }

        .notification-tray {
          display: flex;
          flex-direction: column-reverse;
          align-items: flex-end;
          margin: 0.5rem;
          flex: none;
        }

        .window.hidden + .notification-tray {
          margin-bottom: 3rem;
        }

        .notification-tray .message {
          pointer-events: auto;
          background-color: var(--dev-tools-background-color-active);
          color: var(--dev-tools-text-color);
          max-width: 30rem;
          box-sizing: border-box;
          border-radius: var(--dev-tools-border-radius);
          margin-top: 0.5rem;
          transition: var(--dev-tools-transition-duration);
          transform-origin: bottom right;
          animation: slideIn var(--dev-tools-transition-duration);
          box-shadow: var(--dev-tools-box-shadow);
          padding-top: 0.25rem;
          padding-bottom: 0.25rem;
        }

        .notification-tray .message.animate-out {
          animation: slideOut forwards var(--dev-tools-transition-duration);
        }

        .notification-tray .message .message-details {
          max-height: 10em;
          overflow: hidden;
        }

        .message-tray {
          flex: auto;
          overflow: auto;
          max-height: 20rem;
          user-select: text;
        }

        .message-tray .message {
          animation: fade-in var(--dev-tools-transition-duration) ease-in;
          padding-left: 2.25rem;
        }

        .message-tray .message.warning {
          background-color: hsla(var(--dev-tools-yellow-hsl), 0.09);
        }

        .message-tray .message.error {
          background-color: hsla(var(--dev-tools-red-hsl), 0.09);
        }

        .message-tray .message.error .message-heading {
          color: hsl(var(--dev-tools-red-hsl));
        }

        .message-tray .message.warning .message-heading {
          color: hsl(var(--dev-tools-yellow-hsl));
        }

        .message-tray .message + .message {
          border-top: 1px solid rgba(255, 255, 255, 0.07);
        }

        .message-tray .dismiss-message,
        .message-tray .persist {
          display: none;
        }

        .info-tray {
          padding: 0.75rem;
          position: relative;
          flex: auto;
          overflow: auto;
          animation: fade-in var(--dev-tools-transition-duration) ease-in;
          user-select: text;
        }

        .info-tray dl {
          margin: 0;
          display: grid;
          grid-template-columns: max-content 1fr;
          column-gap: 0.75rem;
          position: relative;
        }

        .info-tray dt {
          grid-column: 1;
          color: var(--dev-tools-text-color-emphasis);
        }

        .info-tray dt:not(:first-child)::before {
          content: '';
          width: 100%;
          position: absolute;
          height: 1px;
          background-color: rgba(255, 255, 255, 0.1);
          margin-top: -0.375rem;
        }

        .info-tray dd {
          grid-column: 2;
          margin: 0;
        }

        .info-tray :is(dt, dd):not(:last-child) {
          margin-bottom: 0.75rem;
        }

        .info-tray dd + dd {
          margin-top: -0.5rem;
        }

        .info-tray .live-reload-status::before {
          content: '•';
          color: var(--status-color);
          width: 0.75rem;
          display: inline-block;
          font-size: 1rem;
          line-height: 0.5rem;
        }

        .info-tray .copy {
          position: fixed;
          z-index: 1;
          top: 0.5rem;
          right: 0.5rem;
        }

        .info-tray .switch {
          vertical-align: -4px;
        }

        @keyframes slideIn {
          from {
            transform: translateX(100%);
            opacity: 0;
          }
          to {
            transform: translateX(0%);
            opacity: 1;
          }
        }

        @keyframes slideOut {
          from {
            transform: translateX(0%);
            opacity: 1;
          }
          to {
            transform: translateX(100%);
            opacity: 0;
          }
        }

        @keyframes fade-in {
          0% {
            opacity: 0;
          }
        }

        @keyframes bounce {
          0% {
            transform: scale(0.8);
          }
          50% {
            transform: scale(1.5);
            background-color: hsla(var(--dev-tools-red-hsl), 1);
          }
          100% {
            transform: scale(1);
          }
        }

        @supports (backdrop-filter: blur(1px)) {
          .dev-tools,
          .window,
          .notification-tray .message {
            backdrop-filter: blur(8px);
          }
          .dev-tools:hover,
          .dev-tools.active,
          .window,
          .notification-tray .message {
            background-color: var(--dev-tools-background-color-active-blurred);
          }
        }
      `,uo]}static get isActive(){const e=window.sessionStorage.getItem(g.ACTIVE_KEY_IN_SESSION_STORAGE);return e===null||e!=="false"}static notificationDismissed(e){const t=window.localStorage.getItem(g.DISMISSED_NOTIFICATIONS_IN_LOCAL_STORAGE);return t!==null&&t.includes(e)}elementTelemetry(){let e={};try{const t=localStorage.getItem("vaadin.statistics.basket");if(!t)return;e=JSON.parse(t)}catch{return}this.frontendConnection&&this.frontendConnection.sendTelemetry(e)}openWebSocketConnection(){this.frontendStatus="unavailable",this.javaStatus="unavailable";const e=l=>this.log("error",l),t=()=>{if(this.liveReloadDisabled)return;this.showSplashMessage("Reloading…");const l=window.sessionStorage.getItem(g.TRIGGERED_COUNT_KEY_IN_SESSION_STORAGE),a=l?parseInt(l,10)+1:1;window.sessionStorage.setItem(g.TRIGGERED_COUNT_KEY_IN_SESSION_STORAGE,a.toString()),window.sessionStorage.setItem(g.TRIGGERED_KEY_IN_SESSION_STORAGE,"true"),window.location.reload()},i=new et(this.getDedicatedWebSocketUrl());i.onHandshake=()=>{this.log("log","Vaadin development mode initialized"),g.isActive||i.setActive(!1),this.elementTelemetry()},i.onConnectionError=e,i.onReload=t,i.onStatusChange=l=>{this.frontendStatus=l},i.onMessage=l=>{(l==null?void 0:l.command)==="serverInfo"?this.serverInfo=l.data:(l==null?void 0:l.command)==="featureFlags"?this.features=l.data.features:console.error("Unknown message from front-end connection:",JSON.stringify(l))},this.frontendConnection=i;let o;this.backend===g.SPRING_BOOT_DEVTOOLS&&this.springBootLiveReloadPort?(o=new et(this.getSpringBootWebSocketUrl(window.location)),o.onHandshake=()=>{g.isActive||o.setActive(!1)},o.onReload=t,o.onConnectionError=e):this.backend===g.JREBEL||this.backend===g.HOTSWAP_AGENT?o=i:o=new et(void 0);const r=o.onStatusChange;o.onStatusChange=l=>{r(l),this.javaStatus=l};const s=o.onHandshake;o.onHandshake=()=>{s(),this.backend&&this.log("information",`Java live reload available: ${g.BACKEND_DISPLAY_NAME[this.backend]}`)},this.javaConnection=o,this.backend||this.showNotification("warning","Java live reload unavailable","Live reload for Java changes is currently not set up. Find out how to make use of this functionality to boost your workflow.","https://vaadin.com/docs/latest/flow/configuration/live-reload","liveReloadUnavailable")}getDedicatedWebSocketUrl(){function e(i){const o=document.createElement("div");return o.innerHTML=`<a href="${i}"/>`,o.firstChild.href}if(this.url===void 0)return;const t=e(this.url);if(!t.startsWith("http://")&&!t.startsWith("https://")){console.error("The protocol of the url should be http or https for live reload to work.");return}return`${t.replace(/^http/,"ws")}?v-r=push&debug_window`}getSpringBootWebSocketUrl(e){const{hostname:t}=e,i=e.protocol==="https:"?"wss":"ws";if(t.endsWith("gitpod.io")){const o=t.replace(/.*?-/,"");return`${i}://${this.springBootLiveReloadPort}-${o}`}else return`${i}://${t}:${this.springBootLiveReloadPort}`}connectedCallback(){if(super.connectedCallback(),this.catchErrors(),this.disableEventListener=i=>this.demoteSplashMessage(),document.body.addEventListener("focus",this.disableEventListener),document.body.addEventListener("click",this.disableEventListener),this.openWebSocketConnection(),window.sessionStorage.getItem(g.TRIGGERED_KEY_IN_SESSION_STORAGE)){const i=new Date,o=`${`0${i.getHours()}`.slice(-2)}:${`0${i.getMinutes()}`.slice(-2)}:${`0${i.getSeconds()}`.slice(-2)}`;this.showSplashMessage(`Page reloaded at ${o}`),window.sessionStorage.removeItem(g.TRIGGERED_KEY_IN_SESSION_STORAGE)}this.transitionDuration=parseInt(window.getComputedStyle(this).getPropertyValue("--dev-tools-transition-duration"),10);const t=window;t.Vaadin=t.Vaadin||{},t.Vaadin.devTools=Object.assign(this,t.Vaadin.devTools),lo(),document.documentElement.addEventListener("vaadin-overlay-outside-click",i=>{i.detail.sourceEvent.composedPath().includes(this)&&i.preventDefault()})}format(e){return e.toString()}catchErrors(){const e=window.Vaadin.ConsoleErrors;e&&e.forEach(t=>{this.log("error",t.map(i=>this.format(i)).join(" "))}),window.Vaadin.ConsoleErrors={push:t=>{this.log("error",t.map(i=>this.format(i)).join(" "))}}}disconnectedCallback(){this.disableEventListener&&(document.body.removeEventListener("focus",this.disableEventListener),document.body.removeEventListener("click",this.disableEventListener)),super.disconnectedCallback()}toggleExpanded(){this.notifications.slice().forEach(e=>this.dismissNotification(e.id)),this.expanded=!this.expanded,this.expanded&&this.root.focus()}showSplashMessage(e){this.splashMessage=e,this.splashMessage&&(this.expanded?this.demoteSplashMessage():setTimeout(()=>{this.demoteSplashMessage()},g.AUTO_DEMOTE_NOTIFICATION_DELAY))}demoteSplashMessage(){this.splashMessage&&this.log("log",this.splashMessage),this.showSplashMessage(void 0)}checkLicense(e){this.frontendConnection?this.frontendConnection.sendLicenseCheck(e):_i({message:"Internal error: no connection",product:e})}log(e,t,i,o){const r=this.nextMessageId;for(this.nextMessageId+=1,this.messages.push({id:r,type:e,message:t,details:i,link:o,dontShowAgain:!1,deleted:!1});this.messages.length>g.MAX_LOG_ROWS;)this.messages.shift();this.requestUpdate(),this.updateComplete.then(()=>{const s=this.renderRoot.querySelector(".message-tray .message:last-child");this.expanded&&s?(setTimeout(()=>s.scrollIntoView({behavior:"smooth"}),this.transitionDuration),this.unreadErrors=!1):e==="error"&&(this.unreadErrors=!0)})}showNotification(e,t,i,o,r){if(r===void 0||!g.notificationDismissed(r)){if(this.notifications.filter(a=>a.persistentId===r).filter(a=>!a.deleted).length>0)return;const l=this.nextMessageId;this.nextMessageId+=1,this.notifications.push({id:l,type:e,message:t,details:i,link:o,persistentId:r,dontShowAgain:!1,deleted:!1}),o===void 0&&setTimeout(()=>{this.dismissNotification(l)},g.AUTO_DEMOTE_NOTIFICATION_DELAY),this.requestUpdate()}else this.log(e,t,i,o)}dismissNotification(e){const t=this.findNotificationIndex(e);if(t!==-1&&!this.notifications[t].deleted){const i=this.notifications[t];if(i.dontShowAgain&&i.persistentId&&!g.notificationDismissed(i.persistentId)){let o=window.localStorage.getItem(g.DISMISSED_NOTIFICATIONS_IN_LOCAL_STORAGE);o=o===null?i.persistentId:`${o},${i.persistentId}`,window.localStorage.setItem(g.DISMISSED_NOTIFICATIONS_IN_LOCAL_STORAGE,o)}i.deleted=!0,this.log(i.type,i.message,i.details,i.link),setTimeout(()=>{const o=this.findNotificationIndex(e);o!==-1&&(this.notifications.splice(o,1),this.requestUpdate())},this.transitionDuration)}}findNotificationIndex(e){let t=-1;return this.notifications.some((i,o)=>i.id===e?(t=o,!0):!1),t}toggleDontShowAgain(e){const t=this.findNotificationIndex(e);if(t!==-1&&!this.notifications[t].deleted){const i=this.notifications[t];i.dontShowAgain=!i.dontShowAgain,this.requestUpdate()}}setActive(e){var t,i;(t=this.frontendConnection)==null||t.setActive(e),(i=this.javaConnection)==null||i.setActive(e),window.sessionStorage.setItem(g.ACTIVE_KEY_IN_SESSION_STORAGE,e?"true":"false")}getStatusColor(e){return e==="active"?x`hsl(${g.GREEN_HSL})`:e==="inactive"?x`hsl(${g.GREY_HSL})`:e==="unavailable"?x`hsl(${g.YELLOW_HSL})`:e==="error"?x`hsl(${g.RED_HSL})`:x`none`}renderMessage(e){return E`
      <div
        class="message ${e.type} ${e.deleted?"animate-out":""} ${e.details||e.link?"has-details":""}"
      >
        <div class="message-content">
          <div class="message-heading">${e.message}</div>
          <div class="message-details" ?hidden="${!e.details&&!e.link}">
            ${e.details?E`<p>${e.details}</p>`:""}
            ${e.link?E`<a class="ahreflike" href="${e.link}" target="_blank">Learn more</a>`:""}
          </div>
          ${e.persistentId?E`<div
                class="persist ${e.dontShowAgain?"on":"off"}"
                @click=${()=>this.toggleDontShowAgain(e.id)}
              >
                Don’t show again
              </div>`:""}
        </div>
        <div class="dismiss-message" @click=${()=>this.dismissNotification(e.id)}>Dismiss</div>
      </div>
    `}render(){return E` <div
        class="window ${this.expanded&&!this.componentPickActive?"visible":"hidden"}"
        tabindex="0"
        @keydown=${e=>e.key==="Escape"&&this.expanded&&this.toggleExpanded()}
      >
        <div class="window-toolbar">
          ${this.tabs.map(e=>E`<button
                class=${gi({tab:!0,active:this.activeTab===e.id,unreadErrors:e.id==="log"&&this.unreadErrors})}
                id="${e.id}"
                @click=${()=>{this.activeTab=e.id,e.activate&&e.activate.call(this)}}
              >
                ${e.title}
              </button> `)}
          <button class="minimize-button" title="Minimize" @click=${()=>this.toggleExpanded()}>
            <svg fill="none" height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
              <g fill="#fff" opacity=".8">
                <path
                  d="m7.25 1.75c0-.41421.33579-.75.75-.75h3.25c2.0711 0 3.75 1.67893 3.75 3.75v6.5c0 2.0711-1.6789 3.75-3.75 3.75h-6.5c-2.07107 0-3.75-1.6789-3.75-3.75v-3.25c0-.41421.33579-.75.75-.75s.75.33579.75.75v3.25c0 1.2426 1.00736 2.25 2.25 2.25h6.5c1.2426 0 2.25-1.0074 2.25-2.25v-6.5c0-1.24264-1.0074-2.25-2.25-2.25h-3.25c-.41421 0-.75-.33579-.75-.75z"
                />
                <path
                  d="m2.96967 2.96967c.29289-.29289.76777-.29289 1.06066 0l5.46967 5.46967v-2.68934c0-.41421.33579-.75.75-.75.4142 0 .75.33579.75.75v4.5c0 .4142-.3358.75-.75.75h-4.5c-.41421 0-.75-.3358-.75-.75 0-.41421.33579-.75.75-.75h2.68934l-5.46967-5.46967c-.29289-.29289-.29289-.76777 0-1.06066z"
                />
              </g>
            </svg>
          </button>
        </div>
        ${this.tabs.map(e=>this.activeTab===e.id?e.render.call(this):y)}
      </div>

      <div class="notification-tray">${this.notifications.map(e=>this.renderMessage(e))}</div>
      <vaadin-dev-tools-component-picker
        .active=${this.componentPickActive}
        @component-picker-pick=${e=>{const t=e.detail.component;this.renderRoot.querySelector("#locationType").value==="create"?this.frontendConnection.sendShowComponentCreateLocation(t):this.frontendConnection.sendShowComponentAttachLocation(t),this.componentPickActive=!1}}
        @component-picker-abort=${e=>{this.componentPickActive=!1}}
      ></vaadin-dev-tools-component-picker>
      <div
        class="dev-tools ${this.splashMessage?"active":""}${this.unreadErrors?" error":""}"
        @click=${()=>this.toggleExpanded()}
      >
        ${this.unreadErrors?E`<svg
              fill="none"
              height="16"
              viewBox="0 0 16 16"
              width="16"
              xmlns="http://www.w3.org/2000/svg"
              xmlns:xlink="http://www.w3.org/1999/xlink"
              class="dev-tools-icon error"
            >
              <clipPath id="a"><path d="m0 0h16v16h-16z" /></clipPath>
              <g clip-path="url(#a)">
                <path
                  d="m6.25685 2.09894c.76461-1.359306 2.72169-1.359308 3.4863 0l5.58035 9.92056c.7499 1.3332-.2135 2.9805-1.7432 2.9805h-11.1606c-1.529658 0-2.4930857-1.6473-1.743156-2.9805z"
                  fill="#ff5c69"
                />
                <path
                  d="m7.99699 4c-.45693 0-.82368.37726-.81077.834l.09533 3.37352c.01094.38726.32803.69551.71544.69551.38741 0 .70449-.30825.71544-.69551l.09533-3.37352c.0129-.45674-.35384-.834-.81077-.834zm.00301 8c.60843 0 1-.3879 1-.979 0-.5972-.39157-.9851-1-.9851s-1 .3879-1 .9851c0 .5911.39157.979 1 .979z"
                  fill="#fff"
                />
              </g>
            </svg>`:E`<svg
              fill="none"
              height="17"
              viewBox="0 0 16 17"
              width="16"
              xmlns="http://www.w3.org/2000/svg"
              class="dev-tools-icon logo"
            >
              <g fill="#fff">
                <path
                  d="m8.88273 5.97926c0 .04401-.0032.08898-.00801.12913-.02467.42848-.37813.76767-.8117.76767-.43358 0-.78704-.34112-.81171-.76928-.00481-.04015-.00801-.08351-.00801-.12752 0-.42784-.10255-.87656-1.14434-.87656h-3.48364c-1.57118 0-2.315271-.72849-2.315271-2.21758v-1.26683c0-.42431.324618-.768314.748261-.768314.42331 0 .74441.344004.74441.768314v.42784c0 .47924.39576.81265 1.11293.81265h3.41538c1.5542 0 1.67373 1.156 1.725 1.7679h.03429c.05095-.6119.17048-1.7679 1.72468-1.7679h3.4154c.7172 0 1.0145-.32924 1.0145-.80847l-.0067-.43202c0-.42431.3227-.768314.7463-.768314.4234 0 .7255.344004.7255.768314v1.26683c0 1.48909-.6181 2.21758-2.1893 2.21758h-3.4836c-1.04182 0-1.14437.44872-1.14437.87656z"
                />
                <path
                  d="m8.82577 15.1648c-.14311.3144-.4588.5335-.82635.5335-.37268 0-.69252-.2249-.83244-.5466-.00206-.0037-.00412-.0073-.00617-.0108-.00275-.0047-.00549-.0094-.00824-.0145l-3.16998-5.87318c-.08773-.15366-.13383-.32816-.13383-.50395 0-.56168.45592-1.01879 1.01621-1.01879.45048 0 .75656.22069.96595.6993l2.16882 4.05042 2.17166-4.05524c.2069-.47379.513-.69448.9634-.69448.5603 0 1.0166.45711 1.0166 1.01879 0 .17579-.0465.35029-.1348.50523l-3.1697 5.8725c-.00503.0096-.01006.0184-.01509.0272-.00201.0036-.00402.0071-.00604.0106z"
                />
              </g>
            </svg>`}

        <span
          class="status-blip"
          style="background: linear-gradient(to right, ${this.getStatusColor(this.frontendStatus)} 50%, ${this.getStatusColor(this.javaStatus)} 50%)"
        ></span>
        ${this.splashMessage?E`<span class="status-description">${this.splashMessage}</span></div>`:y}
      </div>`}renderLog(){return E`<div class="message-tray">${this.messages.map(e=>this.renderMessage(e))}</div>`}activateLog(){this.unreadErrors=!1,this.updateComplete.then(()=>{const e=this.renderRoot.querySelector(".message-tray .message:last-child");e&&e.scrollIntoView()})}renderCode(){return E`<div class="info-tray">
      <div>
        <select id="locationType">
          <option value="create" selected>Create</option>
          <option value="attach">Attach</option>
        </select>
        <button
          class="button pick"
          @click=${()=>{this.componentPickActive=!0,be(()=>import("./component-picker-fe6a22e8.js"),["./component-picker-fe6a22e8.js","./custom-element-1c6555a1.js"],import.meta.url)}}
        >
          Find component in code
        </button>
      </div>
      </div>
    </div>`}renderInfo(){return E`<div class="info-tray">
      <button class="button copy" @click=${this.copyInfoToClipboard}>Copy</button>
      <dl>
        <dt>${this.serverInfo.productName}</dt>
        <dd>${this.serverInfo.vaadinVersion}</dd>
        <dt>Flow</dt>
        <dd>${this.serverInfo.flowVersion}</dd>
        <dt>Java</dt>
        <dd>${this.serverInfo.javaVersion}</dd>
        <dt>OS</dt>
        <dd>${this.serverInfo.osVersion}</dd>
        <dt>Browser</dt>
        <dd>${navigator.userAgent}</dd>
        <dt>
          Live reload
          <label class="switch">
            <input
              id="toggle"
              type="checkbox"
              ?disabled=${this.liveReloadDisabled||(this.frontendStatus==="unavailable"||this.frontendStatus==="error")&&(this.javaStatus==="unavailable"||this.javaStatus==="error")}
              ?checked="${this.frontendStatus==="active"||this.javaStatus==="active"}"
              @change=${e=>this.setActive(e.target.checked)}
            />
            <span class="slider"></span>
          </label>
        </dt>
        <dd class="live-reload-status" style="--status-color: ${this.getStatusColor(this.javaStatus)}">
          Java ${this.javaStatus} ${this.backend?`(${g.BACKEND_DISPLAY_NAME[this.backend]})`:""}
        </dd>
        <dd class="live-reload-status" style="--status-color: ${this.getStatusColor(this.frontendStatus)}">
          Front end ${this.frontendStatus}
        </dd>
      </dl>
    </div>`}renderFeatures(){return E`<div class="features-tray">
      ${this.features.map(e=>E`<div class="feature">
          <label class="switch">
            <input
              class="feature-toggle"
              id="feature-toggle-${e.id}"
              type="checkbox"
              ?checked=${e.enabled}
              @change=${t=>this.toggleFeatureFlag(t,e)}
            />
            <span class="slider"></span>
            ${e.title}
          </label>
          <a class="ahreflike" href="${e.moreInfoLink}" target="_blank">Learn more</a>
        </div>`)}
    </div>`}copyInfoToClipboard(){const e=this.renderRoot.querySelectorAll(".info-tray dt, .info-tray dd"),t=Array.from(e).map(i=>(i.localName==="dd"?": ":`
`)+i.textContent.trim()).join("").replace(/^\n/,"");ro(t),this.showNotification("information","Environment information copied to clipboard",void 0,void 0,"versionInfoCopied")}toggleFeatureFlag(e,t){const i=e.target.checked;this.frontendConnection?(this.frontendConnection.setFeature(t.id,i),this.showNotification("information",`“${t.title}” ${i?"enabled":"disabled"}`,t.requiresServerRestart?"This feature requires a server restart":void 0,void 0,`feature${t.id}${i?"Enabled":"Disabled"}`)):this.log("error",`Unable to toggle feature ${t.title}: No server connection available`)}};m.BLUE_HSL=x`206, 100%, 70%`;m.GREEN_HSL=x`145, 80%, 42%`;m.GREY_HSL=x`0, 0%, 50%`;m.YELLOW_HSL=x`38, 98%, 64%`;m.RED_HSL=x`355, 100%, 68%`;m.MAX_LOG_ROWS=1e3;m.DISMISSED_NOTIFICATIONS_IN_LOCAL_STORAGE="vaadin.live-reload.dismissedNotifications";m.ACTIVE_KEY_IN_SESSION_STORAGE="vaadin.live-reload.active";m.TRIGGERED_KEY_IN_SESSION_STORAGE="vaadin.live-reload.triggered";m.TRIGGERED_COUNT_KEY_IN_SESSION_STORAGE="vaadin.live-reload.triggeredCount";m.AUTO_DEMOTE_NOTIFICATION_DELAY=5e3;m.HOTSWAP_AGENT="HOTSWAP_AGENT";m.JREBEL="JREBEL";m.SPRING_BOOT_DEVTOOLS="SPRING_BOOT_DEVTOOLS";m.BACKEND_DISPLAY_NAME={HOTSWAP_AGENT:"HotswapAgent",JREBEL:"JRebel",SPRING_BOOT_DEVTOOLS:"Spring Boot Devtools"};S([_({type:String})],m.prototype,"url",2);S([_({type:Boolean,attribute:!0})],m.prototype,"liveReloadDisabled",2);S([_({type:String})],m.prototype,"backend",2);S([_({type:Number})],m.prototype,"springBootLiveReloadPort",2);S([_({type:Boolean,attribute:!1})],m.prototype,"expanded",2);S([_({type:Array,attribute:!1})],m.prototype,"messages",2);S([_({type:String,attribute:!1})],m.prototype,"splashMessage",2);S([_({type:Array,attribute:!1})],m.prototype,"notifications",2);S([_({type:String,attribute:!1})],m.prototype,"frontendStatus",2);S([_({type:String,attribute:!1})],m.prototype,"javaStatus",2);S([Z()],m.prototype,"tabs",2);S([Z()],m.prototype,"activeTab",2);S([Z()],m.prototype,"serverInfo",2);S([Z()],m.prototype,"features",2);S([Z()],m.prototype,"unreadErrors",2);S([Gn(".window")],m.prototype,"root",2);S([Z()],m.prototype,"componentPickActive",2);let po=m;customElements.get("vaadin-dev-tools")===void 0&&customElements.define("vaadin-dev-tools",po);export{si as C,Jn as D,j as L,Kn as P,H as a,_ as b,x as c,Yn as d,Z as e,E as h,y as n,uo as p,Gn as q,ye as r,mo as s,gn as u};
